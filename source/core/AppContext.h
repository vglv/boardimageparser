#ifndef APPCONTEXT_H
#define APPCONTEXT_H

#include <QApplication>
#include <QThread>
#include <QSettings>
#include <QDebug>
#include <QtSql>
#include "db/dbshell.h"
#include "utils/xmlprocessor.h"
#include "board/regulars.h"
#include "session.h"
#include "utils/garbageutilizer.h"


class AppContext : public QObject {

    Q_OBJECT

public:
    AppContext();
    void init(QString configName = "");
    QString getProperty(QString name, QString groupName = "");
    void setProperty(QString name, QString value, QString groupName = "");

    void setPhantomjsPrefix(const QString &str);
    QString getPhantomjsPrefix();

    ~AppContext();

    inline bool isMainThread() const { return m_mainThread == QThread::currentThreadId(); }

    static void setContext(AppContext *defaultContext);
    static AppContext* getContext();
    static void setApp(QApplication *appObject);
    static QApplication* getApp();

    QVector<Table> tables;
    Table getTable(QString tableName, bool &result);

    XmlProcessor xmlProcessor;
    DB db;
    DBShell dbShell;

    Session session;
    Regulars regulars;
    GarbageUtilizer garbageUtilizer;

    QString currentSession;

    int deletePostsTimeout;

protected:
    QSettings *setting;

    static AppContext *defaultContext;
    static QApplication *app;
    Qt::HANDLE m_mainThread;
private:
    QString phantomjs_prefix;




};

#endif
