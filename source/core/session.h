#ifndef SESSION_H
#define SESSION_H

#include <QObject>
#include <QVector>
#include <QThread>
#include <QDebug>
#include <QHash>
#include "board/bnworker.h"
#include "board/regulars.h"
#include "sessiontask.h"

class Session : public QObject
{
    Q_OBJECT
public:
    Session();
    //Session(QObject *parent, const QVector<BNType> &p_bn);
    ~Session();

    void getData();
    void Start();
    void addTask(BNType bnType);
    void addTask(BNType bnType, QString address, int timeout);
    void addTask(BNType bnType, QString address, int timeout, const QVariant& var);

    bool isThreadWithAddress(const QString &str);

    void setPhantomjsPrefix(const QString &str);
    QString getPhantomjsPrefix() const;

    void setClearInterval(int value);
    int getClearInterval() const;

    bool locked = false;

    Regulars regulars;

    QVector<QSharedPointer<BNWorker>> bnworkersList;
public slots:
    void changeStatus(QString id, TaskStatus status);

    //void finishedSuccessSlot(QString id, QSharedPointer<QObject> &someData);
    void finishedSuccessSlot(QString id, ProcessingData pData);
    void finishedErrorSlot(QString id);
    void finishedTimeoutSlot(QString id);
    void setTimestampStartSlot(QString id, qint64 value);
    void setTimestampStopSlot(QString id, qint64 value);

    void clearTasks();

private slots:

private:
    void addThread(const SessionTask &task);
    void stopThreads();

    QHash<QString, SessionTask> tasks;
    int current_index = 0;
    int clearInterval = 0;

    QString phantomjs_prefix;

    int maxRunningThreadsCount;
    int currentRunningThreadsCount;

signals:
    void stopAll();
    void startedTask(SessionTask task);
    void finishedTask(SessionTask task);
    void sendLogMessage(QString value);

    void allTasksFinished();

};

#endif // SESSION_H
