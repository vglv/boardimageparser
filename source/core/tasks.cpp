#include "tasks.h"

Tasks* Tasks::currentTasks = NULL;

Tasks::Tasks()
{

}

void Tasks::setCurrentTasks(Tasks *tasks)
{
    Tasks::currentTasks = tasks;
}

Tasks* Tasks::getCurrentTasks()
{
    return Tasks::currentTasks;
}

void Tasks::addTask(const Task &task)
{
    tasks.append(task);
    recalculateBoardsTasks();
}

int Tasks::getTasksCount()
{
    return tasks.size();
}

Task* Tasks::getTaskByIndex(int index)
{
    return &tasks[index];
}

bool Tasks::existsTaskWithID(QString id)
{
    for(int i = 0; i < tasks.size(); i++)
    {
        if(tasks[i].getID() == id)
            return true;
    }
    return false;
}

Task* Tasks::getTaskByID(QString id)
{
    for(int i = 0; i < tasks.size(); i++)
    {
        if(tasks[i].getID() == id)
            return &tasks[i];
    }
}

void Tasks::recalculateBoardsTasks()
{
    boardsTasks.clear();

    for(int i = 0; i < tasks.size(); i++)
    {
        QStringList strList = tasks[i].getBoardsList();
        for(int j = 0; j < strList.size(); j++)
        {
            if(!isBoardInHash(strList[j]))
            {
                QStringList temp;
                boardsTasks.insert(strList[j], temp);
                boardsTasks[strList[j]].append(tasks[i].getID());
            }
            else
            {
                boardsTasks[strList[j]].append(tasks[i].getID());
            }
        }
    }

    for(int i = 0; i < boardsTasks.keys().size(); i++)
    {
        qDebug() << boardsTasks.keys().at(i) << boardsTasks[boardsTasks.keys()[i]];
    }
}

bool Tasks::isBoardInHash(const QString &boardName)
{
    for(int i = 0; i < boardsTasks.keys().size(); i++)
    {
        if(boardsTasks.keys()[i] == boardName)
            return true;
    }
    return false;
}

void Tasks::removeTaskByIndex(int index)
{
    tasks.removeAt(index);
    recalculateBoardsTasks();
}

void Tasks::sortBoardsTasks(const QStringList &boards)
{
    for(int i = 0; i < boards.size(); i++)
    {
        int index = -1;
        for(int j = 0; j < boardsTasks.keys().size(); j++)
        {
            if(boards[i] == boardsTasks.keys()[j])
            {
                index = j;
            }
        }

        if(index != -1)
        {
            boardsTasks.keys().move(index, i);
        }

    }
}

QStringList Tasks::getBoards()
{
    QStringList strList;
    for(int i = 0; i < boardsTasks.keys().size(); i++) {
        strList.append(boardsTasks.keys()[i]);
    }
    return strList;
}
