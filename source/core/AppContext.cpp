#include "AppContext.h"

AppContext::AppContext() :
    QObject(), m_mainThread(QThread::currentThreadId())
{
}

AppContext::~AppContext()
{
    db.executeQuery(dbShell.generateFinishSessionSQL(currentSession, MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())));
}

AppContext* AppContext::defaultContext = NULL;
QApplication* AppContext::app = NULL;

void AppContext::setContext(AppContext *defaultContext)
{
    AppContext::defaultContext = defaultContext;
}

AppContext* AppContext::getContext()
{
    return defaultContext;
}

void AppContext::setPhantomjsPrefix(const QString &str)
{
    phantomjs_prefix = str;
}

QString AppContext::getPhantomjsPrefix()
{
    return phantomjs_prefix;
}

void AppContext::init(QString configName) {
    if(!configName.isEmpty()) {
        //qDebug() << "APP: Init Context" << configName;

        if(!QFile::exists(configName)) {
            throw MyException("Не найден файл " + configName + "!");
        }

        setting = new QSettings(configName, QSettings::IniFormat);
    }
    else {
        throw MyException("Задан пустой параметр configName!");
    }

    //qDebug() << "Available drivers:" << QSqlDatabase::drivers();
}

QString AppContext::getProperty(QString name, QString groupName) {
    QString val;
    if(groupName != "")
        val = setting->value(groupName + "/" + name, "").toString();
    else
        val = setting->value(name, "").toString();
    return val;
}

void AppContext::setProperty(QString name, QString value, QString groupName) {
    if(groupName != "") setting->setValue(groupName + "/" + name, value);
    else setting->setValue(name, value);
}

void AppContext::setApp(QApplication *appObject) {
    AppContext::app = appObject;
}

QApplication* AppContext::getApp() {
    return app;
}

Table AppContext::getTable(QString tableName, bool &result)
{
    Table table;
    result = false;
    for(int i = 0; i < tables.size(); i++) {
        if(tables[i].getName().toLower() == tableName.toLower()) {
            result = true;
            return tables[i];
        }
    }

    return table;
}
