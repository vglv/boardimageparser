#ifndef TASKS_H
#define TASKS_H
#include "task.h"
#include <QVector>


class Tasks
{
public:
    Tasks();

    static void setCurrentTasks(Tasks* tasks);
    static Tasks* getCurrentTasks();

    void addTask(const Task &task);
    int getTasksCount();
    Task* getTaskByIndex(int index);
    bool existsTaskWithID(QString id);
    Task* getTaskByID(QString id);
	void removeTaskByIndex(int index);
	
	void recalculateBoardsTasks();
    void sortBoardsTasks(const QStringList &boards);

    QStringList getBoards();

protected:
    static Tasks* currentTasks;
private:
    QVector<Task> tasks;

    QHash<QString, QStringList> boardsTasks;
    bool isBoardInHash(const QString &boardName);


};

#endif // TASKS_H
