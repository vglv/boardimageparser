#ifndef TASK_H
#define TASK_H
#include <QString>
#include <QVector>
#include "utils/MainUtils.h"


class Task
{
public:
    Task();

    QString getID() const;
    void setID(const QString &str);
    void setSearchString(const QString &str);
    QString getSearchString() const;

    void setBoards(const QStringList boardsList);
    void setBoards(const QString str);
    QString getBoardsString();
    QStringList getBoardsList();

private:
    QString id;
    QString search;
    QString boards;

};

#endif // TASK_H
