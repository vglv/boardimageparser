#include "session.h"
#include <QMessageBox>

Session::Session()
{
    //qDebug() << Q_FUNC_INFO;
    currentRunningThreadsCount = 0;
    maxRunningThreadsCount = 5;
}

/*Session::Session(QObject *parent, const QVector<BNType> &p_bn)
{
    for(int i = 0; i < p_bn.size(); i++) {
        bnTypes.append(p_bn[i]);
    }
}*/

void Session::setPhantomjsPrefix(const QString &str)
{
    phantomjs_prefix = str;
}

QString Session::getPhantomjsPrefix() const
{
    return phantomjs_prefix;
}

void Session::addThread(const SessionTask &task)
{
    //qDebug() << Q_FUNC_INFO;
    if(currentRunningThreadsCount < maxRunningThreadsCount) {
        //qDebug() << Q_FUNC_INFO;
        BNWorker *worker = new BNWorker(task);
        QThread *thread = new QThread();

        //bnworkersList.append(worker);
        bnworkersList.push_back(QSharedPointer<BNWorker>(worker));

        worker->setPhantomjsPrefix(getPhantomjsPrefix());
        worker->regulars = this->regulars;

        connect(thread, SIGNAL(started()), worker, SLOT(process()));
        connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
        //connect(worker, SIGNAL(finishedSuccess(QString, QSharedPointer<QObject>)), this, SLOT(finishedSuccessSlot(QString, QSharedPointer<QObject>)));
        connect(worker, SIGNAL(finishedSuccess(QString, ProcessingData)), this, SLOT(finishedSuccessSlot(QString, ProcessingData)));
        connect(worker, SIGNAL(finishedError(QString)), this, SLOT(finishedErrorSlot(QString)));
        connect(worker, SIGNAL(finishedTimeout(QString)), this, SLOT(finishedTimeoutSlot(QString)));
        connect(worker, SIGNAL(setTimestampStart(QString,qint64)), this, SLOT(setTimestampStartSlot(QString,qint64)));
        connect(worker, SIGNAL(setTimestampStop(QString,qint64)), this, SLOT(setTimestampStopSlot(QString,qint64)));
        connect(worker, SIGNAL(changeStatus(QString, TaskStatus)), this, SLOT(changeStatus(QString, TaskStatus)));
        connect(this, SIGNAL(stopAll()), worker, SLOT(stop()));

        worker->pThread = thread;

        /*if(task.bnType == BN_GETTHREAD)
        {
            qDebug() << Q_FUNC_INFO << "999999" << task.getAddress() << task.getID() << task.getIndex();
        }*/

        //qDebug() << Q_FUNC_INFO << "starting:" << currentRunningThreadsCount << maxRunningThreadsCount;
        thread->start();
        emit startedTask(task);
        currentRunningThreadsCount++;
        //tasks[keys[i]].taskStatus = TaskStatus::TS_INPROCESS;
        //task.taskStatus = TaskStatus::TS_INPROCESS;
    }

    return;
}

void Session::addTask(BNType bnType)
{
    locked = true;

    //qDebug() << Q_FUNC_INFO;
    SessionTask newTask(bnType);

    tasks[newTask.getID()] = newTask;
    tasks[newTask.getID()].setIndex(current_index);
    current_index++;

    locked = false;
}

void Session::addTask(BNType bnType, QString address, int timeout)
{
    locked = true;

    //qDebug() << Q_FUNC_INFO;
    SessionTask newTask(bnType);
    newTask.setAddress(address);
    newTask.setTimeout(timeout);

    tasks[newTask.getID()] = newTask;
    tasks[newTask.getID()].setIndex(current_index);
    current_index++;

    locked = false;
}

void Session::addTask(BNType bnType, QString address, int timeout, const QVariant& var)
{
    locked = true;

    SessionTask newTask(bnType);
    newTask.setAddress(address);
    newTask.setTimeout(timeout);

    newTask.setValue(var);

    tasks[newTask.getID()] = newTask;
    tasks[newTask.getID()].setIndex(current_index);
    current_index++;

    locked = false;
}

void Session::Start()
{
    //qDebug() << Q_FUNC_INFO << "111111";
    locked = true;

    /*QMessageBox msgBox;
    msgBox.setText("1");
    msgBox.exec();*/

    QList<QString> keys = tasks.keys();

    //QString message = "Keys size: " + QString::number(tasks.size());
    //emit sendLogMessage(message);

    for(int i = 0; i < keys.size(); i++)
    {
        //qDebug() << Q_FUNC_INFO << 1 << i << currentRunningThreadsCount << maxRunningThreadsCount;
        if(currentRunningThreadsCount >= maxRunningThreadsCount)
        {
            break;
        }
        //qDebug() << Q_FUNC_INFO << 2 << i;

        if(tasks[keys[i]].taskStatus == TaskStatus::TS_NOTSTARTED)
        {
            //qDebug() << Q_FUNC_INFO << 3 << i;
            /*if(tasks[keys[i]].bnType == BN_GETTHREAD)
            {
                qDebug() << Q_FUNC_INFO << i << tasks[keys[i]].getAddress() << tasks[keys[i]].getID()
                         << tasks[keys[i]].getIndex() << QThread::currentThreadId() << "SIZE:" << keys.size() << tasks.keys().size();
            }*/

            //tasks[keys[i]].taskStatus = TaskStatus::TS_INPROCESS;
            //message = "Starting task: " + QString::number(tasks[keys[i]].getIndex()) + " " + tasks[keys[i]].getAddress();
            //emit sendLogMessage(message);

            addThread(tasks[keys[i]]);
        }
    }

    locked = false;
}

void Session::stopThreads()
{
    emit stopAll();
}

Session::~Session()
{
    stopThreads();
}

void Session::changeStatus(QString id, TaskStatus status)
{
    switch(tasks[id].taskStatus)
    {
        case TaskStatus::TS_NOTSTARTED:
        {
            tasks[id].taskStatus = status;
            break;
        }
        case TaskStatus::TS_INPROCESS:
        {
            tasks[id].taskStatus = status;
            break;
        }
        case TaskStatus::TS_FINISHEDERROR:
        {
            tasks[id].taskStatus = status;
            break;
        }
        case TaskStatus::TS_FINISHEDSUCCESS:
        {
            tasks[id].taskStatus = status;
            break;
        }
        case TaskStatus::TS_TIMEOUT:
        {
            tasks[id].taskStatus = status;
            break;
        }
    }
}

void Session::setTimestampStartSlot(QString id, qint64 value)
{
    tasks[id].setTimestampStart(value);
}

void Session::setTimestampStopSlot(QString id, qint64 value)
{
    tasks[id].setTimestampStop(value);
}

//void Session::finishedSuccessSlot(QString id, QSharedPointer<QObject> &someData)
void Session::finishedSuccessSlot(QString id, ProcessingData pData)
{
    tasks[id].someData = pData;
    //tasks[id].taskStatus = TaskStatus::TS_FINISHEDSUCCESS;
    emit finishedTask(tasks[id]);
    currentRunningThreadsCount--;

    Start();
}

void Session::finishedErrorSlot(QString id)
{
    //tasks[id].taskStatus = TaskStatus::TS_FINISHEDERROR;
    emit finishedTask(tasks[id]);
    currentRunningThreadsCount--;

    Start();
}

void Session::finishedTimeoutSlot(QString id)
{
    //tasks[id].taskStatus = TaskStatus::TS_TIMEOUT;
    emit finishedTask(tasks[id]);
    currentRunningThreadsCount--;

    Start();
}

void Session::setClearInterval(int value)
{
    clearInterval = value;
}

int Session::getClearInterval() const
{
    return clearInterval;
}

void Session::clearTasks()
{
    //qDebug() << Q_FUNC_INFO;
    int size = tasks.size();
    QString message = "Начало процесса удаления завершившихся задач из списка. Всего задач в списке: " + QString::number(size) + ".";
    emit sendLogMessage(message);
    int count = 0;
    for(int i = tasks.size() - 1; i >= 0; i--) {
        if(tasks[   tasks.keys()[i]].taskStatus != TaskStatus::TS_NOTSTARTED
                 && tasks[tasks.keys()[i]].taskStatus != TaskStatus::TS_INPROCESS)
        {
            tasks.remove(tasks.keys()[i]);
            count++;
        }

    }
    message = "Удалено " + QString::number(count) + " завершившихся задач из списка. Всего осталось задач в списке: "
            + QString::number(tasks.size()) + ".";
    emit sendLogMessage(message);



    int all_size = 0;

    for(int i = 0; i < bnworkersList.size(); i++) {
        //qDebug() << bnworkersList[i].data();
        //qDebug() << bnworkersList[i].data()->temp004;
        all_size += sizeof(*bnworkersList[i].data());
    }

    message = "Количество объектов BNWorker: " + QString::number(bnworkersList.size())
            + ". Всего массив объектов BNWorker занимает " + QString::number(all_size) + " Б.";
    emit sendLogMessage(message);


    for(int i = 0; i < bnworkersList.size(); i++) {
        //qDebug() << i << bnworkersList.value(i);
    }

    for(int i = bnworkersList.size() - 1; i >=0; i--) {
        if(bnworkersList[i]->isFinished) {
            //qDebug() << "BNWorker with index"  << i << "is finished.";
            bnworkersList.remove(i);
        }
    }

    all_size = 0;

    for(int i = 0; i < bnworkersList.size(); i++) {
        all_size += sizeof(*bnworkersList[i].data());
    }

    message = "Количество объектов BNWorker: " + QString::number(bnworkersList.size())
            + ". Всего массив объектов BNWorker занимает " + QString::number(all_size) + " Б.";
    emit sendLogMessage(message);

    /*int c_ = bnworkersList.size() >= 5 ? 5 : bnworkersList.size();

    for(int i = 0; i < c_; i++)
    {
        message = bnworkersList[i]->getSessionTask().getAddress();
        emit sendLogMessage(QString::number(i) + ": " +  message);
        QString taskStatus;
        if( bnworkersList[i]->getSessionTask().taskStatus == TaskStatus::TS_NOTSTARTED)
            taskStatus = "TS_NOTSTARTED";
        else if( bnworkersList[i]->getSessionTask().taskStatus == TaskStatus::TS_INPROCESS)
            taskStatus = "TS_INPROCESS";
        else if( bnworkersList[i]->getSessionTask().taskStatus == TaskStatus::TS_FINISHEDSUCCESS)
            taskStatus = "TS_FINISHEDSUCCESS";
        else if( bnworkersList[i]->getSessionTask().taskStatus == TaskStatus::TS_FINISHEDERROR)
            taskStatus = "TS_FINISHEDERROR";
        else if( bnworkersList[i]->getSessionTask().taskStatus == TaskStatus::TS_TIMEOUT)
            taskStatus = "TS_TIMEOUT";
        emit sendLogMessage(QString::number(i) + ": " + taskStatus );
        emit sendLogMessage(QString::number(i) + ": " + QString::number(bnworkersList[i]->getSessionTask().getTimeout()) );
        emit sendLogMessage(  QString::number(i) + ": "
                            + QString::number(bnworkersList[i]->getSessionTask().getTimestampStop() - bnworkersList[i]->getSessionTask().getTimestampStop()) );
    }*/

    bool exit = false;
    while(!exit)
    {
        QVector<int> indexes;
        for(int i = 0; i < bnworkersList.size(); i++)
        {
            if (tasks.find(bnworkersList[i]->getSessionTask().getID()) == tasks.end())
            {
                indexes.append(i);
            }
        }
        for(int i = 0; i < indexes.size(); i++)
            qDebug() << indexes[i];

        for(int i = indexes.size() - 1; i >=0; i--)
        {
            qDebug() << "Removing " << indexes[i];
            bnworkersList.removeAt(indexes[i]);
        }
        exit = true;
    }



    if(bnworkersList.size() == 0)
    {
        emit allTasksFinished();
    }

    //QTimer::singleShot(clearInterval, this, SLOT(clearTasks()));
    QTimer::singleShot(clearInterval, this, SLOT(clearTasks()));
}

bool Session::isThreadWithAddress(const QString &str)
{
    for(int i = 0; i < tasks.keys().size(); i++)
    {
        if(str == tasks[tasks.keys().at(i)].getAddress())
        {
            return true;
        }
    }

    return false;
}
