#include "task.h"

Task::Task()
{
    id = MainUtils::getContext()->newMD5();
}

QString Task::getID() const
{
    return id;
}

void Task::setID(const QString &str)
{
    id = str;
}

void Task::setSearchString(const QString &str)
{
    search = str;
}

QString Task::getSearchString() const
{
    return search;
}

void Task::setBoards(const QStringList boardsList)
{
    QString temp("");
    for(int i = 0; i < boardsList.size(); i++)
    {
        if(i < boardsList.size() - 1)
            temp += boardsList[i] + QString(",");
        else
            temp += boardsList[i];
    }
    boards = temp;
}

void Task::setBoards(const QString str)
{
    boards = str;
}

QString Task::getBoardsString()
{
    return boards;
}

QStringList Task::getBoardsList()
{
    return boards.split(",");
}
