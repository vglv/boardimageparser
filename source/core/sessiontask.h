#ifndef SESSIONTASK_H
#define SESSIONTASK_H

#include "utils/MainUtils.h"
#include "core/enums.h"
#include "board/processingdata.h"

class SessionTask
{
public:
    SessionTask();
    SessionTask(BNType bn_type);
	~SessionTask();

    TaskStatus taskStatus = TS_NOTSTARTED;
    BNType bnType;

    QString getID() const;

    void setIndex(int value);
    int getIndex() const;

    void setAddress(const QString &value);
    QString getAddress() const;

    void setTimeout(int value);
    int getTimeout() const;

    void setTimestampStart(qint64 value);
    qint64 getTimestampStart() const;

    void setTimestampStop(qint64 value);
    qint64 getTimestampStop() const;

    ProcessingData someData;

    void setValue(const QVariant &var);
    QVariant getValue() const;

private:
    QString id;
    int index;

    QString address;
    int timeout;

    qint64 timestampStart;
    qint64 timestampStop;

    QVariant value;

};

#endif // SESSIONTASK_H
