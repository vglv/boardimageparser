#include "sessiontask.h"

SessionTask::SessionTask()
{
    id = MainUtils::getContext()->newMD5();
}

SessionTask::~SessionTask()
{
    //qDebug() << Q_FUNC_INFO;
}


SessionTask::SessionTask(BNType bn_type)
{
    bnType = bn_type;
    id = MainUtils::getContext()->newMD5();
}

QString SessionTask::getID() const
{
    return id;
}

void SessionTask::setIndex(int value)
{
    index = value;
}

int SessionTask::getIndex() const
{
    return index;
}

void SessionTask::setAddress(const QString &value)
{
    address = value;
}

QString SessionTask::getAddress() const
{
    return address;
}

void SessionTask::setTimeout(int value)
{
    timeout = value;
}

int SessionTask::getTimeout() const
{
    return timeout;
}

void SessionTask::setTimestampStart(qint64 value)
{
    timestampStart = value;
}

qint64 SessionTask::getTimestampStart() const
{
    return timestampStart;
}

void SessionTask::setTimestampStop(qint64 value)
{
    timestampStop = value;
}

qint64 SessionTask::getTimestampStop() const
{
    return timestampStop;
}


void SessionTask::setValue(const QVariant& var)
{
    value = var;
}

QVariant SessionTask::getValue() const
{
    return value;
}
