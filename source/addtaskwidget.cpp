#include "addtaskwidget.h"
#include "ui_addtaskwidget.h"

addTaskWidget::addTaskWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddTask)
{
    ui->setupUi(this);
    model = NULL;
}

addTaskWidget::~addTaskWidget()
{
    delete ui;
}

void addTaskWidget::cleanSearch()
{
    ui->leSearch->clear();
}

void addTaskWidget::getBoards()
{
    //qDebug() << Q_FUNC_INFO;
    //qDebug() << AppContext::getContext()->db.getDBName();

    if(model != NULL)
        delete model;

    model = new QStandardItemModel(0, 4, this);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Доска")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Название")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Описание")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Выбрать")));

    //ui->tvBoards->setModel(model);
    ui->tvBoards->setModel(model);

    QVector<QPair<Board, QString>> boards = AppContext::getContext()->dbShell.getBoards();
    for(int i = 0; i < boards.size(); i++) {
            QList<QStandardItem*> items;
            items.append(new QStandardItem(boards[i].first.getBrd()));
            items.append(new QStandardItem(boards[i].first.getName()));
            items.append(new QStandardItem(boards[i].first.getDescription()));
            items.append(new QStandardItem(QString("Выбрать")));

            model->appendRow(items);
    }
    for(int i = 0; i < boards.size(); i++)
    {
        model->item(i, 3)->setFlags(model->item(i, 3)->flags() | Qt::ItemIsUserCheckable);
        model->item(i, 3)->setCheckState(Qt::Unchecked);
    }
}

void addTaskWidget::on_pbSelectAll_clicked()
{
    for(int i = 0; i < model->rowCount(); i++)
    {
        model->item(i, 3)->setCheckState(Qt::Checked);
    }
}

void addTaskWidget::on_pbDeselectAll_clicked()
{
    for(int i = 0; i < model->rowCount(); i++)
    {
        model->item(i, 3)->setCheckState(Qt::Unchecked);
    }
}

void addTaskWidget::on_pbCancel_clicked()
{
    this->close();
}

void addTaskWidget::on_pbAdd_clicked()
{
    if(!isModeChange)
    {
        bool error = false;
        if(ui->leSearch->text().trimmed() == QString(""))
        {
            QMessageBox msgBox;
            msgBox.setText("Строка для поиска не должна быть пустой!");
            msgBox.setWindowTitle("Ошибка");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
            error = true;
        }
        int count = 0;

        for(int i = 0; i < model->rowCount(); i++)
        {
            if(model->item(i, 3)->checkState() == Qt::Checked) {
                count++;
                break;
            }
        }

        if(count == 0)
        {
            QMessageBox msgBox;
            msgBox.setText("Должна быть выбрана хотя бы одна доска!");
            msgBox.setWindowTitle("Ошибка");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
            error = true;
        }

        if(error)
        {
            this->close();
            return;
        }

        Task task;
        task.setSearchString(ui->leSearch->text());
        QStringList boards;

        for(int i = 0; i < model->rowCount(); i++)
        {
            if(model->item(i, 3)->checkState() == Qt::Checked)
                boards.append(model->item(i,0)->text());
        }

        task.setBoards(boards);

        bool result = false;
        Table tasksTable = AppContext::getContext()->getTable("tasks", result);
        Row tempRow = Row::createRowObject(tasksTable);

        for(int j = 0; j < tempRow.values.size(); j++) {
            if(tempRow.values[j].getName() == "uid") {
                tempRow.values[j].setTextValue(task.getID());
            }
            if(tempRow.values[j].getName() == "search") {
                tempRow.values[j].setTextValue(task.getSearchString());
            }
            if(tempRow.values[j].getName() == "boards") {
                tempRow.values[j].setTextValue(task.getBoardsString());
            }
            if(tempRow.values[j].getName() == "created") {
                tempRow.values[j].setMarked(true);
                tempRow.values[j].setTextValue(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp()));
            }
            if(tempRow.values[j].getName() == "last_modified") {
                tempRow.values[j].setTextValue(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp()));
            }
        }

        QStringList queries = AppContext::getContext()->dbShell.generateUpdateInsertRow(tempRow);

        AppContext::getContext()->db.executeQueries(queries);
        if(AppContext::getContext()->db.getIsError()) {
            qDebug() << "ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
        }
        else
        {
            Tasks::getCurrentTasks()->addTask(task);

            emit taskAdded();
        }

        this->close();
    }
    else
    {
        bool error = false;
        if(ui->leSearch->text().trimmed() == QString(""))
        {
            QMessageBox msgBox;
            msgBox.setText("Строка для поиска не должна быть пустой!");
            msgBox.setWindowTitle("Ошибка");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
            error = true;
        }
        int count = 0;

        for(int i = 0; i < model->rowCount(); i++)
        {
            if(model->item(i, 3)->checkState() == Qt::Checked) {
                count++;
                break;
            }
        }

        if(count == 0)
        {
            QMessageBox msgBox;
            msgBox.setText("Должна быть выбрана хотя бы одна доска!");
            msgBox.setWindowTitle("Ошибка");
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
            error = true;
        }

        if(error)
        {
            this->close();
            return;
        }

        QStringList boards;

        for(int i = 0; i < model->rowCount(); i++)
        {
            if(model->item(i, 3)->checkState() == Qt::Checked)
                boards.append(model->item(i,0)->text());
        }

        QString boardsString;

        for(int i = 0; i < boards.size(); i++)
        {
            if(i < boards.size() - 1)
                boardsString += boards[i] + QString(",");
            else
                boardsString += boards[i];
        }

        bool result = false;
        Table tasksTable = AppContext::getContext()->getTable("tasks", result);
        Row tempRow = Row::createRowObject(tasksTable);

        for(int j = 0; j < tempRow.values.size(); j++) {
            if(tempRow.values[j].getName() == "uid") {
                tempRow.values[j].setTextValue(changingTaskID);
            }
            if(tempRow.values[j].getName() == "search") {
                tempRow.values[j].setTextValue(ui->leSearch->text());
            }
            if(tempRow.values[j].getName() == "boards") {
                tempRow.values[j].setTextValue(boardsString);
            }
            if(tempRow.values[j].getName() == "last_modified") {
                tempRow.values[j].setTextValue(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp()));
            }
        }

        QString query = AppContext::getContext()->dbShell.generateUpdateTaskSQL(  changingTaskID
                                                                                , ui->leSearch->text()
                                                                                , boardsString
                                                                                , MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp()));

        if(!AppContext::getContext()->db.executeQuery(query))
        {
            if(AppContext::getContext()->db.getIsError()) {
                qDebug() << "ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
            }
        }

        else
        {
            Task *currentTask = Tasks::getCurrentTasks()->getTaskByID(changingTaskID);
            currentTask->setSearchString(ui->leSearch->text());
            currentTask->setBoards(boardsString);

            Tasks::getCurrentTasks()->recalculateBoardsTasks();

            emit taskChanged();
        }

        this->close();
    }
}

void addTaskWidget::setModeChange(bool isChange)
{
    if(isChange) {
        isModeChange = true;
        ui->pbAdd->setText(QString("Изменить"));
    }
    else {
        isModeChange = false;
        ui->pbAdd->setText(QString("Добавить"));
    }
}

void addTaskWidget::setChangingTaskID(const QString &id)
{
    changingTaskID = id;
}

void addTaskWidget::setSearchString(const QString &searchString)
{
    ui->leSearch->setText(searchString);
}

void addTaskWidget::setBoardsSelection(const QStringList &boards)
{
    for(int i = 0; i < model->rowCount(); i++)
    {
        bool found = false;
        for(int j = 0; j < boards.size(); j++)
        {
            if(boards[j] == model->item(i, 0)->text())
            {
                found = true;
                break;
            }
        }
        if(found) {
            model->item(i, 3)->setCheckState(Qt::Checked);
        }
        else {
            model->item(i, 3)->setCheckState(Qt::Unchecked);
        }
    }
}
