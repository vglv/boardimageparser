#include "garbageutilizer.h"

GarbageUtilizer::GarbageUtilizer()
{
}

void GarbageUtilizer::setFilesDeleteTimeout(int value)
{
    filesDeleteTimeout = value;
}

int GarbageUtilizer::getFilesDeleteTimeout()
{
    return filesDeleteTimeout;
}

void GarbageUtilizer::deleteFile(QString fileName)
{
}

void GarbageUtilizer::start()
{
    QTimer::singleShot(filesDeleteTimeout, this, SLOT(deleteFiles()));
}

void GarbageUtilizer::deleteFiles()
{
    QString pathdir = QCoreApplication::applicationDirPath();
    QDir dir(pathdir + "/phantomjs/temp/");
    QStringList files = dir.entryList();
    for(int i = 0; i < files.size(); i++) {
        if(files[i] == "." || files[i] == "..")
            continue;
        QString fileName = dir.absolutePath() + "/" + files[i];
        filesToDeleteList.append(fileName);
    }

    QString message = QString("Начало удаления временных файлов. Количество файлов для удаления: ")
                    + QString::number(filesToDeleteList.size()) + QString(". ");
    emit sendLogMessage(message);

    QVector<int> indexes;

    for(int i = 0; i < filesToDeleteList.size(); i++) {
        message = QString("Удаляем файл: ") + filesToDeleteList[i] + QString(". ");
        try {
            if(QFile::remove(filesToDeleteList[i])) {
                indexes.append(i);
                message += "Файл успешно удален.";
            }
            else {
                message += "Возникла ошибка при удалении файла.";
            }
        }
        catch(std::exception e) {
            message += "Возникла ошибка при удалении файла.";
        }

        emit sendLogMessage(message);
    }

    for(int i = indexes.size() - 1; i >=0; i--) {
        filesToDeleteList.removeAt(indexes[i]);
    }



    message = QString("Начало удаления сохраненных тредов. Количество тредов для удаления: ")
                    + QString::number(dirsToDeleteList.size()) + QString(". ");
    emit sendLogMessage(message);

    indexes.clear();

    for(int i = 0; i < dirsToDeleteList.size(); i++) {
        message = QString("Удаляем тред: ") + dirsToDeleteList[i] + QString(". ");
        try {

            QFile tempFile(dirsToDeleteList[i]);
            tempFile.setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner);
            tempFile.close();


            QDir tempDir(dirsToDeleteList[i]);


            if(tempDir.removeRecursively()) {
                indexes.append(i);
                message += "Тред успешно удален.";
            }
            else {
                message += "Возникла ошибка при удалении треда.";
            }
        }
        catch(std::exception e) {
            message += "Возникла ошибка при удалении треда.";
        }

        emit sendLogMessage(message);
    }

    for(int i = indexes.size() - 1; i >=0; i--) {
        dirsToDeleteList.removeAt(indexes[i]);
    }


    QTimer::singleShot(filesDeleteTimeout, this, SLOT(deleteFiles()));
}

void GarbageUtilizer::addDirToDelete(QString dir)
{
    bool found = false;
    for(int i = 0; i < dirsToDeleteList.count(); i++)
    {
        if(dirsToDeleteList[i] == dir)
        {
            found = true;
            break;
        }
    }

    if(!found)
        dirsToDeleteList.append(dir);

}
