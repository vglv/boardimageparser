#include "xmlprocessor.h"

XmlProcessor::XmlProcessor()
{
    //qDebug() << Q_FUNC_INFO;
}

XmlProcessor::~XmlProcessor()
{
    //qDebug() << Q_FUNC_INFO;
}

bool XmlProcessor::setXmlFile(const QString &filename)
{
    //qDebug() << Q_FUNC_INFO << filename;
    xmlFile = filename;
    return true;
}

QString XmlProcessor::getXmlFile()
{
    //qDebug() << Q_FUNC_INFO;
    return xmlFile;
}

bool XmlProcessor::ifFileExists()
{
    //qDebug() << Q_FUNC_INFO;
    QFile file(xmlFile);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Невозможно открыть файл " << xmlFile;
        return false;
    }

    file.close();

    return true;
}

bool XmlProcessor::ifXmlSyntaxCorrect()
{
    //qDebug() << Q_FUNC_INFO;
    QFile file(xmlFile);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QXmlStreamReader xmlReader(&file);

    bool isError = false;

    while(!xmlReader.atEnd() && !xmlReader.hasError()) {
        QXmlStreamReader::TokenType token = xmlReader.readNext();
        if(token == QXmlStreamReader::StartDocument)
            continue;
        if(token == QXmlStreamReader::StartElement) {
            //qDebug() << "token: " << xmlReader.name();
            QXmlStreamAttributes attrs = xmlReader.attributes();
            for(int i = 0; i < attrs.size(); i++) {
                //qDebug() << "attribute: " << attrs.at(i).name() << " " << attrs.at(i).value();
            }
        }

        if(xmlReader.hasError()) {
            qDebug() << xmlReader.errorString();
            isError = true;
        }
    }

    file.close();
    xmlReader.clear();

    return !isError;
}

QVector<Table> XmlProcessor::getTables()
{
    //qDebug() << Q_FUNC_INFO;
    QFile file(xmlFile);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QXmlStreamReader xmlReader(&file);

    bool isError = false;

    QVector<Table> tables;

    while(!xmlReader.atEnd() && !xmlReader.hasError()) {
        QXmlStreamReader::TokenType token = xmlReader.readNext();
        if(token == QXmlStreamReader::StartDocument)
            continue;
        if(token == QXmlStreamReader::StartElement) {

            if(xmlReader.name().trimmed().toString().toLower() == QString("table")) {
                Table table;

                QXmlStreamAttributes attrs = xmlReader.attributes();

                bool nameExists = false;

                for(int i = 0; i < attrs.size(); i++) {
                    if(attrs.at(i).name().trimmed().toString().toLower() == QString("name")) {
                        nameExists = true;
                        table.setName(attrs.at(i).value().trimmed().toString());
                    }
                }

                if(!nameExists) {
                    MyException ex("Ошибка парсинга XML-файла");
                    throw ex;
                }

                while(xmlReader.readNextStartElement()) {


                    if(xmlReader.name().trimmed().toString().toLower() == QString("column")) {
                        QXmlStreamAttributes attrs = xmlReader.attributes();

                        bool nameExists = false;
                        bool typeExists = false;

                        Column column;

                        for(int i = 0; i < attrs.size(); i++) {
                            if(attrs.at(i).name().trimmed().toString().toLower() == QString("name")) {
                                nameExists = true;
                                column.setName(attrs.at(i).value().trimmed().toString());
                            }
                            if(attrs.at(i).name().trimmed().toString().toLower() == QString("type")) {
                                typeExists = true;
                                QString typeValue = attrs.at(i).value().toString().toLower();
                                if(typeValue == "integer") {
                                    column.setColumnType(SqliteTypesEnum::SQ_INTEGER);
                                }
                                if(typeValue == "real") {
                                    column.setColumnType(SqliteTypesEnum::SQ_REAL);
                                }
                                if(typeValue == "text") {
                                    column.setColumnType(SqliteTypesEnum::SQ_TEXT);
                                }
                                if(typeValue == "blob") {
                                    column.setColumnType(SqliteTypesEnum::SQ_BLOB);
                                }
                                if(typeValue != "null" && typeValue != "integer" && typeValue != "real" && typeValue != "text" && typeValue != "blob") {
                                    MyException ex("Ошибка парсинга XML-файла: неверный тип данных поля");
                                    throw ex;
                                }
                            }
                            if(attrs.at(i).name().trimmed().toString().toLower() == QString("primary_key")) {
                                column.setIsPrimaryKey(true);
                            }
                            if(attrs.at(i).name().trimmed().toString().toLower() == QString("null")) {
                                QString nullValue = attrs.at(i).value().toString().toLower();
                                if(nullValue == "false")
                                    column.setIsNull(false);
                                else
                                    column.setIsNull(true);
                            }
                        }

                        if(!nameExists) {
                            MyException ex("Ошибка парсинга XML-файла: отсутствует название поля");
                            throw ex;
                        }
                        if(!typeExists) {
                            MyException ex("Ошибка парсинга XML-файла: отсутствует тип данных поля");
                            throw ex;
                        }

                        table.columns.append(column);
                    }

                    xmlReader.readNext();
                }

                tables.append(table);
            }
        }

        if(xmlReader.hasError()) {
            qDebug() << xmlReader.errorString();
            isError = true;
        }
    }

    file.close();
    xmlReader.clear();

    return tables;
}
