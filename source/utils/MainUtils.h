#ifndef MAINUTILS_H
#define MAINUTILS_H

#define STRINGIZE(x) #x
#define STRINGIZE_VALUE_OF(x) STRINGIZE(x)

#include <QDateTime>
#include <QProcess>
#include <QScopedPointer>
#include <QString>
#include <QCryptographicHash>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "myexception.h"
#ifdef Q_OS_WIN
#include <windows.h>
#endif

class MainUtils
{
public:
    MainUtils();
    ~MainUtils();
    qint64 getCurrentTimestamp();
    std::mt19937_64 mt;

    static void setContext(MainUtils* defaultContext);
    static MainUtils* getContext();

    QString md5(quint64 value);
    QString newMD5();

    QString getDateTimeString(qint64 value);

protected:
    static MainUtils* defaultContext;

};

#endif
