#ifndef XMLPROCESSOR_H
#define XMLPROCESSOR_H

#include <QXmlStreamReader>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QException>
#include "db/db.h"
#include "utils/MainUtils.h"


class XmlProcessor
{
public:
    XmlProcessor();
    ~XmlProcessor();

    bool setXmlFile(const QString &filaname);
    QString getXmlFile();

    bool ifFileExists();
    bool ifXmlSyntaxCorrect();

    QVector<Table> getTables();

    /*bool startElement(const QString &namespaceURI, const QString &localName, const QString &qName, const QXmlAttributes &atts);
    bool endElement(const QString &namespaceURI, const QString &locaName, const QString &qName);

    QString errorString() const;
    bool endDocument();
    bool startDocument();*/
private:
    QString xmlFile;
};

#endif // XMLPROCESSOR_H
