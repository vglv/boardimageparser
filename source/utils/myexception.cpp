#include "myexception.h"

MyException::MyException()
{
    //qDebug() << Q_FUNC_INFO << this;
}

MyException::MyException(const QString &message)
{
    //qDebug() << Q_FUNC_INFO << this;
    msg = message;
}

MyException::~MyException()
{
    //qDebug() << Q_FUNC_INFO << this;
}

void MyException::setMessage(const QString &message)
{
    //qDebug() << Q_FUNC_INFO;
    msg = message;
}

QString MyException::getMessage() const
{
    //qDebug() << Q_FUNC_INFO << this;
    return msg;
}
