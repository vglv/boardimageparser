#include "MainUtils.h"


MainUtils::MainUtils()
{
    QDateTime dt;
    mt.seed((quint64)dt.currentMSecsSinceEpoch());
}

MainUtils* MainUtils::defaultContext = NULL;

MainUtils::~MainUtils()
{

}

qint64 MainUtils::getCurrentTimestamp()
{
    QDateTime dt;
    return dt.currentMSecsSinceEpoch();
}

QString MainUtils::md5(quint64 value)
{
    QByteArray ba;
    ba.append( reinterpret_cast< const char* >(&value), sizeof(value));
    QByteArray res = QCryptographicHash::hash(ba, QCryptographicHash::Md5);
    return QString(res.toHex());
}

QString MainUtils::newMD5()
{
    return md5(mt());
}

QString MainUtils::getDateTimeString(qint64 value)
{
    QDateTime temp;
    temp.setMSecsSinceEpoch(value);

    return temp.toString("yyyy.MM.dd HH:mm:ss.zzz");
}

void MainUtils::setContext(MainUtils *defaultContext)
{
    MainUtils::defaultContext = defaultContext;
}

MainUtils* MainUtils::getContext()
{
    return defaultContext;
}
