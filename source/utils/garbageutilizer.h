﻿#ifndef GARBAGEUTILIZER_H
#define GARBAGEUTILIZER_H

#include <QObject>
#include <QcoreApplication>
#include <QString>
#include <QVector>
#include <QFile>
#include <QDir>
#include <QTimer>

class GarbageUtilizer : public QObject {
    Q_OBJECT
public:
    GarbageUtilizer();
    void setFilesDeleteTimeout(int value);
    int getFilesDeleteTimeout();
    void addDirToDelete(QString dir);
public slots:
    void deleteFile(QString fileName);
private:
    int filesDeleteTimeout;
    QStringList filesToDeleteList;
    QStringList dirsToDeleteList;

private slots:
	void deleteFiles();
	void start();
signals:
    void sendLogMessage(QString value);

};

#endif // GARBAGEUTILIZER_H
