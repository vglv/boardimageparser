#ifndef MYEXCEPTION_H
#define MYEXCEPTION_H

#include <QString>
#include <QDebug>
#include <QException>


class MyException : public QException
{
public:
    MyException();
    MyException(const QString &message);
    ~MyException();
    void setMessage(const QString &message);
    QString getMessage() const;

    void raise() const { throw *this; }
    MyException *clone() const { return new MyException(*this); }

private:
    QString msg;
};

#endif // MYEXCEPTION_H
