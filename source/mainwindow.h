#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "core/AppContext.h"
#include <QThread>
#include <QtConcurrent/QtConcurrentRun>
#include "boardswidget.h"
#include "taskswidget.h"
#include "core/tasks.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    boardsWidget bWidget;
    tasksWidget tWidget;
    void newLogItemSuccess(const QString& msg);
    void newLogItemError(const QString& msg);
    void newLogItemTimeout(const QString& msg);
public slots:
    void startedTaskSlot(SessionTask task);
    void finishedTaskSlot(SessionTask task);
    void receiveLogMessage(QString value);

private slots:
    void on_pushButton_clicked();
    void getBoardsList();

    void qwerty123();
    void on_pbCleanLog_clicked();
    void clearTasks();
    void on_bpBoardsList_clicked();
    void on_pbTasksList_clicked();
    void on_pbExport_clicked();
    void on_pbExportAll_clicked();

    void onPostsCountChanged();
    void onFilterChanged();

    void showCurrentPost();

    void on_pbBegin_clicked();

    void on_pbPrev_clicked();

    void on_pbNext_clicked();

    void on_pbLast_clicked();

    void on_pbBrowserLocal_clicked();

    void on_pbBrowserInternet_clicked();

    void refreshPosts();
    void deletePosts();

private:
    Ui::MainWindow *ui;
    QVector<Task> searchTasks;
    QString outputFile;

    bool firstBoards;

    QVector<QString> filter;
    //QVector<QPair<Post, QPair<QString, QString>>> currentPosts;
    QPair<Post, QPair<QString, QString>> post;
    int currentPost;
    int totalPostsCount;

signals:
    void guStart();
    void postsCountChanged();
    void filterChanged();
};

#endif // MAINWINDOW_H
