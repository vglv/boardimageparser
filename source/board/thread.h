#ifndef THREAD_H
#define THREAD_H

#include <QString>
#include <QMetaType>

class Thread
{
public:
    Thread();
    void setBrd(const QString& str);
    QString getBrd() const;
    void setNum(const QString& str);
    QString getNum() const;
    void setSubject(const QString& str);
    QString getSubject() const;
    void setComment(const QString& str);
    QString getComment() const;
    void setThreadCreated(const QString& str);
    QString getThreadCreated() const;
    void setThreadLasthit(const QString& str);
    QString getThreadLasthit() const;
    void setPostsCount(int value);
    int getPostsCount() const;
    void setFilesCount(int value);
    int getFilesCount() const;
    void setEmail(const QString& str);
    QString getEmail() const;

    void setAddress(const QString& str);
    QString getAddress() const;

    void setLastSaved(const QString& str);
    QString getLastSaved() const;

    void setDeleted(int value);
    int getDeleted() const;

private:
    QString brd;
    QString num;
    QString subject;
    QString comment;
    QString threadCreated;
    QString threadLasthit;
    int postsCount;
    int filesCount;
    QString email;

    QString address;

    QString last_saved;
    int deleted;
};

class ThreadsList
{
public:
    std::vector<Thread> threads;
};

Q_DECLARE_METATYPE( ThreadsList )
#endif // THREAD_H
