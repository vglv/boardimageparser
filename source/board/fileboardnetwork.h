#ifndef FILEBOARDNETWORK_H
#define FILEBOARDNETWORK_H

#include <QProcess>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDir>
#include <QFile>
#include "boardnetwork.h"

class FileBoardNetwork : public BoardNetwork
{
    Q_OBJECT
public:
    FileBoardNetwork();
    ~FileBoardNetwork();

    bool getHTML();

private slots:
    void htmlDownloadedSlot(QNetworkReply* pReply);
    void timeoutSlot();

};

#endif // FILEBOARDNETWORK_H
