#include "thread.h"

Thread::Thread()
{

}

void Thread::setBrd(const QString& str)
{
    brd = str;
}

QString Thread::getBrd() const
{
    return brd;
}

void Thread::setNum(const QString& str)
{
    num = str;
}

QString Thread::getNum() const
{
    return num;
}

void Thread::setSubject(const QString& str)
{
    subject = str;
}

QString Thread::getSubject() const
{
    return subject;
}

void Thread::setComment(const QString& str)
{
    comment = str;
}

QString Thread::getComment() const
{
    return comment;
}

void Thread::setThreadCreated(const QString& str)
{
    threadCreated = str;
}

QString Thread::getThreadCreated() const
{
    return threadCreated;
}

void Thread::setThreadLasthit(const QString& str)
{
    threadLasthit = str;
}

QString Thread::getThreadLasthit() const
{
    return threadLasthit;
}

void Thread::setPostsCount(int value)
{
    postsCount = value;
}

int Thread::getPostsCount() const
{
    return postsCount;
}

void Thread::setFilesCount(int value)
{
    filesCount = value;
}

int Thread::getFilesCount() const
{
    return filesCount;
}

void Thread::setEmail(const QString &str)
{
    email = str;
}

QString Thread::getEmail() const
{
    return email;
}

void Thread::setAddress(const QString &str)
{
    address = str;
}

QString Thread::getAddress() const
{
    return address;
}

void Thread::setLastSaved(const QString& str)
{
    last_saved = str;
}

QString Thread::getLastSaved() const
{
    return last_saved;
}

void Thread::setDeleted(int value)
{
    deleted = value;
}

int Thread::getDeleted() const
{
    return deleted;
}
