#include "threadboardnetwork.h"

ThreadBoardNetwork::ThreadBoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

ThreadBoardNetwork::~ThreadBoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

bool ThreadBoardNetwork::getHTML()
{
    QTimer::singleShot(timeout, this, SLOT(timeoutSlot()));

    //qDebug() << Q_FUNC_INFO << "!!! ###### !!!" << address;

    manager = QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(this));
    QObject::connect(manager.data(), SIGNAL(finished(QNetworkReply *)), this, SLOT(htmlDownloadedSlot(QNetworkReply *)));

    QNetworkRequest request;

    request.setUrl(QUrl(address));
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");


    manager->setNetworkAccessible(QNetworkAccessManager::Accessible);
    //qDebug() << QNetworkAccessManager::NetworkAccessibility();

    manager->get(request);

    return true;
}

void ThreadBoardNetwork::htmlDownloadedSlot(QNetworkReply* pReply)
{
    //qDebug() << Q_FUNC_INFO;
    if(!isTimeout) {
        htmlDownloaded = true;
        int statusCode = pReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        //html = QString::fromUtf8(pReply->readAll());

        QByteArray ba = pReply->readAll();

        parseHTML(ba);
    }


    if(!isTimeout) {

    }
}

/*void ThreadBoardNetwork::imageDownloadedSlot(QNetworkReply *pReply)
{
    QByteArray ba = pReply->readAll();

    QString pathdir = QCoreApplication::applicationDirPath();
    int index1 = getAddress().lastIndexOf("/");
    int index2 = getAddress().lastIndexOf(".");
    QString thread = getAddress().mid(index1 + 1, index2 - index1 - 1);

    QString dir = pathdir + "/threads/" + thread + "/data";

    index1 = images[currentImage].lastIndexOf("/");
    QString fname = images[currentImage].mid(index1 + 1);
    QString filename = dir + "/" + fname;

    //qDebug() << filename << ba.length() << currentImage << images.size();

    if(!QFile::exists(filename))
    {
        QFile file(filename);

        if (file.open(QIODevice::ReadWrite)) {
            file.write(ba);
            file.close();
        }
    }

    currentImage++;

    if(currentImage < images.size())
    {
        QNetworkAccessManager *tmanager = new QNetworkAccessManager(this);
        //manager = QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(this));
        QObject::connect(tmanager, SIGNAL(finished(QNetworkReply *)), this, SLOT(imageDownloadedSlot(QNetworkReply *)));

        QNetworkRequest request;

        request.setUrl(images.at(currentImage));

        tmanager->setNetworkAccessible(QNetworkAccessManager::Accessible);
        tmanager->get(request);
    }
    else
    {
        qDebug() << Q_FUNC_INFO << currentImage << images.size();
        emit downloadedCorrect();
    }
}*/

void ThreadBoardNetwork::timeoutSlot()
{
    if(!htmlDownloaded) {
        isTimeout = true;
        emit timeoutExpired();
    }
}

void ThreadBoardNetwork::parseHTML(const QByteArray& b)
{
    QByteArray ba(b);

    if(ba.indexOf("<span class=\"notfound404\">404</span>") != -1)
    {
        someData.htmlLength = QString::fromUtf8(ba).length();
        emit downloadedError();
        return;
    }

    QStringList images;

    QRegExp rx("(\\w*)\\/src\\/(\\w*)\\/(\\w+).jpg");

    QString str = QString::fromUtf8(ba);
    QStringList list1;
    QStringList list2;
    QStringList list3;
    int pos = 0;

    while ((pos = rx.indexIn(str, pos)) != -1) {
        list1 << rx.cap(1);
        list2 << rx.cap(2);
        list3 << rx.cap(3);
        pos += rx.matchedLength();
    }

    QString pathdir = QCoreApplication::applicationDirPath();
    int index1 = getAddress().lastIndexOf("/");
    int index2 = getAddress().lastIndexOf(".");
    QString thread = getAddress().mid(index1 + 1, index2 - index1 - 1);

    QStringList strl = getAddress().split("/");
    QString boardName = strl.at(strl.size() - 3);

    if(!QDir(pathdir + "/threads").exists()) {
        QDir().mkdir(pathdir + "/threads");
    }

    if(!QDir(pathdir + "/threads/" + boardName).exists()) {
        QDir().mkdir(pathdir + "/threads/" + boardName);
    }

    if(!QDir(pathdir + "/threads/" + boardName + "/" + thread).exists()) {
        QDir().mkdir(pathdir + "/threads/" + boardName + "/" + thread);
    }

    for(int i = 0; i < list1.size(); i++)
    {
        QString addr = "https://2ch.hk/" + list1.at(i) + "/src/" + list2.at(i) + "/" + list3.at(i) + ".jpg";

        bool found = false;

        for(int j = 0; j < images.size(); j++)
        {
            if(images[j] == addr) {
                found = true;
                break;
            }
        }
        if(!found)
        {
            QString before = "/" + list1.at(i) + "/src/" + list2.at(i) + "/" + list3.at(i) + ".jpg";
            QByteArray after = QString("./data/" + list3.at(i) + ".jpg").toUtf8();
            ba = ba.replace(before, after);
            images.append(addr);
        }
    }


    QRegExp rxs("(\\w*)\\/thumb\\/(\\w*)\\/(\\w+).jpg");

    QStringList lists1;
    QStringList lists2;
    QStringList lists3;
    pos = 0;

    while ((pos = rxs.indexIn(str, pos)) != -1) {
        lists1 << rxs.cap(1);
        lists2 << rxs.cap(2);
        lists3 << rxs.cap(3);
        pos += rxs.matchedLength();
    }

    for(int i = 0; i < lists1.size(); i++)
    {
        QString addr = "https://2ch.hk/" + lists1.at(i) + "/thumb/" + lists2.at(i) + "/" + lists3.at(i) + ".jpg";

        bool found = false;

        for(int j = 0; j < images.size(); j++)
        {
            if(images[j] == addr) {
                found = true;
                break;
            }
        }
        if(!found)
        {
            QString before = "/" + lists1.at(i) + "/thumb/" + lists2.at(i) + "/" + lists3.at(i) + ".jpg";
            QByteArray after = QString("./data/" + lists3.at(i) + ".jpg").toUtf8();
            ba = ba.replace(before, after);
            images.append(addr);
        }
    }



    QRegExp rxp("(\\w*)\\/src\\/(\\w*)\\/(\\w+).png");

    QStringList listp1;
    QStringList listp2;
    QStringList listp3;
    pos = 0;

    while ((pos = rxp.indexIn(str, pos)) != -1) {
        listp1 << rxp.cap(1);
        listp2 << rxp.cap(2);
        listp3 << rxp.cap(3);
        pos += rxp.matchedLength();
    }

    for(int i = 0; i < listp1.size(); i++)
    {
        QString addr = "https://2ch.hk/" + listp1.at(i) + "/src/" + listp2.at(i) + "/" + listp3.at(i) + ".png";

        bool found = false;

        for(int j = 0; j < images.size(); j++)
        {
            if(images[j] == addr) {
                found = true;
                break;
            }
        }
        if(!found)
        {
            QString before = "/" + listp1.at(i) + "/src/" + listp2.at(i) + "/" + listp3.at(i) + ".png";
            QByteArray after = QString("./data/" + listp3.at(i) + ".png").toUtf8();
            ba = ba.replace(before, after);
            images.append(addr);
        }
    }

    QString before = QString("/makaba/templates/css/makaba.css");
    QByteArray after = QString("https://2ch.hk/makaba/templates/css/makaba.css").toUtf8();
    ba = ba.replace(before, after);

    before = QString("/makaba/templates/js/jquery-3.1.0.min.js");
    after = QString("https://2ch.hk/makaba/templates/js/jquery-3.1.0.min.js").toUtf8();
    ba = ba.replace(before, after);

    before = QString("/makaba/templates/js/swag.js?cache=");
    after = QString("https://2ch.hk/makaba/templates/js/swag.js?cache=").toUtf8();
    ba = ba.replace(before, after);

    QString filename = pathdir + "/threads/" + boardName + "/" + thread + "/" + thread + ".html";
    QFile file(filename);
    if (file.open(QIODevice::ReadWrite)) {
        file.write(ba);
        file.close();

        QVariant var(images);
        someData.setValue(var);
        someData.htmlLength = QString::fromUtf8(ba).length();

        emit downloadedCorrect();
    }
    else
    {
        someData.htmlLength = QString::fromUtf8(ba).length();
        emit downloadedError();
    }
}
