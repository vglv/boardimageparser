#ifndef CATALOGBOARDNETWORK_H
#define CATALOGBOARDNETWORK_H

#include <QProcess>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "boardnetwork.h"
#include "thread.h"

class CatalogBoardNetwork : public BoardNetwork
{
    Q_OBJECT
public:
    CatalogBoardNetwork();
    ~CatalogBoardNetwork();

    bool getHTML();
    void parseHTML();
private:
    QVector<Thread> threads;
    QString board;
private slots:
    void htmlDownloadedSlot(QNetworkReply* pReply);
    void timeoutSlot();

};

#endif // CATALOGBOARDNETWORK_H
