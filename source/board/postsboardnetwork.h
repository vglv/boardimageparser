#ifndef POSTSBOARDNETWORK_H
#define POSTSBOARDNETWORK_H

#include <QProcess>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "boardnetwork.h"
#include "post.h"

class PostsBoardNetwork : public BoardNetwork
{
    Q_OBJECT
public:
    PostsBoardNetwork();
    ~PostsBoardNetwork();

    bool getHTML();
    void parseHTML();
private:
    QVector<Post> posts;
    QString board;
private slots:
    void htmlDownloadedSlot(QNetworkReply* pReply);
    void timeoutSlot();

};

#endif // POSTSBOARDNETWORK_H
