#include "fileboardnetwork.h"

FileBoardNetwork::FileBoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;

}

FileBoardNetwork::~FileBoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

bool FileBoardNetwork::getHTML()
{
    QTimer::singleShot(timeout, this, SLOT(timeoutSlot()));

    manager = QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(this));
    QObject::connect(manager.data(), SIGNAL(finished(QNetworkReply *)), this, SLOT(htmlDownloadedSlot(QNetworkReply *)));

    QNetworkRequest request;

    request.setUrl(QUrl(address));
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");

    manager->setNetworkAccessible(QNetworkAccessManager::Accessible);

    manager->get(request);

    return true;
}

void FileBoardNetwork::htmlDownloadedSlot(QNetworkReply* pReply)
{
    if(!isTimeout) {
        htmlDownloaded = true;
        int statusCode = pReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

        QByteArray ba = pReply->readAll();

        QStringList str = address.split("/");

        QString fname = str.at(str.length() - 1);
        QString thread = str.at(str.length() - 2);
        QString boardName = str.at(str.size() - 4);


        QString pathdir = QCoreApplication::applicationDirPath();

        if(!QDir(pathdir + "/threads").exists()) {
            QDir().mkdir(pathdir + "/threads");
        }

        if(!QDir(pathdir + "/threads/" + boardName).exists()) {
            QDir().mkdir(pathdir + "/threads/" + boardName);
        }

        if(!QDir(pathdir + "/threads/" + boardName + "/" + thread).exists()) {
            QDir().mkdir(pathdir + "/threads/" + boardName + "/" + thread);
        }

        if(!QDir(pathdir + "/threads/" + boardName + "/" + thread + "/data").exists()) {
            QDir().mkdir(pathdir + "/threads/" + boardName + "/" + thread + "/data");
        }

        QString filename = pathdir + "/threads/" + boardName + "/" + thread + "/data/" + fname;

        QFile file(filename);
        if (file.open(QIODevice::ReadWrite)) {
            file.write(ba);
            file.close();
            someData.htmlLength = QString::fromUtf8(ba).length();
            emit downloadedCorrect();
        }
        else
        {
            someData.htmlLength = QString::fromUtf8(ba).length();
            emit downloadedError();
        }
    }
    if(!isTimeout) {
    }
}

void FileBoardNetwork::timeoutSlot()
{
    if(!htmlDownloaded) {
        isTimeout = true;
        emit timeoutExpired();
    }
}
