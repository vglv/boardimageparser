#include "postsboardnetwork.h"


PostsBoardNetwork::PostsBoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

PostsBoardNetwork::~PostsBoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

bool PostsBoardNetwork::getHTML()
{
    //qDebug() << Q_FUNC_INFO << address;
    QTimer::singleShot(timeout, this, SLOT(timeoutSlot()));

    //qDebug() << Q_FUNC_INFO << address;
    manager = QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(this));
    QObject::connect(manager.data(), SIGNAL(finished(QNetworkReply *)), this, SLOT(htmlDownloadedSlot(QNetworkReply *)));

    QNetworkRequest request;

    request.setUrl(QUrl(address));
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");


    manager->setNetworkAccessible(QNetworkAccessManager::Accessible);
    //qDebug() << QNetworkAccessManager::NetworkAccessibility();

    manager->get(request);

    return true;
}

void PostsBoardNetwork::htmlDownloadedSlot(QNetworkReply* pReply)
{
    //qDebug() << Q_FUNC_INFO;
    if(!isTimeout) {
        htmlDownloaded = true;
        int statusCode = pReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        //qDebug() << statusCode;
        html = QString::fromUtf8(pReply->readAll());

        parseHTML();
    }
}

void PostsBoardNetwork::timeoutSlot()
{
    //qDebug() << Q_FUNC_INFO;
    if(!htmlDownloaded) {
        isTimeout = true;
        emit timeoutExpired();
    }
}


void PostsBoardNetwork::parseHTML()
{
    bool res = false;

    QJsonDocument json = QJsonDocument::fromJson(html.toUtf8());
    QJsonObject objAll = json.object();

    if(objAll.contains("threads") && objAll["threads"].isArray())
    {
        QJsonArray threadsArr = objAll["threads"].toArray();
        QJsonObject threadsObj = threadsArr.at(0).toObject();

        if(threadsObj.contains("posts") && threadsObj["posts"].isArray())
        {
            QJsonArray postsArr = threadsObj["posts"].toArray();

            for(int i = 0; i < postsArr.size(); i++)
            {
                QJsonObject tempObj = postsArr[i].toObject();

                Post postTemp;

                if(tempObj.contains("comment"))
                {
                    postTemp.setComment(tempObj["comment"].toString());
                }
                if(tempObj.contains("subject"))
                {
                    postTemp.setSubject(tempObj["subject"].toString());
                }

                QString date = QString::number(tempObj["timestamp"].toInt());
                date = MainUtils::getContext()->getDateTimeString(date.toLongLong() * 1000);
                postTemp.setDate(date);

                if(tempObj.contains("num"))
                {
                    postTemp.setNum(QString::number(tempObj["num"].toInt()));
                }

                if(tempObj.contains("email"))
                {
                    postTemp.setEmail(tempObj["email"].toString());
                }

                if(tempObj.contains("name"))
                {
                    postTemp.setName(tempObj["name"].toString());
                }

                if(tempObj.contains("number"))
                {
                    postTemp.setNumber(tempObj["number"].toInt());
                }

                postTemp.setAddress(this->address.replace(".json", ".html") + "#" + postTemp.getNum());

                posts.append(postTemp);
            }

        }
    }

    PostsList pList;
    pList.posts = posts.toStdVector();


    QVariant var = QVariant::fromValue(pList);
    someData.setValue(var);

    someData.htmlLength = html.length();

    emit downloadedCorrect();
}
