#include "boardsboardnetwork.h"

BoardsBoardNetwork::BoardsBoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

BoardsBoardNetwork::~BoardsBoardNetwork()
{
    //qDebug() << someData;
    //qDebug() << Q_FUNC_INFO;
}

bool BoardsBoardNetwork::getHTML()
{
    QTimer::singleShot(getTimeout(), this, SLOT(timeoutSlot()));

    qint64 timestamp = MainUtils::getContext()->getCurrentTimestamp();
    QString pathdir = QCoreApplication::applicationDirPath();
    QString exe =  pathdir + "/phantomjs/" + "phantomjs_bat.exe";
    QStringList arguments;

    QString fileName = pathdir + "/phantomjs/temp/" + getPhantomjsPrefix() + "_" + QString::number(timestamp) + ".html";
    filePath = fileName;

    arguments << QString("load.js") << getAddress() << getPhantomjsPrefix() << QString::number(timestamp);

    QProcess::startDetached(exe, arguments);

    QTimer::singleShot(50, this, SLOT(fileWait()));

    return true;
}

void BoardsBoardNetwork::htmlDownloadedSlot(const QString &filePath)
{
    //qDebug() << Q_FUNC_INFO << filePath;

    if(!isTimeout) {
        htmlDownloaded = true;

        QFile htmlFile(filePath);

        htmlFile.open(QIODevice::ReadOnly);
        html = QString::fromUtf8(htmlFile.readAll());
        htmlFile.close();
        //qDebug() << html.length();

        parseHTML();
    }
}

void BoardsBoardNetwork::fileWait()
{
    if(isTimeout)
        return;

    if(QFile::exists(filePath)) {
        emit htmlDownloadedSlot(filePath);
    }
    else
    {
        QTimer::singleShot(50, this, SLOT(fileWait()));
    }
}

void BoardsBoardNetwork::parseHTML()
{
    QRegularExpression regBoard(regulars.regBoard);
    QRegularExpressionMatchIterator matchIterator1 = regBoard.globalMatch(html);

    int count = 0;
    while(matchIterator1.hasNext()) {
        QRegularExpressionMatch match = matchIterator1.next();
        QString brd = match.captured(2);
        Board board;
        board.setBrd(brd);
        boards.append(board);
        count++;
    }

    QRegularExpression regName(regulars.regName);
    QRegularExpressionMatchIterator matchIterator2 = regName.globalMatch(html);

    count = 0;
    while(matchIterator2.hasNext()) {
        QRegularExpressionMatch match = matchIterator2.next();
        QString name = match.captured(1);
        boards[count].setName(name);
        count++;
    }

    QRegularExpression regPostsPerHour(regulars.regPostsPerHour);
    QRegularExpressionMatchIterator matchIterator3 = regPostsPerHour.globalMatch(html);

    count = 0;
    while(matchIterator3.hasNext()) {
        QRegularExpressionMatch match = matchIterator3.next();
        QString postsPerHour = match.captured(1);
        int posts_per_hour = 0;
        try {
            posts_per_hour = postsPerHour.toInt();
        }
        catch(std::exception e)
        {
            //qDebug() << e.what();
            emit downloadedError();
            return;
        }

        boards[count].setPostsPerHour(posts_per_hour);
        count++;
    }

    QRegularExpression regUnicIps(regulars.regUnicIps);
    QRegularExpressionMatchIterator matchIterator4 = regUnicIps.globalMatch(html);

    count = 0;
    while(matchIterator4.hasNext()) {
        QRegularExpressionMatch match = matchIterator4.next();
        QString unicIps = match.captured(1);
        int unic_ips = 0;
        try {
            unic_ips = unicIps.toInt();
        }
        catch(std::exception e)
        {
            //qDebug() << e.what();
            emit downloadedError();
            return;
        }
        boards[count].setUnicIps(unic_ips);
        count++;
    }

    QRegularExpression regPostsCount(regulars.regPostsCount);
    QRegularExpressionMatchIterator matchIterator6 = regPostsCount.globalMatch(html);

    count = 0;
    while(matchIterator6.hasNext()) {
        QRegularExpressionMatch match = matchIterator6.next();
        QString postsCount = match.captured(1);
        qint64 posts_count = 0;
        try {
            posts_count = postsCount.toLongLong();
        }
        catch(std::exception e)
        {
            //qDebug() << e.what();
            emit downloadedError();
            return;
        }
        boards[count].setPostsCount(QString::number(posts_count));
        count++;
    }

    for(int i = 0; i < boards.size(); i++) {
        QString regDescription_temp = regulars.regDescription;
        regDescription_temp = regDescription_temp.replace(QString("###REPLACE_THIS_BY_STRING###"), boards[i].getName());
        QRegularExpression regDescription(regDescription_temp);
        QRegularExpressionMatchIterator matchIterator5 = regDescription.globalMatch(html);

        while(matchIterator5.hasNext()) {
            QRegularExpressionMatch match = matchIterator5.next();
            QString description = match.captured(3);
            boards[i].setDescription(description);
        }
    }

    try {
        QFile::remove(filePath);
    }
    catch(std::exception e) {
        //qDebug() << e.what();
    }

    if(count == 0) {
        emit downloadedError();
        return;
    }

    BoardsList bList;
    bList.boards = boards.toStdVector();

    QVariant var = QVariant::fromValue(bList);
    someData.setValue(var);
    //someData.setBoards(boards);

    emit downloadedCorrect();
}

void BoardsBoardNetwork::timeoutSlot()
{
    if(!htmlDownloaded) {
        isTimeout = true;
        emit timeoutExpired();
    }
}

void BoardsBoardNetwork::doDeleteLayer(QObject *obj)
{
    //qDebug() << Q_FUNC_INFO;
}
