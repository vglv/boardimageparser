#ifndef BOARD_H
#define BOARD_H

#include <QString>
#include <QMetaType>

class Board
{
public:
    Board();
    void setBrd(const QString& str);
    QString getBrd() const;
    void setName(const QString& str);
    QString getName() const;
    void setPostsPerHour(int value);
    int getPostsPerHour() const;
    void setUnicIps(int value);
    int getUnicIps() const;
    void setDescription(const QString& str);
    QString getDescription() const;
    void setPostsCount(QString value);
    QString getPostsCount() const;
private:
    QString brd;
    QString name;
    int posts_per_hour;
    int unic_ips;
    QString description;
    QString posts_count;
};

class BoardsList
{
public:
    std::vector<Board> boards;
};

Q_DECLARE_METATYPE( BoardsList )
#endif // BOARD_H
