#include "catalogboardnetwork.h"

using QtJson::JsonArray;
using QtJson::JsonObject;

CatalogBoardNetwork::CatalogBoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

CatalogBoardNetwork::~CatalogBoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

bool CatalogBoardNetwork::getHTML()
{
    //qDebug() << Q_FUNC_INFO << address;
    QTimer::singleShot(timeout, this, SLOT(timeoutSlot()));

    //qDebug() << Q_FUNC_INFO << address;
    manager = QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(this));
    QObject::connect(manager.data(), SIGNAL(finished(QNetworkReply *)), this, SLOT(htmlDownloadedSlot(QNetworkReply *)));

    QNetworkRequest request;

    request.setUrl(QUrl(address));
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");


    manager->setNetworkAccessible(QNetworkAccessManager::Accessible);
    //qDebug() << QNetworkAccessManager::NetworkAccessibility();

    manager->get(request);

    return true;
}

void CatalogBoardNetwork::htmlDownloadedSlot(QNetworkReply* pReply)
{
    //qDebug() << Q_FUNC_INFO;
    if(!isTimeout) {
        htmlDownloaded = true;
        int statusCode = pReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        //qDebug() << statusCode;
        html = QString::fromUtf8(pReply->readAll());

        parseHTML();
    }
}

void CatalogBoardNetwork::timeoutSlot()
{
    //qDebug() << Q_FUNC_INFO;
    if(!htmlDownloaded) {
        isTimeout = true;
        emit timeoutExpired();
    }
}


void CatalogBoardNetwork::parseHTML()
{
    //qDebug() << Q_FUNC_INFO;
    //qDebug() << "length:" << html.length();
    bool res = false;


    QJsonDocument json = QJsonDocument::fromJson(html.toUtf8());
    QJsonObject objAll = json.object();

    if(objAll.contains("threads") && objAll["threads"].isArray())
    {
        QJsonArray threadsArr = objAll["threads"].toArray();

        for(int i = 0; i < threadsArr.size(); i++)
        {
            QJsonObject tempObj = threadsArr[i].toObject();

            Thread threadTemp;
            threadTemp.setBrd(value.toString());

            if(tempObj.contains("comment"))
            {
                threadTemp.setComment(tempObj["comment"].toString());
            }
            if(tempObj.contains("subject"))
            {
                threadTemp.setSubject(tempObj["subject"].toString());
            }
            else
            {
                threadTemp.setSubject(tempObj["comment"].toString().mid(0, 100));
            }

            if(threadTemp.getSubject().trimmed() == "" || threadTemp.getSubject().trimmed() == "\"\"")
            {
                threadTemp.setSubject(tempObj["comment"].toString().mid(0, 100));
            }

            if(tempObj.contains("num"))
            {
                threadTemp.setNum(tempObj["num"].toString());
            }

            QString threadCreated = QString::number(tempObj["timestamp"].toInt());
            threadCreated = MainUtils::getContext()->getDateTimeString(threadCreated.toLongLong() * 1000);
            threadTemp.setThreadCreated(threadCreated);

            QString threadLasthit = QString::number(tempObj["lasthit"].toInt());
            threadLasthit = MainUtils::getContext()->getDateTimeString(threadLasthit.toLongLong() * 1000);
            threadTemp.setThreadLasthit(threadLasthit);

            if(tempObj.contains("posts_count"))
            {
                threadTemp.setPostsCount(tempObj["posts_count"].toInt());
            }

            if(tempObj.contains("files_count"))
            {
                threadTemp.setFilesCount(tempObj["files_count"].toInt());
            }

            if(tempObj.contains("email"))
            {
                threadTemp.setEmail(tempObj["email"].toString());
            }

            QString address = "https://2ch.hk" + threadTemp.getBrd() + "res/" + threadTemp.getNum() + ".html";
            threadTemp.setAddress(address);

            threads.append(threadTemp);
        }
    }

    /*for(int i = 0; i < threads.size(); i++) {
        qDebug() << threads[i].getBrd() << i << ":";
        qDebug() << threads[i].getSubject();
        qDebug() << threads[i].getComment().mid(0, 100);
        qDebug() << threads[i].getNum();
        qDebug() << threads[i].getThreadCreated();
        qDebug() << threads[i].getThreadLasthit();
        qDebug() << threads[i].getPostsCount();
        qDebug() << threads[i].getFilesCount();
        qDebug() << threads[i].getEmail();
    }

    */




    /*JsonObject temp = QtJson::parse(html, res).toMap();
    if(!res) {
        emit downloadedError();
        return;
    }

    int index = -1;
    for(int i = 0; i < temp.keys().size(); i++) {
        if(temp.keys().at(i) == QString("threads")) {
            index = i;
            break;
        }
    }

    if(index == -1) {
        emit downloadedError();
        return;
    }

    JsonArray arr = temp["threads"].toList();

    for(int i = 0; i < arr.size(); i++) {

        Thread thread;
        QString subject;
        QString comment;
        QString num;
        QString threadCreated;
        QString threadLasthit;
        int postsCount;
        int filesCount;
        QString email;

        thread.setBrd(value.toString());

        JsonObject temp2 = arr.at(i).toMap();
        if(temp2["subject"] != NULL)
            subject = QtJson::serializeStr(temp2["subject"]);
        else
            subject = QtJson::serializeStr(temp2["comment"]).mid(0, 100);

        if(subject.trimmed() == QString("") || subject.trimmed() == QString("\"\"")) {
            subject = QtJson::serializeStr(temp2["comment"]).mid(0, 100);
        }

        subject = subject.replace("&#47;", "/");
        subject = subject.replace("&#92;", "\\");
        subject = subject.replace("&gt;", ">");
        subject = subject.replace("&lt;", "<");
        //subject = subject.replace("\"", """");
        thread.setSubject(subject);

        comment = QtJson::serializeStr(temp2["comment"]);
        comment = comment.replace("&#47;", "/");
        comment = comment.replace("&#92;", "\\");
        comment = comment.replace("&gt;", ">");
        comment = comment.replace("&lt;", "<");
        thread.setComment(comment);

        num = QtJson::serializeStr(temp2["num"]);
        thread.setNum(num);

        threadCreated = QtJson::serializeStr(temp2["timestamp"]);
        threadCreated = MainUtils::getContext()->getDateTimeString(threadCreated.toLongLong() * 1000);
        thread.setThreadCreated(threadCreated);

        threadLasthit = QtJson::serializeStr(temp2["lasthit"]);
        threadLasthit = MainUtils::getContext()->getDateTimeString(threadLasthit.toLongLong() * 1000);
        thread.setThreadLasthit(threadLasthit);

        postsCount = QtJson::serializeStr(temp2["posts_count"]).toInt();
        thread.setPostsCount(postsCount);

        filesCount = QtJson::serializeStr(temp2["files_count"]).toInt();
        thread.setFilesCount(filesCount);

        email = QtJson::serializeStr(temp2["email"]);
        thread.setEmail(email);

        threads.append(thread);
    }

    for(int i = 0; i < threads.size(); i++) {
        qDebug() << threads[i].getBrd() << i << ":";
        qDebug() << threads[i].getSubject();
        qDebug() << threads[i].getComment().mid(0, 100);
        qDebug() << threads[i].getNum();
        qDebug() << threads[i].getThreadCreated();
        qDebug() << threads[i].getThreadLasthit();
        qDebug() << threads[i].getPostsCount();
        qDebug() << threads[i].getFilesCount();
        qDebug() << threads[i].getEmail();
    }*/

    ThreadsList tList;
    tList.threads = threads.toStdVector();

    QVariant var = QVariant::fromValue(tList);
    someData.setValue(var);

    someData.htmlLength = html.length();

    emit downloadedCorrect();
}
