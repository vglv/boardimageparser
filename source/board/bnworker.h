﻿#ifndef BNWORKER_H
#define BNWORKER_H

#include "boardnetwork.h"
#include "boardsboardnetwork.h"
#include "catalogboardnetwork.h"
#include "postsboardnetwork.h"
#include "threadboardnetwork.h"
#include "fileboardnetwork.h"
#include "regulars.h"
#include "core/sessiontask.h"
#include "core/enums.h"

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QSslSocket>
#include <QSsl>

class BNWorker : public QObject
{
    Q_OBJECT
private:
    QSharedPointer<BoardNetwork> bn;
    SessionTask task;
    QString phantomjs_prefix;
    qint64 timestampStart;
    qint64 timestampStop;

public:
    BNWorker(const SessionTask &temp_task);
    ~BNWorker();

    void setPhantomjsPrefix(const QString &str);
    QString getPhantomjsPrefix();

    qint64 getTimestampStart() const;
    qint64 getTimestampStop();
	
	SessionTask getSessionTask() const;

    Regulars regulars;
	
	bool isFinished = false;

    QThread *pThread;

public slots:
    void process();
    void stop();

    void downloadedErrorSlot();
    void downloadedCorrectSlot();
    void timeoutExpiredSlot();

signals:
    void finished();
    //void finishedSuccess(QString id, QSharedPointer<QObject>);
    void finishedSuccess(QString id, ProcessingData pData);
    void finishedError(QString id);
    void finishedTimeout(QString id);
    void setTimestampStart(QString id, qint64 value);
    void setTimestampStop(QString id, qint64 value);

    void changeStatus(QString id, TaskStatus status);


};

#endif // BNWORKER_H
