#ifndef THREADBOARDNETWORK_H
#define THREADBOARDNETWORK_H

#include <QProcess>
#include <QCoreApplication>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDir>
#include <QFile>
#include "boardnetwork.h"

class ThreadBoardNetwork : public BoardNetwork
{
    Q_OBJECT
public:
    ThreadBoardNetwork();
    ~ThreadBoardNetwork();

    bool getHTML();
    void parseHTML(const QByteArray& b);

private slots:
    void htmlDownloadedSlot(QNetworkReply* pReply);
    //void imageDownloadedSlot(QNetworkReply* pReply);
    void timeoutSlot();

};

#endif // THREADBOARDNETWORK_H
