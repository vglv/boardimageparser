#include "boardnetwork.h"
#include "core/AppContext.h"

BoardNetwork::BoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

BoardNetwork::~BoardNetwork()
{
    //qDebug() << Q_FUNC_INFO;
}

void BoardNetwork::setPhantomjsPrefix(const QString &str)
{
    phantomjs_prefix = str;
}

QString BoardNetwork::getPhantomjsPrefix()
{
    return phantomjs_prefix;
}

void BoardNetwork::setAddress(const QString &str)
{
    //qDebug() << Q_FUNC_INFO << str;
    address = str;
}

QString BoardNetwork::getAddress()
{
    return address;
}

void BoardNetwork::setTimeout(int value)
{
    //qDebug() << Q_FUNC_INFO << value;
    timeout = value;
}

int BoardNetwork::getTimeout()
{
    return timeout;
}

bool BoardNetwork::getHTML()
{
    //qDebug() << Q_FUNC_INFO << address;
    QTimer::singleShot(timeout, this, SLOT(timeoutSlot()));

    //qDebug() << Q_FUNC_INFO << address;
    manager = QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(this));
    QObject::connect(manager.data(), SIGNAL(finished(QNetworkReply *)), this, SLOT(urlDownloaded(QNetworkReply *)));

    QNetworkRequest request;

    request.setUrl(QUrl(address));
    request.setRawHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");


    manager->setNetworkAccessible(QNetworkAccessManager::Accessible);
    //qDebug() << QNetworkAccessManager::NetworkAccessibility();

    manager->get(request);

    return true;
}

void BoardNetwork::urlDownloaded(QNetworkReply *pReply)
{
    //qDebug() << Q_FUNC_INFO;
    if(!isTimeout) {
        htmlDownloaded = true;
        int statusCode = pReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        //qDebug() << statusCode;
        html = QString::fromUtf8(pReply->readAll());
        //qDebug() << html.length() << html;

        emit downloaded();
        //emit downloadedCorrect();
    }
}

ProcessingData BoardNetwork::getSomeData()
{
    return someData;
}

void BoardNetwork::timeoutSlot()
{
    //qDebug() << Q_FUNC_INFO;
    if(!htmlDownloaded) {
        //qDebug() << Q_FUNC_INFO;
        isTimeout = true;
        emit timeoutExpired();
    }
}

void BoardNetwork::setValue(const QVariant &var)
{
    value = var;
}

QVariant BoardNetwork::getValue() const
{
    return value;
}


