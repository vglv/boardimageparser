#ifndef REGULARS_H
#define REGULARS_H

#include <QString>


class Regulars
{
public:
    Regulars();
    QString regBoard;
    QString regName;
    QString regPostsPerHour;
    QString regUnicIps;
    QString regDescription;
    QString regPostsCount;
};

#endif // REGULARS_H
