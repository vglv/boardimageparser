#ifndef PROCESSINGDATA_H
#define PROCESSINGDATA_H
#include <QVector>
#include <QDebug>
#include "board.h"

class ProcessingData
{
public:
    ProcessingData();
    ~ProcessingData();

    void setValue(const QVariant& var);
    QVariant getValue() const;

    int htmlLength;
private:

    QVariant value;
};

#endif // PROCESSINGDATA_H
