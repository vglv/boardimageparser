#ifndef BOARDNETWORK_H
#define BOARDNETWORK_H

#include <QObject>
#include <QString>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QUrl>
#include <QTimer>
#include <QRegularExpression>
#include "utils/MainUtils.h"
#include "regulars.h"
#include "board.h"
#include "thread.h"
#include "processingdata.h"
#include "utils/json.h"

class BoardNetwork : public QObject
{
	Q_OBJECT
public:
    BoardNetwork();
    ~BoardNetwork();
    virtual bool getHTML();

    void setAddress(const QString &str);
    QString getAddress();
    void setTimeout(int value);
    int getTimeout();

    void setPhantomjsPrefix(const QString &str);
    QString getPhantomjsPrefix();

    Regulars regulars;
	
    ProcessingData getSomeData();

    void setValue(const QVariant& var);
    QVariant getValue() const;

signals:
    void downloaded();
	void downloadedCorrect();
	void downloadedError();
	void timeoutExpired();

private slots:
    void urlDownloaded(QNetworkReply* pReply);
    void timeoutSlot();
protected:
    QString html;

    QString address;
    int timeout;

    QString phantomjs_prefix;

    QSharedPointer<QNetworkAccessManager> manager;
    ProcessingData someData;

    bool isTimeout = false;
    bool htmlDownloaded = false;

    QVariant value;

};

#endif // BOARDNETWORK_H
