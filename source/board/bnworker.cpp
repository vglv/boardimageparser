#include "bnworker.h"

BNWorker::BNWorker(const SessionTask &temp_task)
{
    task = temp_task;
}

BNWorker::~BNWorker()
{
    //qDebug() << Q_FUNC_INFO;
}

qint64 BNWorker::getTimestampStart() const
{
    return timestampStart;
}

void BNWorker::setPhantomjsPrefix(const QString &str)
{
    phantomjs_prefix = str;
}

QString BNWorker::getPhantomjsPrefix()
{
    return phantomjs_prefix;
}

void BNWorker::process()
{
    timestampStart = MainUtils::getContext()->getCurrentTimestamp();
    task.setTimestampStart(timestampStart);
    emit setTimestampStart(task.getID(), timestampStart);

    emit changeStatus(task.getID(), TaskStatus::TS_INPROCESS);
    //qDebug() << Q_FUNC_INFO;
    switch(task.bnType) {
        case BNType::BN_GETBOARDS: {
            //bn = new BoardsBoardNetwork();

            bn = QSharedPointer<BoardNetwork>(new BoardsBoardNetwork());

            bn->regulars = regulars;

            connect(bn.data(), SIGNAL(downloadedError()), this, SLOT(downloadedErrorSlot()));
            connect(bn.data(), SIGNAL(downloadedCorrect()), this, SLOT(downloadedCorrectSlot()));
            connect(bn.data(), SIGNAL(timeoutExpired()), this, SLOT(timeoutExpiredSlot()));

            bn->setAddress(task.getAddress());
            bn->setTimeout(task.getTimeout());
            bn->setPhantomjsPrefix(getPhantomjsPrefix());
            bn->getHTML();
            //qDebug() << Q_FUNC_INFO << "BN_GETBOARDS";

            break;
        }
        case BNType::BN_GETTHREADSBYBOARD: {
            bn = QSharedPointer<BoardNetwork>(new CatalogBoardNetwork());

            connect(bn.data(), SIGNAL(downloadedError()), this, SLOT(downloadedErrorSlot()));
            connect(bn.data(), SIGNAL(downloadedCorrect()), this, SLOT(downloadedCorrectSlot()));
            connect(bn.data(), SIGNAL(timeoutExpired()), this, SLOT(timeoutExpiredSlot()));

            bn->setAddress(task.getAddress());
            bn->setTimeout(task.getTimeout());
            bn->setValue(task.getValue());

            //bn->setPhantomjsPrefix(getPhantomjsPrefix());
            bn->getHTML();
            //qDebug() << Q_FUNC_INFO << "BN_GETTHREADSBYBOARD";

            break;
        }
        case BNType::BN_GETPOSTSBYTHREAD: {
            bn = QSharedPointer<BoardNetwork>(new PostsBoardNetwork());

            connect(bn.data(), SIGNAL(downloadedError()), this, SLOT(downloadedErrorSlot()));
            connect(bn.data(), SIGNAL(downloadedCorrect()), this, SLOT(downloadedCorrectSlot()));
            connect(bn.data(), SIGNAL(timeoutExpired()), this, SLOT(timeoutExpiredSlot()));

            bn->setAddress(task.getAddress());
            bn->setTimeout(task.getTimeout());
            bn->setValue(task.getValue());

            //bn->setPhantomjsPrefix(getPhantomjsPrefix());
            bn->getHTML();
            //qDebug() << Q_FUNC_INFO << "BN_GETTHREADSBYBOARD";

            break;

        }
        case BNType::BN_GETTHREAD: {
            bn = QSharedPointer<BoardNetwork>(new ThreadBoardNetwork());

            connect(bn.data(), SIGNAL(downloadedError()), this, SLOT(downloadedErrorSlot()));
            connect(bn.data(), SIGNAL(downloadedCorrect()), this, SLOT(downloadedCorrectSlot()));
            connect(bn.data(), SIGNAL(timeoutExpired()), this, SLOT(timeoutExpiredSlot()));

            bn->setAddress(task.getAddress());
            bn->setTimeout(task.getTimeout());
            bn->setValue(task.getValue());

            //qDebug() << Q_FUNC_INFO << "******" << task.getAddress() << task.getID() << task.getIndex();
            bn->getHTML();

            break;

        }
        case BNType::BN_GETFILE: {
            bn = QSharedPointer<BoardNetwork>(new FileBoardNetwork());

            connect(bn.data(), SIGNAL(downloadedError()), this, SLOT(downloadedErrorSlot()));
            connect(bn.data(), SIGNAL(downloadedCorrect()), this, SLOT(downloadedCorrectSlot()));
            connect(bn.data(), SIGNAL(timeoutExpired()), this, SLOT(timeoutExpiredSlot()));

            bn->setAddress(task.getAddress());
            bn->setTimeout(task.getTimeout());
            bn->setValue(task.getValue());

            bn->getHTML();

            break;

        }
        default: {
            //emit changeStatus(task.getID(), TaskStatus::FINISHED_SUCCESS);
            //emit finished();
            delete this;
            return;
        }
    }
    return;
}

SessionTask BNWorker::getSessionTask() const
{
    return task;
}

void BNWorker::stop()
{
    if(bn.data() != NULL) {
        //bn->stop();
    }
    return;
}

void BNWorker::downloadedErrorSlot()
{
    timestampStop = MainUtils::getContext()->getCurrentTimestamp();
    emit setTimestampStop(task.getID(), timestampStop);
    task.setTimestampStop(timestampStop);
    emit changeStatus(task.getID(), TaskStatus::TS_FINISHEDERROR);
    emit finished();
    emit finishedError(task.getID());
    isFinished = true;
}

void BNWorker::downloadedCorrectSlot()
{
    /*if(task.bnType == BN_GETTHREAD)
    {
        qDebug() << Q_FUNC_INFO << "======" << task.getAddress() << task.getID() << task.getIndex();
    }*/

    timestampStop = MainUtils::getContext()->getCurrentTimestamp();
    emit setTimestampStop(task.getID(), timestampStop);
    task.setTimestampStop(timestampStop);
    task.someData = bn->getSomeData();
    emit changeStatus(task.getID(), TaskStatus::TS_FINISHEDSUCCESS);
    emit finished();
    emit finishedSuccess(task.getID(), task.someData);
    isFinished = true;
}

void BNWorker::timeoutExpiredSlot()
{
    timestampStop = MainUtils::getContext()->getCurrentTimestamp();
    emit setTimestampStop(task.getID(), timestampStop);
    task.setTimestampStop(timestampStop);
    emit changeStatus(task.getID(), TaskStatus::TS_TIMEOUT);
    emit finished();
    emit finishedTimeout(task.getID());
    isFinished = true;
}
