#include "board.h"

Board::Board()
{
}

void Board::setBrd(const QString &str)
{
    brd = str;
}

QString Board::getBrd() const
{
    return brd;
}

void Board::setName(const QString &str)
{
    name = str;
}

QString Board::getName() const
{
    return name;
}

void Board::setPostsPerHour(int value)
{
    posts_per_hour = value;
}

int Board::getPostsPerHour() const
{
    return posts_per_hour;
}

void Board::setUnicIps(int value)
{
    unic_ips = value;
}

int Board::getUnicIps() const
{
    return unic_ips;
}

void Board::setDescription(const QString &str)
{
    description = str;
}

QString Board::getDescription() const
{
    return description;
}

void Board::setPostsCount(QString value)
{
    posts_count = value;
}

QString Board::getPostsCount() const
{
    return posts_count;
}
