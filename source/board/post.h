#ifndef POST_H
#define POST_H

#include <QString>
#include <QStringList>
#include <QMetaType>

class Post
{
public:
    Post();

    void setComment(const QString& str);
    QString getComment() const;
    void setSubject(const QString& str);
    QString getSubject() const;
    void setDate(const QString& str);
    QString getDate() const;
    void setEmail(const QString& str);
    QString getEmail() const;
    void setName(const QString& str);
    QString getName() const;
    void setNum(const QString& str);
    QString getNum() const;
    void setNumber(int value);
    int getNumber() const;

    void setAddress(const QString& str);
    QString getAddress() const;

    QString getThread() const;
    QString getBoard() const;
    QString getThreadNum() const;

    void setSaved(int value);
    int getSaved() const;

    void setDeleted(int value);
    int getDeleted() const;

private:
    QString comment;
    QString subject;
    QString date;
    QString email;
    QString name;
    QString num;
    int number;

    QString address;

    int saved;
    int deleted;
};

class PostsList
{
public:
    std::vector<Post> posts;
};

Q_DECLARE_METATYPE( PostsList )
#endif // THREAD_H
