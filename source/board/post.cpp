#include "post.h"

Post::Post()
{
}

void Post::setComment(const QString &str)
{
    comment = str;
}

QString Post::getComment() const
{
    return comment;
}

void Post::setSubject(const QString &str)
{
    subject = str;
}

QString Post::getSubject() const
{
    return subject;
}

void Post::setDate(const QString &str)
{
    date = str;
}

QString Post::getDate() const
{
    return date;
}

void Post::setEmail(const QString &str)
{
    email = str;
}

QString Post::getEmail() const
{
    return email;
}

void Post::setName(const QString &str)
{
    name = str;
}

QString Post::getName() const
{
    return name;
}

void Post::setNum(const QString &str)
{
    num = str;
}

QString Post::getNum() const
{
    return num;
}

void Post::setNumber(int value)
{
    number = value;
}

int Post::getNumber() const
{
    return number;
}

void Post::setAddress(const QString &str)
{
    address = str;
}

QString Post::getAddress() const
{
    return address;
}

QString Post::getThread() const
{
    return address.mid(0, address.indexOf('#'));
}

QString Post::getThreadNum() const
{
    QStringList strs = address.split("/");
    return strs[5].mid(0, strs[5].indexOf('.'));
}

QString Post::getBoard() const
{
    QStringList strs = address.split("/");
    return strs[3];
}

void Post::setSaved(int value)
{
    saved = value;
}

int Post::getSaved() const
{
    return saved;
}

void Post::setDeleted(int value)
{
    deleted = value;
}

int Post::getDeleted() const
{
    return deleted;
}
