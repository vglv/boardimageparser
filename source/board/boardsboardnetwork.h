﻿#ifndef BOARDSBOARDNETWORK_H
#define BOARDSBOARDNETWORK_H

#include <QProcess>
#include <QCoreApplication>
#include <QFile>
#include "boardnetwork.h"
#include "board.h"

class BoardsBoardNetwork : public BoardNetwork
{
    Q_OBJECT
public:
    BoardsBoardNetwork();
    ~BoardsBoardNetwork();
    bool getHTML();

    void parseHTML();
private slots:
    void htmlDownloadedSlot(const QString &filePath);
    void timeoutSlot();
    void fileWait();
private:
    QProcess process;
    QString filePath;
    QVector<Board> boards;
	QString regular;

    static void doDeleteLayer(QObject *obj);
};

#endif // BOARDSBOARDNETWORK_H
