#include "boardswidget.h"
#include "ui_boardswidget.h"

boardsWidget::boardsWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::boardsWidget)
{
    ui->setupUi(this);

    this->setLayout(this->ui->verticalLayout);

    model = NULL;
}

void boardsWidget::getBoards()
{
    qDebug() << Q_FUNC_INFO;
    qDebug() << AppContext::getContext()->db.getDBName();

    model = new QStandardItemModel(0, 6, this);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Доска")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Название")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("П/ч")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Уник IP")));
    model->setHorizontalHeaderItem(4, new QStandardItem(QString("Описание")));
    model->setHorizontalHeaderItem(5, new QStandardItem(QString("Постов")));
    model->setHorizontalHeaderItem(6, new QStandardItem(QString("Время")));

    changedColor.setRgb(0xFF, 0xCC, 0x66);

    ui->tvBoards->setModel(model);

    QVector<QPair<Board, QString>> boards = AppContext::getContext()->dbShell.getBoards();
    for(int i = 0; i < boards.size(); i++) {
            QList<QStandardItem*> items;
            items.append(new QStandardItem(boards[i].first.getBrd()));
            items.append(new QStandardItem(boards[i].first.getName()));
            items.append(new QStandardItem(QString::number(boards[i].first.getPostsPerHour())));
            items.append(new QStandardItem(QString::number(boards[i].first.getUnicIps())));
            items.append(new QStandardItem(boards[i].first.getDescription()));
            items.append(new QStandardItem(boards[i].first.getPostsCount()));
            items.append(new QStandardItem(boards[i].second));

            model->appendRow(items);
    }
}

boardsWidget::~boardsWidget()
{
    delete ui;
}

void boardsWidget::setGettingBoardsTaskOn(bool value) {
    gettingBoardsTaskOn = value;
}

bool boardsWidget::getGettingBoardsTaskOn() const {
    return gettingBoardsTaskOn;
}
void boardsWidget::setGettingBoardsTimestampStart(qint64 value) {
    gettingBoardsTimestampStart = value;
}

qint64 boardsWidget::getGettingBoardsTimestampStart() const {
    return gettingBoardsTimestampStart;
}
void boardsWidget::setGettingBoardsTaskTimeout(int value) {
    gettingBoardsTaskTimeout = value;
}

int boardsWidget::getGettingBoardsTaskTimeout() const {
    return gettingBoardsTaskTimeout;
}

void boardsWidget::setPBBoardsBalue(int value) {
    ui->pbBoardsGet->setValue(value);
}

void boardsWidget::resetGettingBoardsProgressBar()
{
    if(gettingBoardsTaskOn) {
        qint64 timestamp = MainUtils::getContext()->getCurrentTimestamp();
        int diff = (int)(timestamp - gettingBoardsTimestampStart);
        int position = (int)((double)diff / (double)gettingBoardsTaskTimeout * 100.0);
        ui->pbBoardsGet->setValue(position);
        QTimer::singleShot(50, this, SLOT(resetGettingBoardsProgressBar()));
    }
}

void boardsWidget::startedTask(SessionTask task)
{
    gettingBoardsTaskOn = true;
    gettingBoardsTaskTimeout = task.getTimeout();
    gettingBoardsTimestampStart = MainUtils::getContext()->getCurrentTimestamp();
    ui->pbBoardsGet->setValue(0);
    QTimer::singleShot(0, this, SLOT(resetGettingBoardsProgressBar()));
}

void boardsWidget::finishedTask(SessionTask task)
{
    gettingBoardsTaskOn = false;
    ui->pbBoardsGet->setValue(100);

    if(task.taskStatus == TaskStatus::TS_FINISHEDSUCCESS)
    {

        QVector<Board> tempBoards;
        QVector<Board> boards;
        std::vector<Board> stdBoards = task.someData.getValue().value< BoardsList >().boards;

        for(int i = 0; i < stdBoards.size(); i++) {
            tempBoards.append(stdBoards[i]);
            boards.append(stdBoards[i]);
        }

        QVector<int> indexes;

        for(int i = 0; i < model->rowCount(); i++)
        {
            QString brdTemp =  model->item(i, 0)->text();
            bool found = false;
            for(int j = 0; j < tempBoards.size(); j++)
            {
                if(tempBoards[j].getBrd() == brdTemp) {
                    found = true;
                    break;
                }
            }
            if(!found) {
                //ui->tvBoards->selectRow(i);
                indexes.append(i);
                //qDebug() << "SELECTED" << i << brdTemp;
            }
        }

        //QModelIndexList indexes = ui->tvBoards->selectionModel()->selectedRows();
        try
        {
            for(int i = indexes.size() - 1; i >= 0; i--)
            {
                model->removeRows(indexes[i], 1);
                indexes.removeAt(i);
            }
        }
        catch(std::exception e) {
            qDebug() << e.what();
        }




        for(int i = 0; i < boards.size(); i++) {
            QList<QStandardItem*> foundItems = model->findItems(boards[i].getBrd());

            if(foundItems.size() == 0) {
                QList<QStandardItem*> items;
                items.append(new QStandardItem(boards[i].getBrd()));
                items.append(new QStandardItem(boards[i].getName()));
                items.append(new QStandardItem(QString::number(boards[i].getPostsPerHour())));
                items.append(new QStandardItem(QString::number(boards[i].getUnicIps())));
                items.append(new QStandardItem(boards[i].getDescription()));
                items.append(new QStandardItem(boards[i].getPostsCount()));
                items.append(new QStandardItem(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())));
                model->appendRow(items);
            }
            else {
                int temp_index = foundItems[0]->row();

                if(model->item(temp_index, 1)->text() != boards[i].getName()) {
                    model->setItem(temp_index, 1, new QStandardItem(boards[i].getName()));
                    itemChanged(1, temp_index);
                }
                if(model->item(temp_index, 2)->text() != QString::number(boards[i].getPostsPerHour())) {
                    model->setItem(temp_index, 2, new QStandardItem(QString::number(boards[i].getPostsPerHour())));
                    itemChanged(2, temp_index);
                }
                if(model->item(temp_index, 3)->text() != QString::number(boards[i].getUnicIps())) {
                    model->setItem(temp_index, 3, new QStandardItem(QString::number(boards[i].getUnicIps())));
                    itemChanged(3, temp_index);
                }
                if(model->item(temp_index, 4)->text() != boards[i].getDescription()) {
                    model->setItem(temp_index, 4, new QStandardItem(boards[i].getDescription()));
                    itemChanged(4, temp_index);
                }
                if(model->item(temp_index, 5)->text() != boards[i].getPostsCount()) {
                    model->setItem(temp_index, 5, new QStandardItem(boards[i].getPostsCount()));
                    itemChanged(5, temp_index);
                }

                model->setItem(temp_index, 6, new QStandardItem(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())));
                itemChanged(6, temp_index);

            }
        }
    }
}

void boardsWidget::itemChanged(int col, int row)
{
    model->item(row, col)->setBackground(changedColor);
    itemChanging *tempItem = new itemChanging();
    tempItem->col = col;
    tempItem->row = row;
    tempItem->ms = 4000;
    tempItem->started = MainUtils::getContext()->getCurrentTimestamp();
    tempItem->parent = this;
    tempItem->color = changedColor;
    tempItem->startTimer();
}

void itemChanging::startTimer()
{
    QTimer::singleShot(20, this, SLOT(changeColorSlot()));
}

void itemChanging::changeColorSlot()
{
    qint64 current = MainUtils::getContext()->getCurrentTimestamp();
    if(current - started >= ms)
    {
        QColor tempColor(255, 255, 255);
        parent->model->item(row, col)->setBackground(tempColor);
        delete this;
    }
    else
    {
        int diff = (int)(current - started);
        int r = color.red() + (int)(((double)diff / (double)ms) * ((double)255 - (double)color.red()));
        int g = color.green() + (int)(((double)diff / (double)ms) * ((double)255 - (double)color.green()));
        int b = color.blue() + (int)(((double)diff / (double)ms) * ((double)255 - (double)color.blue()));
        QColor newColor(r, g, b);
        parent->model->item(row, col)->setBackground(newColor);
        QTimer::singleShot(40, this, SLOT(changeColorSlot()));
    }


}
