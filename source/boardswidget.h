#ifndef BOARDSWIDGET_H
#define BOARDSWIDGET_H

#include <QDialog>
#include <QColor>
#include <QTimer>
#include <QStandardItemModel>
#include <QTableWidgetItem>
#include "core/AppContext.h"

namespace Ui {
class boardsWidget;
}


class boardsWidget : public QDialog
{
    Q_OBJECT

    friend class itemChanging;
public:
    explicit boardsWidget(QWidget *parent = 0);
    ~boardsWidget();

    void setGettingBoardsTaskOn(bool value);
    bool getGettingBoardsTaskOn() const;
    void setGettingBoardsTimestampStart(qint64 value);
    qint64 getGettingBoardsTimestampStart() const;
    void setGettingBoardsTaskTimeout(int value);
    int getGettingBoardsTaskTimeout() const;

    void setPBBoardsBalue(int value);

    void startedTask(SessionTask task);
    void finishedTask(SessionTask task);
    void getBoards();



public slots:
    void resetGettingBoardsProgressBar();


private slots:

private:
    Ui::boardsWidget *ui;
    QStandardItemModel *model;

    bool gettingBoardsTaskOn = false;
    qint64 gettingBoardsTimestampStart;
    int gettingBoardsTaskTimeout;




    void itemChanged(int col, int row);
    QColor changedColor;
};

class itemChanging : public QObject
{
    Q_OBJECT

public:
    itemChanging() {};
    ~itemChanging() {};
    boardsWidget *parent;
    int col;
    int row;
    int ms;
    qint64 started;
    QColor color;
    void startTimer();
public slots:
    void changeColorSlot();
};

#endif // BOARDSWIDGET_H
