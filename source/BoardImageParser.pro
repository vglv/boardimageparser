#-------------------------------------------------
#
# Project created by QtCreator 2017-10-30T14:27:32
#
#-------------------------------------------------

QT       += core gui sql network concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BoardImageParser
TEMPLATE = app

INCLUDEPATH += C:/OpenSSL-Win32/include/
INCLUDEPATH += C:/OpenSSL-Win32/include/openssl/
LIBS += -LC:/OpenSSL-Win32/lib -lubsec

SOURCES += main.cpp\
        mainwindow.cpp \
    core/AppContext.cpp \
    db/db.cpp \
    db/table.cpp \
    utils/MainUtils.cpp \
    utils/myexception.cpp \
    utils/xmlprocessor.cpp \
    db/column.cpp \
    board/boardnetwork.cpp \
    board/boardsboardnetwork.cpp \
    board/bnworker.cpp \
    board/board.cpp \
    core/session.cpp \
    core/sessiontask.cpp \
    board/regulars.cpp \
    utils/garbageutilizer.cpp \
    board/processingdata.cpp \
    db/value.cpp \
    db/row.cpp \
    boardswidget.cpp \
    core/task.cpp \
    core/tasks.cpp \
    taskswidget.cpp \
    addtaskwidget.cpp \
    board/catalogboardnetwork.cpp \
    board/thread.cpp \
    utils/json.cpp \
    db/dbshell.cpp \
    board/post.cpp \
    board/postsboardnetwork.cpp \
    board/threadboardnetwork.cpp \
    board/fileboardnetwork.cpp

HEADERS  += mainwindow.h \
    core/AppContext.h \
    db/db.h \
    db/table.h \
    utils/MainUtils.h \
    utils/myexception.h \
    utils/xmlprocessor.h \
    db/column.h \
    board/boardnetwork.h \
    board/boardsboardnetwork.h \
    board/bnworker.h \
    board/board.h \
    core/session.h \
    core/sessiontask.h \
    core/enums.h \
    board/regulars.h \
    utils/garbageutilizer.h \
    board/processingdata.h \
    db/value.h \
    db/row.h \
    boardswidget.h \
    core/task.h \
    core/tasks.h \
    taskswidget.h \
    addtaskwidget.h \
    board/catalogboardnetwork.h \
    board/thread.h \
    utils/json.h \
    db/dbshell.h \
    board/post.h \
    board/postsboardnetwork.h \
    board/threadboardnetwork.h \
    board/fileboardnetwork.h

FORMS    += mainwindow.ui \
    boardswidget.ui \
    taskswidget.ui \
    addtaskwidget.ui

RESOURCES += \
    res.qrc

DISTFILES += \
    BoardImageParser.pro.user.3147cf3 \
    BoardImageParser.pro.user.ab4728b
