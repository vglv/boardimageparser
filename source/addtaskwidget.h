#ifndef ADDTASK_H
#define ADDTASK_H

#include <QWidget>
#include <QDebug>
#include <QStandardItemModel>
#include <QTableWidgetItem>
#include <QMessageBox>
#include <QVector>
#include "core/AppContext.h"
#include "core/tasks.h"

namespace Ui {
class AddTask;
}

class addTaskWidget : public QWidget
{
    Q_OBJECT

public:
    explicit addTaskWidget(QWidget *parent = 0);
    ~addTaskWidget();

    void getBoards();
    void cleanSearch();
    void setModeChange(bool isChange);
    void setChangingTaskID(const QString &id);
    void setSearchString(const QString &searchString);
    void setBoardsSelection(const QStringList &boards);

private slots:
    void on_pbSelectAll_clicked();

    void on_pbDeselectAll_clicked();

    void on_pbCancel_clicked();

    void on_pbAdd_clicked();

private:
    Ui::AddTask *ui;
    QStandardItemModel *model;
    bool isModeChange;
    QString changingTaskID;
signals:
    void taskAdded();
    void taskChanged();
};

#endif // ADDTASK_H
