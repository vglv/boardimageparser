#include "dbshell.h"

DBShell::DBShell(QObject *parent) : QObject(parent)
{
}

DBShell::~DBShell()
{
}

void DBShell::setDB(DB *pDB)
{
    db = pDB;
}

bool DBShell::executeQuery(const QString& query)
{
    return db->executeQuery(query);
}

QStringList DBShell::generateUpdateInsertRow(Row row)
{
    QStringList qsList;

    bool isPrimary = false;
    int index = -1;
    for(int i = 0; i < row.columns.size(); i++)
    {
        if(row.columns[i].getIsPrimaryKey())
        {
            isPrimary = true;
            index = i;
            break;
        }
    }

    QString str1 = "";

    if(isPrimary)
        str1 += "INSERT OR IGNORE INTO " + row.getName() + "(";
    else
        str1 += "INSERT INTO " + row.getName() + "(";

    for(int i = 0; i < row.columns.size(); i++) {
        if(i < row.columns.size() - 1)
            str1 += row.columns[i].getName() + ", ";
        else
            str1 += row.columns[i].getName() + ") VALUES(";
    }
    for(int i = 0; i < row.columns.size(); i++) {
        if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_INTEGER)
        {
            if(i < row.columns.size() - 1)
                str1 += QString::number(row.values[i].getIntegerValue()) + ", ";
            else
                str1 += QString::number(row.values[i].getIntegerValue()) + "); ";
        }
        if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_REAL)
        {
            if(i < row.columns.size() - 1)
                str1 += QString::number(row.values[i].getRealValue()) + ", ";
            else
                str1 += QString::number(row.values[i].getRealValue()) + "); ";
        }
        if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_TEXT)
        {
            if(i < row.columns.size() - 1) {
                QString tempString = row.values[i].getTextValue();
                str1 += "'" + tempString.replace("'", "''") + "', ";
            }
            else
            {
                QString tempString = row.values[i].getTextValue();
                str1 += "'" + tempString.replace("'", "''") + "'); ";
            }
        }
    }

    qsList.append(str1);

    if(isPrimary)
    {
        QString str2 = "";
        for(int i = row.values.size() - 1; i >= 0; i--) {
            if(row.values[i].getMarked()) {
                row.values.removeAt(i);
                row.columns.removeAt(i);
            }
        }

        str2 += "UPDATE " + row.getName() + " SET ";
        for(int i = 0; i < row.columns.size(); i++)
        {
            if(i == index)
                continue;

            if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_INTEGER)
            {
                if(i < row.columns.size() - 1)
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getIntegerValue()) + ", ";
                else
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getIntegerValue()) + " WHERE ";
            }
            if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_REAL)
            {
                if(i < row.columns.size() - 1)
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getRealValue()) + ", ";
                else
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getRealValue()) + " WHERE ";
            }
            if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_TEXT)
            {
                if(i < row.columns.size() - 1) {
                    QString tempString = row.values[i].getTextValue();
                    str2 += row.columns[i].getName() + " = " + "'" + tempString.replace("'", "''") + "', ";
                }
                else {
                    QString tempString = row.values[i].getTextValue();
                    str2 += row.columns[i].getName() + " = " + "'" + tempString.replace("'", "''") + "' WHERE ";
                }
            }
        }

        if(row.columns[index].getColumnType() == SqliteTypesEnum::SQ_INTEGER)
            str2 += row.columns[index].getName() += " = " + QString::number(row.values[index].getIntegerValue()) + ";";
        else if(row.columns[index].getColumnType() == SqliteTypesEnum::SQ_REAL)
            str2 += row.columns[index].getName() += " = " + QString::number(row.values[index].getRealValue()) + ";";
        else if(row.columns[index].getColumnType() == SqliteTypesEnum::SQ_TEXT) {
            QString tempString = row.values[index].getTextValue();
            str2 += row.columns[index].getName() += " = '" + tempString.replace("'", "''") + "';";
        }

        qsList.append(str2);

    }

    return qsList;
}

QStringList DBShell::generatePostUpdateInsertRow(Row row)
{
    QStringList qsList;

    bool isPrimary = false;
    int index = -1;
    for(int i = 0; i < row.columns.size(); i++)
    {
        if(row.columns[i].getIsPrimaryKey())
        {
            isPrimary = true;
            index = i;
            break;
        }
    }

    QString str1 = "";

    if(isPrimary)
        str1 += "INSERT OR IGNORE INTO " + row.getName() + "(";
    else
        str1 += "INSERT INTO " + row.getName() + "(";

    for(int i = 0; i < row.columns.size(); i++) {
        if(i < row.columns.size() - 1)
            str1 += row.columns[i].getName() + ", ";
        else
            str1 += row.columns[i].getName() + ") VALUES(";
    }
    for(int i = 0; i < row.columns.size(); i++) {
        if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_INTEGER)
        {
            if(i < row.columns.size() - 1)
                str1 += QString::number(row.values[i].getIntegerValue()) + ", ";
            else
                str1 += QString::number(row.values[i].getIntegerValue()) + "); ";
        }
        if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_REAL)
        {
            if(i < row.columns.size() - 1)
                str1 += QString::number(row.values[i].getRealValue()) + ", ";
            else
                str1 += QString::number(row.values[i].getRealValue()) + "); ";
        }
        if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_TEXT)
        {
            if(i < row.columns.size() - 1) {
                QString tempString = row.values[i].getTextValue();
                str1 += "'" + tempString.replace("'", "''") + "', ";
            }
            else
            {
                QString tempString = row.values[i].getTextValue();
                str1 += "'" + tempString.replace("'", "''") + "'); ";
            }
        }
    }

    qsList.append(str1);

    if(isPrimary)
    {
        QString str2 = "";
        for(int i = row.values.size() - 1; i >= 0; i--) {
            if(row.values[i].getMarked()) {
                row.values.removeAt(i);
                row.columns.removeAt(i);
            }
        }

        str2 += "UPDATE " + row.getName() + " SET ";
        for(int i = 0; i < row.columns.size(); i++)
        {
            if(i == index)
                continue;

            if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_INTEGER)
            {
                if(i < row.columns.size() - 1)
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getIntegerValue()) + ", ";
                else
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getIntegerValue()) + " WHERE ";
            }
            if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_REAL)
            {
                if(i < row.columns.size() - 1)
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getRealValue()) + ", ";
                else
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getRealValue()) + " WHERE ";
            }
            if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_TEXT)
            {
                if(i < row.columns.size() - 1) {
                    if(row.columns[i].getName().toLower() == "deleted")
                        continue;
                    QString tempString = row.values[i].getTextValue();
                    str2 += row.columns[i].getName() + " = " + "'" + tempString.replace("'", "''") + "', ";
                }
                else {
                    if(row.columns[i].getName().toLower() == "deleted")
                        continue;
                    QString tempString = row.values[i].getTextValue();
                    str2 += row.columns[i].getName() + " = " + "'" + tempString.replace("'", "''") + "' WHERE ";
                }
            }
        }

        if(row.columns[index].getColumnType() == SqliteTypesEnum::SQ_INTEGER)
            str2 += row.columns[index].getName() += " = " + QString::number(row.values[index].getIntegerValue()) + ";";
        else if(row.columns[index].getColumnType() == SqliteTypesEnum::SQ_REAL)
            str2 += row.columns[index].getName() += " = " + QString::number(row.values[index].getRealValue()) + ";";
        else if(row.columns[index].getColumnType() == SqliteTypesEnum::SQ_TEXT) {
            QString tempString = row.values[index].getTextValue();
            str2 += row.columns[index].getName() += " = '" + tempString.replace("'", "''") + "';";
        }

        qsList.append(str2);

    }

    return qsList;
}

QStringList DBShell::generateThreadUpdateInsertRow(Row row)
{
    QStringList qsList;

    bool isPrimary = false;
    int index = -1;
    for(int i = 0; i < row.columns.size(); i++)
    {
        if(row.columns[i].getIsPrimaryKey())
        {
            isPrimary = true;
            index = i;
            break;
        }
    }

    QString str1 = "";

    if(isPrimary)
        str1 += "INSERT OR IGNORE INTO " + row.getName() + "(";
    else
        str1 += "INSERT INTO " + row.getName() + "(";

    for(int i = 0; i < row.columns.size(); i++) {
        if(i < row.columns.size() - 1)
            str1 += row.columns[i].getName() + ", ";
        else
            str1 += row.columns[i].getName() + ") VALUES(";
    }
    for(int i = 0; i < row.columns.size(); i++) {
        if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_INTEGER)
        {
            if(i < row.columns.size() - 1)
                str1 += QString::number(row.values[i].getIntegerValue()) + ", ";
            else
                str1 += QString::number(row.values[i].getIntegerValue()) + "); ";
        }
        if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_REAL)
        {
            if(i < row.columns.size() - 1)
                str1 += QString::number(row.values[i].getRealValue()) + ", ";
            else
                str1 += QString::number(row.values[i].getRealValue()) + "); ";
        }
        if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_TEXT)
        {
            if(i < row.columns.size() - 1) {
                QString tempString = row.values[i].getTextValue();
                str1 += "'" + tempString.replace("'", "''") + "', ";
            }
            else
            {
                QString tempString = row.values[i].getTextValue();
                str1 += "'" + tempString.replace("'", "''") + "'); ";
            }
        }
    }

    qsList.append(str1);

    if(isPrimary)
    {
        QString str2 = "";
        for(int i = row.values.size() - 1; i >= 0; i--) {
            if(row.values[i].getMarked()) {
                row.values.removeAt(i);
                row.columns.removeAt(i);
            }
        }

        str2 += "UPDATE " + row.getName() + " SET ";
        for(int i = 0; i < row.columns.size(); i++)
        {
            if(i == index)
                continue;

            if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_INTEGER)
            {
                if(i < row.columns.size() - 1)
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getIntegerValue()) + ", ";
                else
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getIntegerValue()) + " WHERE ";
            }
            if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_REAL)
            {
                if(i < row.columns.size() - 1)
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getRealValue()) + ", ";
                else
                    str2 += row.columns[i].getName() + " = " + QString::number(row.values[i].getRealValue()) + " WHERE ";
            }
            if(row.columns[i].getColumnType() == SqliteTypesEnum::SQ_TEXT)
            {
                if(i < row.columns.size() - 1) {
                    if(row.columns[i].getName().toLower() == "deleted")
                        continue;
                    QString tempString = row.values[i].getTextValue();
                    str2 += row.columns[i].getName() + " = " + "'" + tempString.replace("'", "''") + "', ";
                }
                else {
                    if(row.columns[i].getName().toLower() == "deleted")
                        continue;
                    QString tempString = row.values[i].getTextValue();
                    str2 += row.columns[i].getName() + " = " + "'" + tempString.replace("'", "''") + "' WHERE ";
                }
            }
        }

        if(row.columns[index].getColumnType() == SqliteTypesEnum::SQ_INTEGER)
            str2 += row.columns[index].getName() += " = " + QString::number(row.values[index].getIntegerValue()) + ";";
        else if(row.columns[index].getColumnType() == SqliteTypesEnum::SQ_REAL)
            str2 += row.columns[index].getName() += " = " + QString::number(row.values[index].getRealValue()) + ";";
        else if(row.columns[index].getColumnType() == SqliteTypesEnum::SQ_TEXT) {
            QString tempString = row.values[index].getTextValue();
            str2 += row.columns[index].getName() += " = '" + tempString.replace("'", "''") + "';";
        }

        qsList.append(str2);

    }

    return qsList;
}

QString DBShell::generateCreateTableSQL(Table table)
{
    QString str = QString("CREATE TABLE " + table.getName() + " (");
    for(int i = 0; i < table.columns.size(); i++) {

        switch(table.columns[i].getColumnType()) {
        case SqliteTypesEnum::SQ_INTEGER:
            {
                str += table.columns[i].getName() + QString(" INTEGER");
                if(table.columns[i].getIsPrimaryKey())
                    str += QString(" PRIMARY KEY");
                if(table.columns[i].getIsNull())
                    str += QString(" NULL, ");
                else
                    str += QString(" NOT NULL, ");
                break;
            }
        case SqliteTypesEnum::SQ_REAL:
            {
                str += table.columns[i].getName() + QString(" REAL");
                if(table.columns[i].getIsPrimaryKey())
                    str += QString(" PRIMARY KEY");
                if(table.columns[i].getIsNull())
                    str += QString(" NULL, ");
                else
                    str += QString(" NOT NULL, ");
                break;
            }
        case SqliteTypesEnum::SQ_TEXT:
            {
                str += table.columns[i].getName() + QString(" TEXT");
                if(table.columns[i].getIsPrimaryKey())
                    str += QString(" PRIMARY KEY");
                if(table.columns[i].getIsNull())
                    str += QString(" NULL, ");
                else
                    str += QString(" NOT NULL, ");
                break;
            }
        case SqliteTypesEnum::SQ_BLOB:
            {
                str += table.columns[i].getName() + QString(" BLOB");
                if(table.columns[i].getIsPrimaryKey())
                    str += QString(" PRIMARY KEY");
                if(table.columns[i].getIsNull())
                    str += QString(" NULL, ");
                else
                    str += QString(" NOT NULL, ");
                break;
            }
        }
    }
    str = str.mid(0, str.length() - 2);
    str += QString(");");

    return str;
}

QString DBShell::generateDeleteBoards(QVector<Board> boards)
{
    QString result = "DELETE FROM Boards WHERE board NOT IN (";
    for(int i = 0; i < boards.size(); i++) {
        if(i < boards.size() - 1)
            result += "'" + boards[i].getBrd() + "', ";
        else
            result += "'" + boards[i].getBrd() + "');";
    }
    return result;
}

QString DBShell::generateCreateSessionSQL(QString uid, QString created)
{
    QString sql = QString("");

    sql += QString("INSERT INTO Session(uid, started) VALUES('")
         + uid
         + QString("', '")
         + created
         + QString("');");

    //qDebug() << sql;

    return sql;
}

QString DBShell::generateFinishSessionSQL(QString uid, QString finished)
{
    QString sql = QString("");
    sql += QString("UPDATE Session SET finished = '")
         + finished
         + QString("' WHERE uid='")
         + uid
         + QString("';");

    //qDebug() << sql;

    return sql;
}

QString DBShell::generateUpdateTaskSQL(QString uid, QString searchString, QString boardsList, QString last_modified)
{
    QString sql = QString("");
    sql += QString("UPDATE Tasks SET search = '")
         + searchString
         + QString("', boards = '")
         + boardsList
         + QString("', last_modified = '")
         + last_modified
         + QString("' WHERE uid = '")
         + uid
         + QString("';");

    //qDebug() << sql;

    return sql;
}

QVector<QPair<Board, QString>> DBShell::getBoards() {
    db->setIsError(false);
    QVector<QPair<Board, QString>> boards;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(QString("select * from boards"));
        QSqlRecord rec = sqlQuery.record();

        int brdCol = rec.indexOf(QString("board"));
        int nameCol = rec.indexOf(QString("name"));
        int pphCol = rec.indexOf(QString("posts_per_hour"));
        int uipCol = rec.indexOf(QString("unic_ips"));
        int descCol = rec.indexOf(QString("description"));
        int pcountCol = rec.indexOf(QString("posts_count"));
        int modifiedCol = rec.indexOf(QString("last_modified"));

        while(sqlQuery.next()) {
            QPair<Board, QString> board;
            board.first.setBrd(sqlQuery.value(brdCol).toString());
            board.first.setName(sqlQuery.value(nameCol).toString());
            board.first.setPostsPerHour(sqlQuery.value(pphCol).toInt());
            board.first.setUnicIps(sqlQuery.value(uipCol).toInt());
            board.first.setDescription(sqlQuery.value(descCol).toString());
            board.first.setPostsCount(sqlQuery.value(pcountCol).toString());
            board.second = sqlQuery.value(modifiedCol).toString();
            boards.append(board);
        }
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return boards;
    }
    return boards;
}

QVector<QPair<Task, QPair<QString, QString>>> DBShell::getTasks()
{
    db->setIsError(false);
    QVector<QPair<Task, QPair<QString, QString>>> tasks;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(QString("select * from tasks"));
        QSqlRecord rec = sqlQuery.record();

        int uidCol = rec.indexOf(QString("uid"));
        int searchCol = rec.indexOf(QString("search"));
        int boardsCol = rec.indexOf(QString("boards"));
        int createdCol = rec.indexOf(QString("created"));
        int modifiedCol = rec.indexOf(QString("last_modified"));

        while(sqlQuery.next()) {
            QPair<Task, QPair<QString, QString>> task;
            task.first.setID(sqlQuery.value(uidCol).toString());
            task.first.setSearchString(sqlQuery.value(searchCol).toString());
            task.first.setBoards(sqlQuery.value(boardsCol).toString());
            task.second.first = sqlQuery.value(createdCol).toString();
            task.second.second = sqlQuery.value(modifiedCol).toString();

            tasks.append(task);
        }
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return tasks;
    }
    return tasks;
}

QVector<QPair<Post, QPair<QString, QString>>> DBShell::getPosts()
{
    db->setIsError(false);
    QVector<QPair<Post, QPair<QString, QString>>> posts;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(QString("select * from posts"));
        QSqlRecord rec = sqlQuery.record();

        int addressCol = rec.indexOf(QString("address"));
        int commentCol = rec.indexOf(QString("comment"));
        int subjectCol = rec.indexOf(QString("subject"));
        int dateCol = rec.indexOf(QString("date"));
        int emailCol = rec.indexOf(QString("email"));
        int nameCol = rec.indexOf(QString("name"));
        int numCol = rec.indexOf(QString("num"));
        int numberCol = rec.indexOf(QString("number"));
        int searchCol = rec.indexOf(QString("search"));
        int createdCol = rec.indexOf(QString("created"));
        int savedCol = rec.indexOf(QString("saved"));
        int deletedCol = rec.indexOf(QString("deleted"));

        while(sqlQuery.next()) {
            QPair<Post, QPair<QString, QString>> post;

            post.first.setAddress(sqlQuery.value(addressCol).toString());
            post.first.setComment(sqlQuery.value(commentCol).toString());
            post.first.setSubject(sqlQuery.value(subjectCol).toString());
            post.first.setDate(sqlQuery.value(dateCol).toString());
            post.first.setEmail(sqlQuery.value(emailCol).toString());
            post.first.setName(sqlQuery.value(nameCol).toString());
            post.first.setNum(sqlQuery.value(numCol).toString());
            post.first.setNumber(sqlQuery.value(numberCol).toInt());
            post.first.setSaved(sqlQuery.value(savedCol).toInt());
            post.first.setDeleted(sqlQuery.value(deletedCol).toInt());
            post.second.first = sqlQuery.value(searchCol).toString();
            post.second.second = sqlQuery.value(createdCol).toString();

            posts.append(post);
        }
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return posts;
    }
    return posts;
}

QPair<Post, QPair<QString, QString>> DBShell::getPosts(int number)
{
    db->setIsError(false);
    QPair<Post, QPair<QString, QString>> post;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(QString("select * from posts limit 1 offset " + QString::number(number - 1)));
        QSqlRecord rec = sqlQuery.record();

        int addressCol = rec.indexOf(QString("address"));
        int commentCol = rec.indexOf(QString("comment"));
        int subjectCol = rec.indexOf(QString("subject"));
        int dateCol = rec.indexOf(QString("date"));
        int emailCol = rec.indexOf(QString("email"));
        int nameCol = rec.indexOf(QString("name"));
        int numCol = rec.indexOf(QString("num"));
        int numberCol = rec.indexOf(QString("number"));
        int searchCol = rec.indexOf(QString("search"));
        int createdCol = rec.indexOf(QString("created"));
        int savedCol = rec.indexOf(QString("saved"));
        int deletedCol = rec.indexOf(QString("deleted"));

        while(sqlQuery.next()) {

            post.first.setAddress(sqlQuery.value(addressCol).toString());
            post.first.setComment(sqlQuery.value(commentCol).toString());
            post.first.setSubject(sqlQuery.value(subjectCol).toString());
            post.first.setDate(sqlQuery.value(dateCol).toString());
            post.first.setEmail(sqlQuery.value(emailCol).toString());
            post.first.setName(sqlQuery.value(nameCol).toString());
            post.first.setNum(sqlQuery.value(numCol).toString());
            post.first.setNumber(sqlQuery.value(numberCol).toInt());
            post.first.setSaved(sqlQuery.value(savedCol).toInt());
            post.first.setDeleted(sqlQuery.value(deletedCol).toInt());
            post.second.first = sqlQuery.value(searchCol).toString();
            post.second.second = sqlQuery.value(createdCol).toString();
        }
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return post;
    }
    return post;
}

QVector<QPair<Post, QPair<QString, QString>>> DBShell::getNewPosts()
{
    db->setIsError(false);
    QVector<QPair<Post, QPair<QString, QString>>> posts;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(QString("select * from posts where exported = 0;"));
        QSqlRecord rec = sqlQuery.record();

        int addressCol = rec.indexOf(QString("address"));
        int commentCol = rec.indexOf(QString("comment"));
        int subjectCol = rec.indexOf(QString("subject"));
        int dateCol = rec.indexOf(QString("date"));
        int emailCol = rec.indexOf(QString("email"));
        int nameCol = rec.indexOf(QString("name"));
        int numCol = rec.indexOf(QString("num"));
        int numberCol = rec.indexOf(QString("number"));
        int searchCol = rec.indexOf(QString("search"));
        int createdCol = rec.indexOf(QString("created"));
        int savedCol = rec.indexOf(QString("saved"));
        int deletedCol = rec.indexOf(QString("deleted"));

        while(sqlQuery.next()) {
            QPair<Post, QPair<QString, QString>> post;

            post.first.setAddress(sqlQuery.value(addressCol).toString());
            post.first.setComment(sqlQuery.value(commentCol).toString());
            post.first.setSubject(sqlQuery.value(subjectCol).toString());
            post.first.setDate(sqlQuery.value(dateCol).toString());
            post.first.setEmail(sqlQuery.value(emailCol).toString());
            post.first.setName(sqlQuery.value(nameCol).toString());
            post.first.setNum(sqlQuery.value(numCol).toString());
            post.first.setNumber(sqlQuery.value(numberCol).toInt());
            post.first.setSaved(sqlQuery.value(savedCol).toInt());
            post.first.setDeleted(sqlQuery.value(deletedCol).toInt());
            post.second.first = sqlQuery.value(searchCol).toString();
            post.second.second = sqlQuery.value(createdCol).toString();

            posts.append(post);
        }

        sqlQuery.exec(QString("update posts set exported = 1;"));
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return posts;
    }
    return posts;
}

int DBShell::getAllPostsCount()
{
    db->setIsError(false);
    int count = 0;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(QString("select count(*) as count from posts;"));
        QSqlRecord rec = sqlQuery.record();

        int countCol = rec.indexOf(QString("count"));

        while(sqlQuery.next()) {
            count = sqlQuery.value(countCol).toInt();
        }
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return count;
    }
    return count;
}

int DBShell::getNewPostsCount()
{
    db->setIsError(false);
    int count = 0;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(QString("select count(*) as count from posts where exported = 0;"));
        QSqlRecord rec = sqlQuery.record();

        int countCol = rec.indexOf(QString("count"));

        while(sqlQuery.next()) {
            count = sqlQuery.value(countCol).toInt();
        }
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return count;
    }
    return count;
}

Thread DBShell::getThread(const QString& address)
{
    Thread thread;

    db->setIsError(false);
    QString lasthit;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(QString("select * from threads where address = '" + address + "'"));
        QSqlRecord rec = sqlQuery.record();

        int lasthitCol = rec.indexOf(QString("lasthit"));

        while(sqlQuery.next()) {
            lasthit = sqlQuery.value(lasthitCol).toString();
        }

        thread.setThreadLasthit(lasthit);
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return thread;
    }
    return thread;
}

QVector<QPair<Post, QPair<QString, QString>>> DBShell::getPosts(QString query)
{
    db->setIsError(false);
    QVector<QPair<Post, QPair<QString, QString>>> posts;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(query);
        QSqlRecord rec = sqlQuery.record();

        int addressCol = rec.indexOf(QString("address"));
        int commentCol = rec.indexOf(QString("comment"));
        int subjectCol = rec.indexOf(QString("subject"));
        int dateCol = rec.indexOf(QString("date"));
        int emailCol = rec.indexOf(QString("email"));
        int nameCol = rec.indexOf(QString("name"));
        int numCol = rec.indexOf(QString("num"));
        int numberCol = rec.indexOf(QString("number"));
        int searchCol = rec.indexOf(QString("search"));
        int createdCol = rec.indexOf(QString("created"));
        int savedCol = rec.indexOf(QString("saved"));
        int deletedCol = rec.indexOf(QString("deleted"));

        while(sqlQuery.next()) {
            QPair<Post, QPair<QString, QString>> post;

            post.first.setAddress(sqlQuery.value(addressCol).toString());
            post.first.setComment(sqlQuery.value(commentCol).toString());
            post.first.setSubject(sqlQuery.value(subjectCol).toString());
            post.first.setDate(sqlQuery.value(dateCol).toString());
            post.first.setEmail(sqlQuery.value(emailCol).toString());
            post.first.setName(sqlQuery.value(nameCol).toString());
            post.first.setNum(sqlQuery.value(numCol).toString());
            post.first.setNumber(sqlQuery.value(numberCol).toInt());
            post.first.setSaved(sqlQuery.value(savedCol).toInt());
            post.first.setDeleted(sqlQuery.value(deletedCol).toInt());
            post.second.first = sqlQuery.value(searchCol).toString();
            post.second.second = sqlQuery.value(createdCol).toString();

            posts.append(post);
        }
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return posts;
    }
    return posts;
}

QVector<QPair<Thread, QString>> DBShell::getThreads(QString query)
{
    db->setIsError(false);
    QVector<QPair<Thread, QString>> threads;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(query);
        QSqlRecord rec = sqlQuery.record();

        int addressCol = rec.indexOf(QString("address"));
        int boardCol = rec.indexOf(QString("board"));
        int numCol = rec.indexOf(QString("num"));
        int subjectCol = rec.indexOf(QString("subject"));
        int threadCreatedCol = rec.indexOf(QString("thread_created"));
        int lasthitCol = rec.indexOf(QString("lasthit"));
        int postsCountCol = rec.indexOf(QString("posts_count"));
        int filesCountCol = rec.indexOf(QString("files_count"));
        int emailCol = rec.indexOf(QString("email"));
        int createdCol = rec.indexOf(QString("created"));
        int lastSavedCol = rec.indexOf(QString("last_saved"));
        int deletedCol = rec.indexOf(QString("deleted"));

        while(sqlQuery.next()) {
            QPair<Thread, QString> thread;

            thread.first.setAddress(sqlQuery.value(addressCol).toString());
            thread.first.setBrd(sqlQuery.value(boardCol).toString());
            //thread.first.setComment();
            thread.first.setDeleted(sqlQuery.value(deletedCol).toInt());
            thread.first.setEmail(sqlQuery.value(emailCol).toString());
            thread.first.setFilesCount(sqlQuery.value(filesCountCol).toInt());
            thread.first.setLastSaved(sqlQuery.value(lastSavedCol).toString());
            thread.first.setNum(sqlQuery.value(numCol).toString());
            thread.first.setPostsCount(sqlQuery.value(postsCountCol).toInt());
            thread.first.setSubject(sqlQuery.value(subjectCol).toString());
            thread.first.setThreadCreated(sqlQuery.value(threadCreatedCol).toString());
            thread.first.setThreadLasthit(sqlQuery.value(lasthitCol).toString());

            thread.second = sqlQuery.value(createdCol).toString();

            threads.append(thread);
        }
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return threads;
    }
    return threads;
}

int DBShell::getCount(QString query)
{
    db->setIsError(false);
    int count = 0;

    try {
        QSqlQuery sqlQuery(db->getDatabase());
        sqlQuery.exec(query);
        QSqlRecord rec = sqlQuery.record();

        int countCol = rec.indexOf(QString("count"));

        while(sqlQuery.next()) {
            count = sqlQuery.value(countCol).toInt();
        }
    }
    catch(std::exception e) {
        db->setIsError(true);
        db->setLastErrorMessage(QString::fromUtf8(e.what()));
        return count;
    }
    return count;
}
