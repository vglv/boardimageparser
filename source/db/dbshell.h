#ifndef DBSHELL_H
#define DBSHELL_H

#include <QObject>
#include "db.h"

class DBShell : public QObject
{
    Q_OBJECT
public:
    explicit DBShell(QObject *parent = 0);
    ~DBShell();

    void setDB(DB *pDB);

    bool executeQuery(const QString& query);

    QStringList generateUpdateInsertRow(Row row);
    QStringList generatePostUpdateInsertRow(Row row);
    QStringList generateThreadUpdateInsertRow(Row row);

    QString generateCreateTableSQL(Table table);
    QString generateDeleteBoards(QVector<Board> boards);
    QString generateCreateSessionSQL(QString uid, QString created);
    QString generateFinishSessionSQL(QString uid, QString finished);
    QString generateUpdateTaskSQL(QString uid, QString searchString, QString boardsList, QString last_modified);

    QVector<QPair<Board, QString>> getBoards();
    QVector<QPair<Task, QPair<QString, QString>>> getTasks();
    QVector<QPair<Post, QPair<QString, QString>>> getPosts();
    QPair<Post, QPair<QString, QString>> getPosts(int number);
    QVector<QPair<Post, QPair<QString, QString>>> getPosts(QString query);
    QVector<QPair<Thread, QString>> getThreads(QString query);
    QVector<QPair<Post, QPair<QString, QString>>> getNewPosts();
    int getCount(QString query);


    Thread getThread(const QString& address);

    int getNewPostsCount();
    int getAllPostsCount();
private:
    DB* db;
};

#endif // DBSHELL_H
