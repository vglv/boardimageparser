#include "value.h"

Value::Value()
{

}

void Value::setMarked(bool value)
{
    marked = value;
}

bool Value::getMarked() const
{
    return marked;
}

void Value::setTextValue(QString value)
{
    textValue = value;
}

QString Value::getTextValue() const
{
    return textValue;
}

void Value::setIntegerValue(int value)
{
    integerValue = value;
}

int Value::getIntegerValue() const
{
    return integerValue;
}

void Value::setRealValue(double value)
{
    realValue = value;
}

double Value::getRealValue() const
{
    return realValue;
}

void Value::setBlobValue(QByteArray value)
{
    blobValue = value;
}

QByteArray Value::getBlobValue() const
{
    return blobValue;
}

