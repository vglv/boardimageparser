#ifndef DB_H
#define DB_H

#include <QObject>
#include <QtSql>
#include <QFile>
#include <QHash>
#include "table.h"
#include "row.h"
#include "board/board.h"
#include "board/post.h"
#include "board/thread.h"
#include "core/tasks.h"
#include "core/enums.h"


struct DBQuery
{
    QString query;
    QueryStatus status;
    QString errorString;
};


class DB : public QObject
{
    Q_OBJECT
public:
    explicit DB(QObject *parent = 0);
    ~DB();
    QString getDBName();
    void setDBName(const QString &name);
    bool ifExists();
    bool checkOpen();

    bool open();
    void close();

    void addQuery(QString query);
    void addQueryList(QStringList queries);

    bool createTable(Table table);
    bool executeQuery(QString query);
    bool executeQueries(QStringList queriesList);
    bool setSynchronousOff();
    bool setSynchronous0();
    bool setSynchronous1();
    bool setSynchronousNormal();
    bool setSynchronous2();
    bool setSynchronousFull();
    bool setJournalModeOff();
    bool setJournalModeDelete();

    bool isDBCorrect(const QVector<Table> &tables);

    bool getIsError();
    void setIsError(bool value);
    QString getLastErrorMessage();
    void setLastErrorMessage(const QString &str);

    QSqlDatabase getDatabase();

private:
    QHash<QString, DBQuery> queries;
    DBStatus status;

    QString generateCreateTableSQL(Table table);

    QSqlDatabase qDatabase;

    QString dbname;
    QString lastErrorMessage;
    bool isError = false;
    bool isOpened = false;

signals:

public slots:
};

#endif // DB_H
