#ifndef VALUE_H
#define VALUE_H
#include <QString>
#include "column.h"


class Value : public Column
{
public:
    Value();
    void setMarked(bool value);
    bool getMarked() const;
    void setTextValue(QString value);
    QString getTextValue() const;
    void setIntegerValue(int value);
    int getIntegerValue() const;
    void setRealValue(double value);
    double getRealValue() const;
    void setBlobValue(QByteArray value);
    QByteArray getBlobValue() const;

private:
    bool marked = false;
    QString textValue;
    int integerValue;
    double realValue;
    QByteArray blobValue;

};

#endif // VALUE_H
