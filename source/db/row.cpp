#include "row.h"

Row::Row()
{

}

Row::Row(Table table)
{
    this->setName(table.getName());

    for(int i = 0; i < table.columns.size(); i++)
        this->columns.append(table.columns[i]);

    for(int i = 0; i < this->columns.size(); i++) {
        Value value;
        value.setColumnType(this->columns[i].getColumnType());
        value.setIsNull(this->columns[i].getIsNull());
        value.setIsPrimaryKey(this->columns[i].getIsPrimaryKey());
        value.setName(this->columns[i].getName());
        values.append(value);
    }
}

Row Row::createRowObject(Table table)
{
    Row row;
    row.setName(table.getName());

    for(int i = 0; i < table.columns.size(); i++)
        row.columns.append(table.columns[i]);

    for(int i = 0; i < row.columns.size(); i++) {
        Value value;
        value.setColumnType(row.columns[i].getColumnType());
        value.setIsNull(row.columns[i].getIsNull());
        value.setIsPrimaryKey(row.columns[i].getIsPrimaryKey());
        value.setName(row.columns[i].getName());
        row.values.append(value);
    }

    return row;
}
