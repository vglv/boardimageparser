#ifndef TABLE_H
#define TABLE_H

#include <QObject>
#include <QString>
#include <QPair>
#include <QVector>
#include <QDebug>
#include "column.h"
#include "value.h"

class Table
{
public:
    Table();
    ~Table();

    QVector<Column> columns;
    QString getName() const;
    void setName(QString value);

private:
    QString name;

};

#endif // TABLE_H
