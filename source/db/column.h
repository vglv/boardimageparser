#ifndef COLUMN_H
#define COLUMN_H

#include <QDebug>

enum SqliteTypesEnum { SQ_INTEGER, SQ_REAL, SQ_TEXT, SQ_BLOB };


class Column
{
public:
    Column();
    void setColumnType(SqliteTypesEnum value);
    SqliteTypesEnum getColumnType() const;
    void setIsPrimaryKey(bool value);
    bool getIsPrimaryKey() const;
    void setIsNull(bool value);
    bool getIsNull() const;
    void setName(QString value);
    QString getName() const;

private:
    SqliteTypesEnum columnType;
    bool isPrimaryKey = false;
    bool isNull = true;
    QString name;


};

#endif // COLUMN_H
