#ifndef ROW_H
#define ROW_H
#include "table.h"

class Row : public Table
{
public:
    Row();
    Row(Table table);
    QVector<Value> values;

    static Row createRowObject(Table table);
};

#endif // ROW_H
