#include "column.h"

Column::Column()
{
    //qDebug() << Q_FUNC_INFO;
}

void Column::setColumnType(SqliteTypesEnum value)
{
    columnType = value;
}

SqliteTypesEnum Column::getColumnType() const
{
    return columnType;
}

void Column::setIsPrimaryKey(bool value)
{
    isPrimaryKey = value;
}

bool Column::getIsPrimaryKey() const
{
    return isPrimaryKey;
}

void Column::setIsNull(bool value)
{
    isNull = value;
}

bool Column::getIsNull() const
{
    return isNull;
}

void Column::setName(QString value)
{
    name = value;
}

QString Column::getName() const
{
    return name;
}
