#include "db.h"

using namespace std;

DB::DB(QObject *parent) : QObject(parent)
{
}

DB::~DB()
{
}

void DB::setDBName(const QString &name)
{
    dbname = name;
}

QString DB::getDBName()
{
    return dbname;
}

bool DB::getIsError()
{
    return isError;
}

void DB::setIsError(bool value)
{
    isError = value;
}

QString DB::getLastErrorMessage()
{
    return lastErrorMessage;
}

void DB::setLastErrorMessage(const QString &str)
{
    lastErrorMessage = str;
}

bool DB::open()
{
    qDatabase = QSqlDatabase::addDatabase("QSQLITE");
    qDatabase.setDatabaseName(dbname);

    if(!qDatabase.open())
        return false;

    isOpened = true;
    return true;
}

void DB::close()
{
    qDatabase.close();
}

void DB::addQuery(QString query)
{
    DBQuery newQuery;
    newQuery.query = query;
    newQuery.status = QueryStatus::QS_NOTSTARTED;
    newQuery.errorString = QString("");
    queries.insert(MainUtils::getContext()->newMD5(), newQuery);
}

void DB::addQueryList(QStringList queriesList)
{
    for(int i = 0; i < queriesList.size(); i++) {
        DBQuery newQuery;
        newQuery.query = queriesList[i];
        newQuery.status = QueryStatus::QS_NOTSTARTED;
        newQuery.errorString = QString("");
        queries.insert(MainUtils::getContext()->newMD5(), newQuery);
    }
}

bool DB::checkOpen()
{
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(QString("select * from sqlite_master where type='table';"));
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::ifExists()
{
    return QFile::exists(dbname);
}

QString DB::generateCreateTableSQL(Table table)
{
    QString str = QString("CREATE TABLE " + table.getName() + " (");
    for(int i = 0; i < table.columns.size(); i++) {

        switch(table.columns[i].getColumnType()) {
        case SqliteTypesEnum::SQ_INTEGER:
            {
                str += table.columns[i].getName() + QString(" INTEGER");
                if(table.columns[i].getIsPrimaryKey())
                    str += QString(" PRIMARY KEY");
                if(table.columns[i].getIsNull())
                    str += QString(" NULL, ");
                else
                    str += QString(" NOT NULL, ");
                break;
            }
        case SqliteTypesEnum::SQ_REAL:
            {
                str += table.columns[i].getName() + QString(" REAL");
                if(table.columns[i].getIsPrimaryKey())
                    str += QString(" PRIMARY KEY");
                if(table.columns[i].getIsNull())
                    str += QString(" NULL, ");
                else
                    str += QString(" NOT NULL, ");
                break;
            }
        case SqliteTypesEnum::SQ_TEXT:
            {
                str += table.columns[i].getName() + QString(" TEXT");
                if(table.columns[i].getIsPrimaryKey())
                    str += QString(" PRIMARY KEY");
                if(table.columns[i].getIsNull())
                    str += QString(" NULL, ");
                else
                    str += QString(" NOT NULL, ");
                break;
            }
        case SqliteTypesEnum::SQ_BLOB:
            {
                str += table.columns[i].getName() + QString(" BLOB");
                if(table.columns[i].getIsPrimaryKey())
                    str += QString(" PRIMARY KEY");
                if(table.columns[i].getIsNull())
                    str += QString(" NULL, ");
                else
                    str += QString(" NOT NULL, ");
                break;
            }
        }
    }
    str = str.mid(0, str.length() - 2);
    str += QString(");");

    return str;
}

bool DB::createTable(Table table) {
    isError = false;

    bool result = false;
    QString sql = generateCreateTableSQL(table);

    try {
        qDatabase.transaction();
        QSqlQuery sqlQuery(qDatabase);
        sqlQuery.prepare(sql);
        //result = sqlQuery.exec(query);
        sqlQuery.exec();
        sqlQuery.clear();
        qDatabase.commit();
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::executeQuery(QString query) {
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(query);
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}


bool DB::executeQueries(QStringList queries) {
    isError = false;
    bool result = true;

    setSynchronous1();
    setJournalModeOff();
    executeQuery(QString("BEGIN TRANSACTION;"));

    for(int i = 0; i < queries.size(); i++) {

        try {
            QSqlQuery sqlQuery(qDatabase);
            bool tempResult = sqlQuery.exec(queries[i]);
            if(!tempResult)
                result = false;
        }
        catch(std::exception e) {
            isError = true;
            lastErrorMessage = QString::fromUtf8(e.what());
            return false;
        }
    }

    setSynchronousFull();
    setJournalModeDelete();
    executeQuery(QString("COMMIT;"));

    return result;

}

bool DB::setSynchronousOff() {
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(QString("PRAGMA synchronous=OFF"));
        sqlQuery.clear();
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::setSynchronous0() {
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(QString("PRAGMA synchronous=0;"));
        sqlQuery.clear();
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::setJournalModeOff() {
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(QString("PRAGMA journal_mode=OFF;"));
        sqlQuery.clear();
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::setJournalModeDelete() {
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(QString("PRAGMA journal_mode=DELETE;"));
        sqlQuery.clear();
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::setSynchronous1() {
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(QString("PRAGMA synchronous=1;"));
        sqlQuery.clear();
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::setSynchronousNormal() {
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(QString("PRAGMA synchronous=NORMAL;"));
        sqlQuery.clear();
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::setSynchronous2() {
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(QString("PRAGMA synchronous=2;"));
        sqlQuery.clear();
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::setSynchronousFull() {
    isError = false;
    bool result = false;

    try {
        QSqlQuery sqlQuery(qDatabase);
        result = sqlQuery.exec(QString("PRAGMA synchronous=FULL;"));
        sqlQuery.clear();
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }
    return result;
}

bool DB::isDBCorrect(const QVector<Table> &tables) {
    isError = false;
    bool result = true;
    QStringList errorsList;

    try {
        QSqlQuery sqlQuery(qDatabase);
        QString sql = QString("select * from sqlite_master where type='table';");

        QVector<Table> temp_tables;

        result = sqlQuery.exec(sql);

        if(!result) {
            isError = true;
            return false;
        }

        while(sqlQuery.next()) {
            Table temp_table;
            temp_table.setName(sqlQuery.value(1).toString());

            QSqlQuery sqlQueryColumns(qDatabase);
            QString sqlColumns = QString("pragma table_info(" + temp_table.getName() + ")");

            bool resultColumns = sqlQueryColumns.exec(sqlColumns);
            if(!resultColumns) {
                isError = true;
                return false;
            }

            while(sqlQueryColumns.next()) {
                Column temp_column;

                temp_column.setName(sqlQueryColumns.value(1).toString());
                QString columnType = sqlQueryColumns.value(2).toString().toUpper();
                if(columnType == QString("TEXT")) {
                    temp_column.setColumnType(SqliteTypesEnum::SQ_TEXT);
                }
                else if(columnType == QString("BLOB")) {
                    temp_column.setColumnType(SqliteTypesEnum::SQ_BLOB);
                }
                else if(columnType == QString("INTEGER")) {
                    temp_column.setColumnType(SqliteTypesEnum::SQ_INTEGER);
                }
                else if(columnType == QString("REAL")) {
                    temp_column.setColumnType(SqliteTypesEnum::SQ_REAL);
                }
                else {
                    return false;
                }


                int isNull = sqlQueryColumns.value(3).toInt();
                if(isNull == 1)
                    temp_column.setIsNull(false);
                else
                    temp_column.setIsNull(true);
                int isPK = sqlQueryColumns.value(5).toInt();
                if(isPK == 1)
                    temp_column.setIsPrimaryKey(true);
                else
                    temp_column.setIsPrimaryKey(false);

                temp_table.columns.append(temp_column);
            }

            temp_tables.append(temp_table);
        }

        for(int i = 0; i < tables.size(); i++) {
            QString current_name = tables[i].getName();

            bool found = false;
            int index = -1;
            for(int j = 0; j < temp_tables.size(); j++) {
                if(temp_tables[j].getName() == current_name) {
                    found = true;
                    index = j;
                    break;
                }
            }
            if(!found) {
                errorsList.append(QString("Не найдена таблица ") + current_name);
                result = false;
            }

            if(!found)
                continue;

            for(int k = 0; k < tables[i].columns.size(); k++) {
                QString column_name = tables[i].columns[k].getName();
                found = false;
                int col_index = -1;
                for(int m = 0; m < temp_tables[index].columns.size(); m++) {
                    if(temp_tables[index].columns[m].getName() == column_name) {
                        found = true;
                        col_index = m;
                        break;
                    }
                }

                if(!found) {
                    errorsList.append(QString("Не найден столбец ") + column_name + QString(" в таблице ") + current_name);
                    result = false;
                    continue;
                }

                if(tables[i].columns[k].getColumnType() != temp_tables[index].columns[col_index].getColumnType()) {
                    errorsList.append(QString("Неверное значение типа столбца ") + column_name + QString(" в таблице ") + current_name);
                    result = false;
                }

                if(tables[i].columns[k].getIsNull() != temp_tables[index].columns[col_index].getIsNull()) {
                    errorsList.append(QString("Неверное значение isNull столбца ") + column_name + QString(" в таблице ") + current_name);
                    result = false;
                }

                if(tables[i].columns[k].getIsPrimaryKey() != temp_tables[index].columns[col_index].getIsPrimaryKey()) {
                    errorsList.append(QString("Неверное значение isPrimaryKey столбца ") + column_name + QString(" в таблице ") + current_name);
                    result = false;
                }

            }

            for(int t = 0; t < temp_tables[index].columns.size(); t++) {
                QString temp_column_name = temp_tables[index].columns[t].getName();
                found = false;

                for(int r = 0; r < tables[i].columns.size(); r++) {
                    if(tables[i].columns[r].getName() == temp_column_name) {
                        found = true;
                        break;
                    }
                }
                if(!found) {
                    errorsList.append(QString("Лишний столбец ") + temp_column_name + QString(" в таблице ") + current_name);
                    result = false;
                }
            }
        }

        for(int i = 0; i < temp_tables.size(); i++) {
            QString temp_table_name = temp_tables[i].getName();

            bool found = false;

            for(int j = 0; j < tables.size(); j++) {
                if(tables[j].getName() == temp_table_name) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                errorsList.append(QString("Лишняя таблица ") + temp_table_name + QString("!"));
                result = false;
            }
        }

        for(int b = 0; b < errorsList.size(); b++) {
            //qDebug() << errorsList[b];
        }
    }
    catch(std::exception e) {
        isError = true;
        lastErrorMessage = QString::fromUtf8(e.what());
        return false;
    }

    return result;
}

QSqlDatabase DB::getDatabase()
{
    return qDatabase;
}
