#include "taskswidget.h"
#include "ui_taskswidget.h"

tasksWidget::tasksWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::tasksWidget)
{
    ui->setupUi(this);
    model = NULL;

    connect(&aWidget, SIGNAL(taskAdded()), this, SLOT(tasksChanged()));
    connect(&aWidget, SIGNAL(taskChanged()), this, SLOT(tasksChanged()));
}

tasksWidget::~tasksWidget()
{
    delete ui;
}

void tasksWidget::on_pbOK_clicked()
{
    this->close();
}

void tasksWidget::on_pbAdd_clicked()
{
    aWidget.setModeChange(false);
    aWidget.cleanSearch();
    aWidget.getBoards();
    aWidget.show();
}

void tasksWidget::getTasks()
{
    //qDebug() << Q_FUNC_INFO;
    //qDebug() << AppContext::getContext()->db.getDBName();

    if(model != NULL)
        delete model;

    model = new QStandardItemModel(0, 3, this);
    model->setHorizontalHeaderItem(0, new QStandardItem(QString("Строка поиска")));
    model->setHorizontalHeaderItem(1, new QStandardItem(QString("Список досок")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QString("Время создания")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QString("Время последнего изменения")));

    ui->tvTasks->setModel(model);

    QVector<QPair<Task, QPair<QString, QString>>> tasks = AppContext::getContext()->dbShell.getTasks();

    for(int i = 0; i < tasks.size(); i++)
    {
        QList<QStandardItem*> items;
        items.append(new QStandardItem(tasks[i].first.getSearchString()));
        items.append(new QStandardItem(tasks[i].first.getBoardsString()));
        items.append(new QStandardItem(tasks[i].second.first));
        items.append(new QStandardItem(tasks[i].second.second));

        model->appendRow(items);
    }
}

void tasksWidget::tasksChanged()
{
    getTasks();
    setButtonsEnabled(false);
}

void tasksWidget::on_pbDelete_clicked()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Внимание");
    QModelIndexList selection = ui->tvTasks->selectionModel()->selectedRows();

    msgBox.setText("Вы действительно хотите удалить выбранную задачу?");
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    msgBox.setButtonText(QMessageBox::Yes, "Да");
    msgBox.setButtonText(QMessageBox::No, "Нет");
    if(msgBox.exec() == QMessageBox::Yes) {
        QString id = Tasks::getCurrentTasks()->getTaskByIndex(selection[0].row())->getID();
        QString deleteTaskSQL = QString("DELETE FROM Tasks WHERE uid='") + id + QString("';");
        if(!AppContext::getContext()->db.executeQuery(deleteTaskSQL))
        {
            //qDebug() << "SQL QUERY EXECUTION:" << deleteTaskSQL << AppContext::getContext()->db.getLastErrorMessage();
        }
        else
        {
            Tasks::getCurrentTasks()->removeTaskByIndex(selection[0].row());
            tasksChanged();
        }
    }
}

void tasksWidget::setButtonsEnabled(bool enabled)
{
    ui->pbDelete->setEnabled(enabled);
    ui->pbChange->setEnabled(enabled);
}

void tasksWidget::on_tvTasks_clicked(const QModelIndex &index)
{
    setButtonsEnabled(true);
}

void tasksWidget::on_pbChange_clicked()
{
    aWidget.setModeChange(true);
    QModelIndexList selection = ui->tvTasks->selectionModel()->selectedRows();
    QString id = Tasks::getCurrentTasks()->getTaskByIndex(selection[0].row())->getID();
    QString searchString = Tasks::getCurrentTasks()->getTaskByIndex(selection[0].row())->getSearchString();
    QStringList tempList = Tasks::getCurrentTasks()->getTaskByIndex(selection[0].row())->getBoardsList();
    aWidget.setChangingTaskID(id);
    aWidget.setSearchString(searchString);
    aWidget.getBoards();
    aWidget.setBoardsSelection(tempList);
    aWidget.show();
}
