#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    AppContext::getContext()->session.setPhantomjsPrefix(AppContext::getContext()->getPhantomjsPrefix());

    connect(&AppContext::getContext()->session, SIGNAL(startedTask(SessionTask)), this, SLOT(startedTaskSlot(SessionTask)));
    connect(&AppContext::getContext()->session, SIGNAL(finishedTask(SessionTask)), this, SLOT(finishedTaskSlot(SessionTask)));
    connect(&AppContext::getContext()->session, SIGNAL(sendLogMessage(QString)), this, SLOT(receiveLogMessage(QString)));
    connect(&AppContext::getContext()->session, SIGNAL(allTasksFinished()), this, SLOT(qwerty123()));
    connect(&AppContext::getContext()->garbageUtilizer, SIGNAL(sendLogMessage(QString)), this, SLOT(receiveLogMessage(QString)));
    connect(this, SIGNAL(guStart()), &AppContext::getContext()->garbageUtilizer, SLOT(start()));
    connect(this, SIGNAL(guStart()), this, SLOT(deletePosts()));
    connect(this, SIGNAL(filterChanged()), this, SLOT(onFilterChanged()));
    connect(this, SIGNAL(postsCountChanged()), this, SLOT(onPostsCountChanged()));

    firstBoards = false;

    emit guStart();

    currentPost = 0;
    totalPostsCount = 0;
    filter.clear();
    QVector<QPair<Task, QPair<QString, QString>>> tasks = AppContext::getContext()->dbShell.getTasks();

    for(int i = 0; i < tasks.size(); i++)
    {
        filter.append(tasks[i].first.getSearchString());
    }
    //emit filterChanged();

    refreshPosts();
}

void MainWindow::onFilterChanged()
{
    currentPost = 0;


    /*currentPosts.clear();
    QVector<QPair<Post, QPair<QString, QString>>> tempPosts = AppContext::getContext()->dbShell.getPosts();
    for(int i = 0; i < tempPosts.size(); i++)
    {
        bool found = false;
        for(int j = 0; j < filter.size(); j++)
        {
            if(tempPosts[i].second.first == filter[j])
            {
                found = true;
                break;
            }
        }
        if(found)
        {
            currentPosts.append(tempPosts[i]);
        }
    }*/

    totalPostsCount = AppContext::getContext()->dbShell.getAllPostsCount();

    //totalPostsCount = tempPosts.size();

    if(totalPostsCount > 0)
        currentPost = 1;

    showCurrentPost();

    QTimer::singleShot(60000, this, SLOT(refreshPosts()));
}

void MainWindow::refreshPosts()
{
    /*currentPosts.clear();
    QVector<QPair<Post, QPair<QString, QString>>> tempPosts = AppContext::getContext()->dbShell.getPosts();
    for(int i = 0; i < tempPosts.size(); i++)
    {
        bool found = false;
        for(int j = 0; j < filter.size(); j++)
        {
            if(tempPosts[i].second.first == filter[j])
            {
                found = true;
                break;
            }
        }
        if(found)
        {
            currentPosts.append(tempPosts[i]);
        }
    }*/

    totalPostsCount = AppContext::getContext()->dbShell.getAllPostsCount();
    if(totalPostsCount > 0 && currentPost == 0)
        currentPost = 1;

    showCurrentPost();

    QTimer::singleShot(60000, this, SLOT(refreshPosts()));
}

void MainWindow::onPostsCountChanged()
{

}

void MainWindow::showCurrentPost()
{
    if(currentPost == 0)
    {
        ui->lCurrent->setText("Текущий пост: 0 / 0");
        ui->pbBegin->setEnabled(false);
        ui->pbPrev->setEnabled(false);
        ui->pbNext->setEnabled(false);
        ui->pbLast->setEnabled(false);
        ui->pbBrowserLocal->setEnabled(false);
        ui->pbBrowserInternet->setEnabled(false);
    }
    else
    {
        ui->lCurrent->setText("Текущий пост: " + QString::number(currentPost) + " / " + QString::number(totalPostsCount));
        ui->pbBegin->setEnabled(true);
        ui->pbPrev->setEnabled(true);
        ui->pbNext->setEnabled(true);
        ui->pbLast->setEnabled(true);
        ui->pbBrowserLocal->setEnabled(true);
        ui->pbBrowserInternet->setEnabled(true);

        QPair<Post, QPair<QString, QString>> tempPost = AppContext::getContext()->dbShell.getPosts(currentPost);
        post = tempPost;

        QString postData;
        postData = "Время: " + tempPost.second.second + "; ключевое слово: " + tempPost.second.first;
        ui->lPostData->setText(postData);
        QString html;
        html += tempPost.first.getDate() + "<br>";
        html += "<a href=\"" + tempPost.first.getAddress() + "\">"
              + tempPost.first.getAddress() + "</a><br><br>";
        html += tempPost.first.getComment();
        ui->tbPost->setHtml(html);

        //QPalette palette = ui->tbPost->palette();

        if(tempPost.first.getDeleted() == 1)
        {
            ui->tbPost->setStyleSheet("background-color: red;");
        }
        else
            ui->tbPost->setStyleSheet("background-color: white;");

        //ui->tbPost->setPalette(palette);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getBoardsList()
{
    for(int i = 0; i < 1; i++)
    {

        int timeout = 0;
        QString address = AppContext::getContext()->getProperty("GetBoardsAddress", "BNWorker");
        try {
            timeout = AppContext::getContext()->getProperty("GetBoardsTimeout", "BNWorker").toInt();

        } catch (std::exception e) {
            //qDebug() << e.what();
           // qDebug() << "Setting timeout to " << timeout << "...";

        }
        if(timeout <= 0) {
            //qDebug() << "Incorrect timeout value:" << timeout;
            //qDebug() << "Setting timeout to " << timeout << "...";
        }

        AppContext::getContext()->session.addTask(BNType::BN_GETBOARDS, address, timeout);
    }

    QStringList boards_temp = Tasks::getCurrentTasks()->getBoards();
    if(!firstBoards)
    {
        for(int i = 0; i < boards_temp.size(); i++)
        {
            QVariant var(boards_temp[i]);
            QString addr = QString("https://2ch.hk") + boards_temp[i] + QString("catalog.json");
            AppContext::getContext()->session.addTask(BNType::BN_GETTHREADSBYBOARD, addr, 60000, var);
        }
        firstBoards = true;
    }

    AppContext::getContext()->session.regulars = AppContext::getContext()->regulars;
    AppContext::getContext()->session.Start();

    int interval = 600000;
    try {
        interval = AppContext::getContext()->getProperty("interval", "BoardsParser").toInt();
    }
    catch(std::exception e)
    {
        //qDebug() << e.what();
    }

    QTimer::singleShot(interval, this, SLOT(getBoardsList()));
}

void MainWindow::on_pushButton_clicked()
{
    QMessageBox msgBox;
    QVector<QPair<Task, QPair<QString, QString>>> tasks = AppContext::getContext()->dbShell.getTasks();
    searchTasks.clear();

    outputFile = QString::number(MainUtils::getContext()->getCurrentTimestamp()) + ".html";
    for(int i = 0; i < tasks.size(); i++)
    {
        QPair<Task, QPair<QString, QString>> temp = tasks[i];
        searchTasks.append(temp.first);
    }

    QTimer::singleShot(0, this, SLOT(getBoardsList()));
    //QTimer::singleShot(120000, this, SLOT(qwerty123()));


    if(AppContext::getContext()->session.getClearInterval() > 0)
        QTimer::singleShot(AppContext::getContext()->session.getClearInterval(), this, SLOT(clearTasks()));
}

void MainWindow::qwerty123()
{
    ui->lwLog->clear();

    QStringList boards_temp = Tasks::getCurrentTasks()->getBoards();
    qDebug() << Q_FUNC_INFO << boards_temp.size();

    for(int i = 0; i < boards_temp.size(); i++)
    {
        QVariant var(boards_temp[i]);
        QString addr = QString("https://2ch.hk") + boards_temp[i] + QString("catalog.json");
        AppContext::getContext()->session.addTask(BNType::BN_GETTHREADSBYBOARD, addr, 60000, var);
        AppContext::getContext()->session.Start();
    }
}

void MainWindow::clearTasks()
{
    AppContext::getContext()->session.clearTasks();
}

void MainWindow::startedTaskSlot(SessionTask task)
{
    //qDebug() << Q_FUNC_INFO;
    switch(task.bnType)
    {\
    case BNType::BN_GETBOARDS:
        {
            bWidget.startedTask(task);

            break;
        }
    }
}

void MainWindow::newLogItemSuccess(const QString& msg)
{
    ui->lwLog->addItem(msg);
    ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkGreen);
}

void MainWindow::newLogItemError(const QString& msg)
{
    ui->lwLog->addItem(msg);
    ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkRed);
}

void MainWindow::newLogItemTimeout(const QString& msg)
{
    ui->lwLog->addItem(msg);
    ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkYellow);
}

void MainWindow::finishedTaskSlot(SessionTask task)
{
    switch(task.bnType)
    {
    case BNType::BN_GETBOARDS:
        {
            bWidget.finishedTask(task);

            if(task.taskStatus == TaskStatus::TS_FINISHEDSUCCESS) {

                std::vector<Board> boards = task.someData.getValue().value<BoardsList>().boards;

                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                             + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                             + ". Задача выполнена успешно. Досок получено: " + QString::number(boards.size())
                             + ". Время выполнения задачи: "
                             + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkGreen);

                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemSuccess, text);
                t.waitForFinished();

                QStringList insertQueries;
                for(int i = 0; i < boards.size(); i++) {
                    bool result = false;
                    Table boardsTable = AppContext::getContext()->getTable("boards", result);
                    Row tempRow = Row::createRowObject(boardsTable);

                    for(int j = 0; j < tempRow.values.size(); j++) {
                        if(tempRow.values[j].getName() == "board") {
                            tempRow.values[j].setTextValue(boards[i].getBrd());

                        }
                        if(tempRow.values[j].getName() == "name") {
                            tempRow.values[j].setTextValue(boards[i].getName());
                        }
                        if(tempRow.values[j].getName() == "posts_per_hour") {
                            tempRow.values[j].setIntegerValue(boards[i].getPostsPerHour());

                        }
                        if(tempRow.values[j].getName() == "unic_ips") {
                            tempRow.values[j].setIntegerValue(boards[i].getUnicIps());

                        }
                        if(tempRow.values[j].getName() == "description") {
                            tempRow.values[j].setTextValue(boards[i].getDescription());

                        }
                        if(tempRow.values[j].getName() == "posts_count") {
                            tempRow.values[j].setTextValue(boards[i].getPostsCount());
                        }
                        if(tempRow.values[j].getName() == "created") {
                            tempRow.values[j].setMarked(true);
                            tempRow.values[j].setTextValue(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp()));
                        }
                        if(tempRow.values[j].getName() == "last_modified") {
                            tempRow.values[j].setTextValue(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp()));
                        }
                    }
                    QStringList queries = AppContext::getContext()->dbShell.generateUpdateInsertRow(tempRow);
                    for(int k = 0; k < queries.size(); k++) {
                        insertQueries.append(queries[k]);
                    }
                }


                QVector<Board> boardsVector;
                for(int i = 0; i < boards.size(); i++)
                    boardsVector.append(boards[i]);

                QString deleteQuery = AppContext::getContext()->dbShell.generateDeleteBoards(boardsVector);

                insertQueries.append(deleteQuery);

                AppContext::getContext()->db.executeQueries(insertQueries);
                if(AppContext::getContext()->db.getIsError()) {
                    qDebug() << "ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
                }
            }

            if(task.taskStatus == TaskStatus::TS_FINISHEDERROR) {
                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                             + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                             + ". Задача завершилась с ошибкой. Время выполнения задачи: "
                             + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkRed);
                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemError, text);
                t.waitForFinished();
            }
            if(task.taskStatus == TaskStatus::TS_TIMEOUT) {
                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                             + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                             + ". Время ожидания истекло. Время выполнения задачи: "
                             + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkYellow);
                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemTimeout, text);
                t.waitForFinished();
            }

            break;
        }
    case BN_GETTHREADSBYBOARD:
        {
            if(task.taskStatus == TaskStatus::TS_FINISHEDSUCCESS) {
                std::vector<Thread> threads = task.someData.getValue().value<ThreadsList>().threads;

                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                             + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                             + "; address=" + task.getAddress() +
                             //+ ". Задача выполнена успешно. Размер полученных данных: " + QString::number(task.someData.htmlLength)
                        + ". Задача выполнена успешно. Количество тредов: " + QString::number(threads.size())
                             + ". Время выполнения задачи: "
                             + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkGreen);

                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemSuccess, text);
                t.waitForFinished();

                for(int i = 0; i < threads.size(); i++)
                {
                    QVector<Thread> tempThreads;
                    tempThreads.append(threads[i]);

                    Thread tempThread = AppContext::getContext()->dbShell.getThread(threads[i].getAddress());

                    if(threads[i].getThreadLasthit() != tempThread.getThreadLasthit())
                    {
                        ThreadsList tList;
                        tList.threads = tempThreads.toStdVector();
                        QVariant var = QVariant::fromValue(tList);
                        QString addr = threads[i].getAddress().replace(".html", ".json");

                        if(tempThread.getThreadLasthit().trimmed() != "")
                            AppContext::getContext()->session.addTask(BNType::BN_GETTHREAD, threads[i].getAddress(), 60000);
                        AppContext::getContext()->session.addTask(BNType::BN_GETPOSTSBYTHREAD, addr, 60000, var);
                    }
                }

            }
            if(task.taskStatus == TaskStatus::TS_FINISHEDERROR) {
                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                            + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                            + "; address=" + task.getAddress() +
                            + ". Задача завершилась с ошибкой. Время выполнения задачи: "
                            + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkRed);
                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemError, text);
                t.waitForFinished();
            }
            if(task.taskStatus == TaskStatus::TS_TIMEOUT) {
                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                             + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                             + "; address=" + task.getAddress() +
                             + ". Время ожидания истекло. Время выполнения задачи: "
                             + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkYellow);
                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemTimeout, text);
                t.waitForFinished();
            }

            break;
        }
    case BN_GETPOSTSBYTHREAD:
        {
            if(task.taskStatus == TaskStatus::TS_FINISHEDSUCCESS) {
                std::vector<Post> posts = task.someData.getValue().value<PostsList>().posts;

                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                             + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                             + "; address=" + task.getAddress() +
                             //+ ". Задача выполнена успешно. Размер полученных данных: " + QString::number(task.someData.htmlLength)
                        + ". Задача выполнена успешно. Количество постов: " + QString::number(posts.size())
                             + ". Время выполнения задачи: "
                             + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkGreen);
                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemSuccess, text);
                t.waitForFinished();

                std::vector<Thread> threads = task.getValue().value<ThreadsList>().threads;

                for(int i = 0; i < posts.size(); i++)
                {
                    QString comment = posts.at(i).getComment();
                    QString subject = posts.at(i).getSubject();

                    for(int j = 0; j < searchTasks.size(); j++)
                    {
                        QString search = searchTasks[j].getSearchString().toLower();


                        QStringList boardList = searchTasks[j].getBoardsList();
                        bool found = false;
                        for(int q = 0; q < boardList.size(); q++)
                        {
                            if(boardList[q] == threads[0].getBrd())
                            {
                                found = true;
                                break;
                            }
                        }
                        if(!found)
                            continue;

                        if(   comment.toLower().indexOf(search) != -1
                           || subject.toLower().indexOf(search) != -1    )
                        {
                            QString threadAddress = posts.at(i).getAddress();
                            threadAddress = threadAddress.mid(0, threadAddress.indexOf("#"));

                            if(!AppContext::getContext()->session.isThreadWithAddress(threadAddress))
                            {
                                AppContext::getContext()->session.addTask(BNType::BN_GETTHREAD, threadAddress, 60000);
                            }

                            QStringList insertQueries;
                            for(int t = 0; t < threads.size(); t++) {
                                bool result = false;
                                Table threadsTable = AppContext::getContext()->getTable("threads", result);
                                Row tempRow = Row::createRowObject(threadsTable);

                                for(int m = 0; m < tempRow.values.size(); m++) {
                                    if(tempRow.values[m].getName() == "address") {
                                        QString tempAddress = "https://2ch.hk" + threads[t].getBrd() + "res/" + threads[t].getNum() + ".html";
                                        tempRow.values[m].setTextValue(tempAddress);
                                    }
                                    if(tempRow.values[m].getName() == "board") {
                                        tempRow.values[m].setTextValue(threads[t].getBrd());
                                    }
                                    if(tempRow.values[m].getName() == "num") {
                                        tempRow.values[m].setTextValue(threads[t].getNum());
                                    }
                                    if(tempRow.values[m].getName() == "subject") {
                                        tempRow.values[m].setTextValue(threads[t].getSubject());
                                    }
                                    if(tempRow.values[m].getName() == "thread_created") {
                                        tempRow.values[m].setTextValue(threads[t].getThreadCreated());
                                    }
                                    if(tempRow.values[m].getName() == "lasthit") {
                                        tempRow.values[m].setTextValue(threads[t].getThreadLasthit());
                                    }
                                    if(tempRow.values[m].getName() == "posts_count") {
                                        tempRow.values[m].setIntegerValue(threads[t].getPostsCount());
                                    }
                                    if(tempRow.values[m].getName() == "files_count") {
                                        tempRow.values[m].setIntegerValue(threads[t].getFilesCount());
                                    }
                                    if(tempRow.values[m].getName() == "email") {
                                        tempRow.values[m].setTextValue(threads[t].getEmail());
                                    }
                                    if(tempRow.values[m].getName() == "last_saved") {
                                        tempRow.values[m].setTextValue(QString(""));
                                    }
                                    if(tempRow.values[m].getName() == "deleted") {
                                        tempRow.values[m].setIntegerValue(0);
                                    }
                                    if(tempRow.values[m].getName() == "created") {
                                        tempRow.values[m].setMarked(true);
                                        tempRow.values[m].setTextValue(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp()));
                                    }
                                    if(tempRow.values[m].getName() == "last_modified") {
                                        tempRow.values[m].setTextValue(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp()));
                                    }
                                }

                                QStringList queries = AppContext::getContext()->dbShell.generateThreadUpdateInsertRow(tempRow);
                                for(int k = 0; k < queries.size(); k++) {
                                    insertQueries.append(queries[k]);
                                    qDebug() << "**** " << queries[k];
                                }
                            }

                            AppContext::getContext()->db.executeQueries(insertQueries);
                            if(AppContext::getContext()->db.getIsError()) {
                                qDebug() << "ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
                            }



                            QStringList insertQueriesPost;

                            bool result = false;
                            Table postsTable = AppContext::getContext()->getTable("posts", result);
                            Row postRow = Row::createRowObject(postsTable);

                            QString tempAddress;


                            for(int k = 0; k < postRow.values.size(); k++) {
                                if(postRow.values[k].getName() == "address") {
                                    postRow.values[k].setTextValue(posts[i].getAddress());
                                }
                                if(postRow.values[k].getName() == "thread") {
                                    tempAddress = "https://2ch.hk" + threads[0].getBrd() + "res/" + threads[0].getNum() + ".html";
                                    postRow.values[k].setTextValue(tempAddress);
                                }
                                if(postRow.values[k].getName() == "board") {
                                    QString tempValue = threads[0].getBrd();
                                    postRow.values[k].setTextValue(tempValue);
                                }
                                if(postRow.values[k].getName() == "search") {
                                    postRow.values[k].setTextValue(search);
                                }
                                if(postRow.values[k].getName() == "num") {
                                    postRow.values[k].setTextValue(posts[i].getNum());
                                }
                                if(postRow.values[k].getName() == "comment") {
                                    postRow.values[k].setTextValue(posts[i].getComment());
                                }
                                if(postRow.values[k].getName() == "subject") {
                                    postRow.values[k].setTextValue(posts[i].getSubject());
                                }
                                if(postRow.values[k].getName() == "date") {
                                    postRow.values[k].setTextValue(posts[i].getDate());
                                }
                                if(postRow.values[k].getName() == "email") {
                                    postRow.values[k].setTextValue(posts[i].getEmail());
                                }
                                if(postRow.values[k].getName() == "name") {
                                    postRow.values[k].setTextValue(posts[i].getName());
                                }
                                if(postRow.values[k].getName() == "number") {
                                    postRow.values[k].setIntegerValue(posts[i].getNumber());
                                }
                                if(postRow.values[k].getName() == "saved") {
                                    postRow.values[k].setIntegerValue(0);
                                }
                                if(postRow.values[k].getName() == "deleted") {
                                    postRow.values[k].setIntegerValue(0);
                                }

                                QString createdValue = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp());

                                if(postRow.values[k].getName() == "created") {
                                    postRow.values[k].setMarked(true);
                                    postRow.values[k].setTextValue(createdValue);
                                }
                                if(postRow.values[k].getName() == "exported") {
                                    postRow.values[k].setMarked(true);
                                    postRow.values[k].setIntegerValue(0);
                                }
                            }

                            QStringList queries = AppContext::getContext()->dbShell.generatePostUpdateInsertRow(postRow);
                            for(int k = 0; k < queries.size(); k++) {
                                insertQueriesPost.append(queries[k]);
                            }

                            AppContext::getContext()->db.executeQueries(insertQueriesPost);
                            if(AppContext::getContext()->db.getIsError()) {
                                qDebug() << "ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
                            }

                            QString fdsfd = "select count(*) as count from posts where thread = '"
                                          + tempAddress + "' and deleted = 0;";

                            int bcount = AppContext::getContext()->dbShell.getCount(fdsfd);

                            qDebug() << "@@@@ " << fdsfd << bcount;

                            if(bcount > 0) {
                                fdsfd = "UPDATE threads set deleted = 0 where address='"
                                      + tempAddress + "';";

                                AppContext::getContext()->dbShell.executeQuery(fdsfd);
                                qDebug() << "%%%% " << fdsfd;
                            }

                            int allCount = AppContext::getContext()->dbShell.getAllPostsCount();
                            int newCount = AppContext::getContext()->dbShell.getNewPostsCount();

                            ui->lNew->setText("Новых записей: " + QString::number(newCount) + " / " + QString::number(allCount));

                            if(totalPostsCount == 0)
                            {
                                totalPostsCount = allCount;
                                currentPost = 1;
                                showCurrentPost();
                            }
                            else
                            {
                                totalPostsCount = allCount;
                            }
                            ui->lCurrent->setText("Текущий пост: " + QString::number(currentPost) + " / " + QString::number(totalPostsCount));


                            /*if(allCount > totalPostsCount)
                            {
                                currentPosts.clear();
                                QVector<QPair<Post, QPair<QString, QString>>> tempPosts = AppContext::getContext()->dbShell.getPosts();
                                for(int i = 0; i < tempPosts.size(); i++)
                                {
                                    bool found = false;
                                    for(int j = 0; j < filter.size(); j++)
                                    {
                                        if(tempPosts[i].second.first == filter[j])
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                    if(found)
                                    {
                                        currentPosts.append(tempPosts[i]);
                                    }
                                }


                                if(currentPost == 0)
                                {
                                    currentPost = 1;
                                    showCurrentPost();
                                }
                                totalPostsCount = currentPosts.size();

                                ui->lCurrent->setText("Текущий пост: " + QString::number(currentPost) + " / " + QString::number(totalPostsCount));
                            }*/

                        }
                    }
                }
            }
            if(task.taskStatus == TaskStatus::TS_FINISHEDERROR) {
                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                            + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                            + "; address=" + task.getAddress() +
                            + ". Задача завершилась с ошибкой. Время выполнения задачи: "
                            + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkRed);
                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemError, text);
                t.waitForFinished();
            }
            if(task.taskStatus == TaskStatus::TS_TIMEOUT) {
                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                             + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                             + "; address=" + task.getAddress() +
                             + ". Время ожидания истекло. Время выполнения задачи: "
                             + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkYellow);
                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemTimeout, text);
                t.waitForFinished();
            }

            break;

        }
    case BN_GETTHREAD:
        {
            if(task.taskStatus == TaskStatus::TS_FINISHEDSUCCESS) {
                QStringList images = task.someData.getValue().toStringList();
                for(int i = 0; i < images.size(); i++)
                {
                    QStringList str = images[i].split("/");
                    QString fname = str.at(str.length() - 1);
                    QString thread = str.at(str.length() - 2);
                    QString boardName = str.at(str.size() - 4);

                    QString pathdir = QCoreApplication::applicationDirPath();
                    QString filename = pathdir + "/threads/" + boardName + "/" + thread + "/data/" + fname;

                    if(!QFile::exists(filename))
                        AppContext::getContext()->session.addTask(BNType::BN_GETFILE, images[i], 60000);
                }

                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                             + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                             + "; address=" + task.getAddress() +
                             + ". Тред сохранен успешно. Время выполнения задачи: "
                             + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkGreen);
                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemSuccess, text);
                t.waitForFinished();

            }

            break;
        }
    case BN_GETFILE:
        {
            if(task.taskStatus == TaskStatus::TS_FINISHEDSUCCESS) {

                QString text = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp())
                             + " Задача ID=" + task.getID() + "; index=" + QString::number(task.getIndex())
                             + "; address=" + task.getAddress() +
                             + ". Файл сохранен успешно. Время выполнения задачи: "
                             + QString::number(task.getTimestampStop() - task.getTimestampStart()) + " мс. ";
                //ui->lwLog->addItem(text);
                //ui->lwLog->item(ui->lwLog->count() - 1)->setForeground(Qt::darkGreen);
                QFuture<void> t = QtConcurrent::run(this, &MainWindow::newLogItemSuccess, text);
                t.waitForFinished();
            }

            break;
        }
    }
}

void MainWindow::on_pbCleanLog_clicked()
{
    ui->lwLog->clear();
}

void MainWindow::receiveLogMessage(QString value)
{
    if(ui->lwLog->count() >= 100)
        ui->lwLog->clear();

    ui->lwLog->addItem(MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp()) + " " + value);
}

void MainWindow::on_bpBoardsList_clicked()
{
    bWidget.show();
}

void MainWindow::on_pbTasksList_clicked()
{
    tWidget.setButtonsEnabled();
    tWidget.getTasks();
    tWidget.show();
}

void MainWindow::on_pbExport_clicked()
{
    QFile output("export_" + QString::number(MainUtils::getContext()->getCurrentTimestamp()) + ".html");
    output.open(QFile::Append);

    QTextStream out(&output);

    QString pathdir = QCoreApplication::applicationDirPath();

    QVector<QPair<Post, QPair<QString, QString>>> posts = AppContext::getContext()->dbShell.getNewPosts();
    for(int i = 0; i < posts.size(); i++) {

        QStringList str = posts[i].first.getAddress().split("/");
        QString thread = str.at(str.size() - 1);
        QString boardName = str.at(str.size() - 3);
        int index = thread.indexOf(".");
        thread = thread.mid(0, index);

        QString localAddress = pathdir + "/threads/" + boardName + "/" + thread
                             + "/" + thread + ".html#" + posts[i].first.getNum();

        out << "<b>========================================================================================================================</b><br>";
        out << ("<b>Получено: " + posts[i].second.second + "</b><br>");
        //out << ("<b><a href=\"" + posts[i].first.getAddress() + "\">" + posts[i].first.getAddress() + "</a> (" + posts[i].second.first + ")</b><br>");
        out << ("<b><a href=\"" + localAddress + "\">" + localAddress + "</a> (" + posts[i].second.first + ")</b><br>");
        out << ("<b>" + posts[i].first.getDate() + "</b><br>");
        out << "<b>------------------------------------------------------------------------------------------------------------------------</b><br>";
        out << (posts[i].first.getComment() + "<br>");
    }

    output.close();

    int allCount = AppContext::getContext()->dbShell.getAllPostsCount();
    int newCount = AppContext::getContext()->dbShell.getNewPostsCount();
    ui->lNew->setText("Новых записей: " + QString::number(newCount) + " / " + QString::number(allCount));
}

void MainWindow::on_pbExportAll_clicked()
{
    QFile output("export_" + QString::number(MainUtils::getContext()->getCurrentTimestamp()) + ".html");
    output.open(QFile::Append);

    QTextStream out(&output);

    QString pathdir = QCoreApplication::applicationDirPath();

    QVector<QPair<Post, QPair<QString, QString>>> posts = AppContext::getContext()->dbShell.getPosts();
    for(int i = 0; i < posts.size(); i++) {

        QStringList str = posts[i].first.getAddress().split("/");
        QString thread = str.at(str.size() - 1);
        QString boardName = str.at(str.size() - 3);
        int index = thread.indexOf(".");
        thread = thread.mid(0, index);

        QString localAddress = pathdir + "/threads/" + boardName + "/" + thread
                             + "/" + thread + ".html#" + posts[i].first.getNum();

        out << "<b>========================================================================================================================</b><br>";
        out << ("<b>Получено: " + posts[i].second.second + "</b><br>");
        //out << ("<b><a href=\"" + posts[i].first.getAddress() + "\">" + posts[i].first.getAddress() + "</a> (" + posts[i].second.first + ")</b><br>");
        out << ("<b><a href=\"" + localAddress + "\">" + localAddress + "</a> (" + posts[i].second.first + ")</b><br>");
        out << ("<b>" + posts[i].first.getDate() + "</b><br>");
        out << "<b>------------------------------------------------------------------------------------------------------------------------</b><br>";
        out << (posts[i].first.getComment() + "<br>");
    }

    output.close();
}

void MainWindow::on_pbBegin_clicked()
{
    if(currentPost != 1)
    {
        currentPost = 1;
        showCurrentPost();
    }
}

void MainWindow::on_pbPrev_clicked()
{
    if(currentPost > 1)
    {
        currentPost--;
        showCurrentPost();
    }
}

void MainWindow::on_pbNext_clicked()
{
    if(currentPost < totalPostsCount)
    {
        currentPost++;
        showCurrentPost();
    }
}

void MainWindow::on_pbLast_clicked()
{
    if(currentPost != totalPostsCount)
    {
        currentPost = totalPostsCount;
        showCurrentPost();
    }
}

void MainWindow::on_pbBrowserLocal_clicked()
{
    QString pathdir = QCoreApplication::applicationDirPath();
    QString address = post.first.getAddress();
    int index1 = address.lastIndexOf("/");
    int index2 = address.lastIndexOf(".");
    QString thread = address.mid(index1 + 1, index2 - index1 - 1);

    QStringList strl = address.split("/");
    QString boardName = strl.at(strl.size() - 3);

    QString addr = pathdir + "/threads/" + boardName + "/" + thread + "/" + thread + ".html#"
                 + post.first.getNum();

    QProcess::startDetached("cmd /c start file:///" + addr);

}

void MainWindow::on_pbBrowserInternet_clicked()
{
    QString address = post.first.getAddress();
    QProcess::startDetached("cmd /c start " + address);
}


void MainWindow::deletePosts()
{
    QString temp1 = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp() - AppContext::getContext()->deletePostsTimeout);
    QString temp2 = MainUtils::getContext()->getDateTimeString(MainUtils::getContext()->getCurrentTimestamp() - 604800000);
    QString query = "select * from posts where created < '" + temp1 + "' and deleted = 0;";

    qDebug() << "TEMP: " << temp1;
    //qDebug() << "QUERY: " << query;
    QVector<QPair<Post, QPair<QString, QString>>> postsToDelete = AppContext::getContext()->dbShell.getPosts(query);
    qDebug() << "QUERY: " << query << postsToDelete.count();

    query = "select * from threads where created > '" + temp2 + "' and deleted = 1;";
    QVector<QPair<Thread, QString>> threadsToDelete = AppContext::getContext()->dbShell.getThreads(query);

    qDebug() << "QUERY THREADS: " << query << threadsToDelete.count();

    //QString deleteDate;
    query = "UPDATE posts set deleted = 1 where created < '" + temp1 + "';";
    AppContext::getContext()->dbShell.executeQuery(query);

    for(int i = 0; i < postsToDelete.count(); i++)
    {
        query = "select count(*) as count from posts where thread='" + postsToDelete[i].first.getThread() + "' AND deleted = 0;";
        int count = AppContext::getContext()->dbShell.getCount(query);

        qDebug() << "REMOVE: " << i << query << count;
        
        if(count == 0)
        {
            QString board = postsToDelete[i].first.getBoard();
            QString dir = "threads/" + board + "/" + postsToDelete[i].first.getThreadNum();
            qDebug() << "REMOVE board: " << board << postsToDelete[i].first.getThreadNum() << dir;


            // REMOVE FILES
            AppContext::getContext()->garbageUtilizer.addDirToDelete(dir);
 

            query = "UPDATE threads set deleted = 1 where address = '" + postsToDelete[i].first.getThread() + "';";
            AppContext::getContext()->dbShell.executeQuery(query);
        }
    }

    for(int i = 0; i < threadsToDelete.count(); i++)
    {
        QString board = threadsToDelete[i].first.getBrd();
        QString dir = "threads/" + board + "/" + threadsToDelete[i].first.getNum();
        qDebug() << "@@@@ THREAD REMOVE:" << dir;
        AppContext::getContext()->garbageUtilizer.addDirToDelete(dir);
    }

    refreshPosts();

    QTimer::singleShot(60000, this, SLOT(deletePosts()));
}
