#define VERSION 0.1.0

#include "mainwindow.h"
#include "utils/MainUtils.h"
#include "core/AppContext.h"
#include "core/tasks.h"
#include "board/regulars.h"
#include "boardswidget.h"
#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QException>

using std::exception;

bool prepareDB();
bool setDB();
void loadDataFromDB();
bool initSession();
bool initGarbageUtilizer();
bool initPhantomjs();
bool getRegulars(Regulars *regulars);

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QString pathdir = QCoreApplication::applicationDirPath();
    QDir dr;
    dr.mkpath(pathdir + "/logs");

    qDebug() << QString();
    qDebug() << QString();
    qDebug() << QString();
    qDebug() << " ************************************************************";
    qDebug() << " APP: STARTING APPLICATION ...";
    qDebug() << " APP: VERSION " << STRINGIZE_VALUE_OF(VERSION);
    //qDebug() << " APP: BUILD" << BUILD;
    qDebug() << " ************************************************************";
    qDebug() << QString();

    QScopedPointer<AppContext> ac(new AppContext());
    MainUtils *mu = new MainUtils();
    QScopedPointer<Tasks> t(new Tasks());

    try {
        ac->init(pathdir + "/settings.ini");
        AppContext::setContext(ac.data());
        AppContext::setApp(&a);
        MainUtils::setContext(mu);
        Tasks::setCurrentTasks(t.data());
    }
    catch(MyException &ex) {
        qDebug() << ex.getMessage();
        return -1;
    }

    if(!prepareDB())
        return -1;

    if(!setDB())
    {
        qDebug() << "Не удалось открыть базу данных" << AppContext::getContext()->db.getDBName();
        return -1;
    }
    AppContext::getContext()->db.open();
    AppContext::getContext()->dbShell.setDB(&AppContext::getContext()->db);

    AppContext::getContext()->currentSession = mu->newMD5();

    loadDataFromDB();

    AppContext::getContext()->db.executeQuery(AppContext::getContext()->dbShell.generateCreateSessionSQL(  AppContext::getContext()->currentSession
                                                                                                         , mu->getDateTimeString(mu->getCurrentTimestamp())));

    if(!initPhantomjs())
    {
        qDebug() << "Не удалось инициализировать настройки phantomjs!";
        return -1;
    }

    Regulars regs;

    if(!getRegulars(&regs))
    {
        qDebug() << "Не удалось загрузить регулярные выражения из файла настроек!";
        return -1;
    }

    if(!initSession())
    {
        qDebug() << "Не удается инициализировать объект Session! Нет необходимых параметров в файле настроек!";
        return -1;
    }

    if(!initGarbageUtilizer())
    {
        qDebug() << "Не удается инициализировать объект GarbageUtilizer! Нет необходимых параметров в файле настроек!";
        return -1;
    }

    AppContext::getContext()->regulars = regs;

    qDebug() << " ============================================================";

    MainWindow w;
    w.bWidget.getBoards();
    w.show();

    return a.exec();
}

bool prepareDB()
{
    qDebug() << Q_FUNC_INFO;
    QString pathdir = QCoreApplication::applicationDirPath();

    AppContext::getContext()->xmlProcessor.setXmlFile(":/db.xml");
    if(!AppContext::getContext()->xmlProcessor.ifFileExists()) {
        qDebug() << "Не найден файл :/db.xml";
        return false;
    }

    if(!AppContext::getContext()->xmlProcessor.ifXmlSyntaxCorrect()) {
        qDebug() << "Неверный формат файла :/db.xml";
        return false;
    }

    QVector<Table> tables;

    try {
        tables = AppContext::getContext()->xmlProcessor.getTables();
    }
    catch(MyException ex) {
        qDebug() << "Exception:" << ex.getMessage();
        return false;
    }

    for(int i = 0; i < tables.size(); i++)
        AppContext::getContext()->tables.append(tables[i]);

    qDebug() << "Number of tables:" << tables.size();

    for(int i = 0; i < tables.size(); i++) {
        qDebug() << tables[i].getName();
    }

    QString dbFile = AppContext::getContext()->getProperty("dbname", "main");
    if(dbFile.trimmed().isEmpty()) {
        qDebug() << "Не найден параметр dbname!";
        return false;
    }

    AppContext::getContext()->db.setDBName(pathdir + "/" + dbFile);

    AppContext::getContext()->deletePostsTimeout = AppContext::getContext()->getProperty("deletePostsTimeout", "main").toInt();

    if(AppContext::getContext()->deletePostsTimeout == 0)
    {
        qDebug() << "ehgoetmv398vtv349";
        AppContext::getContext()->deletePostsTimeout = 604800000;
        //AppContext::getContext()->deletePostsTimeout = 120000;
    }


    return true;
}

bool setDB()
{
    qDebug() << Q_FUNC_INFO;

    QVector<Table> tables;

    try {
        tables = AppContext::getContext()->xmlProcessor.getTables();
    }
    catch(MyException ex) {
        qDebug() << "Exception:" << ex.getMessage();
        return false;
    }

    if(!AppContext::getContext()->db.ifExists()) {
        qDebug() << "Создаем базу данных" << AppContext::getContext()->db.getDBName();
        if(!AppContext::getContext()->db.open()) {
            if(AppContext::getContext()->db.getIsError()) {
                qDebug() << "SQL DB OPEN ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
            }
            return false;
        }

        qDebug() << tables.size();

        for(int i = 0; i < tables.size(); i++) {
            if(!AppContext::getContext()->db.createTable(tables[i])) {
                if(AppContext::getContext()->db.getIsError()) {
                    qDebug() << "SQL TABLE CREATE ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
                    return false;
                }
            }
        }
        return true;

    }
    else {

        try {
            if(!AppContext::getContext()->db.open()) {
                if(AppContext::getContext()->db.getIsError()) {
                    qDebug() << "SQL DB OPEN ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
                }
                return false;
            }
            if(!AppContext::getContext()->db.isDBCorrect(tables)) {
                if(AppContext::getContext()->db.getIsError()) {
                    qDebug() << "SQL CHECK DB ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
                    return false;
                }

                QFile file(AppContext::getContext()->db.getDBName());
                QString copyFileName = AppContext::getContext()->db.getDBName() + "_" + QString::number(MainUtils::getContext()->getCurrentTimestamp());
                qDebug() << "Создаем резервную копию текущей базы данных:" << copyFileName;
                AppContext::getContext()->db.close();
                file.copy(copyFileName);
                file.close();
                qDebug() << "Удаляем текущую базу данных";
                QFile::remove(AppContext::getContext()->db.getDBName());

                AppContext::getContext()->db.open();
                qDebug() << "Создаем базу данных" << AppContext::getContext()->db.getDBName();
                if(!AppContext::getContext()->db.checkOpen()) {
                    if(AppContext::getContext()->db.getIsError()) {
                        qDebug() << "SQL DB OPEN ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
                    }
                    return false;
                }

                for(int i = 0; i < tables.size(); i++) {
                    qDebug() << "CREATE TABLE" << tables[i].getName();
                    if(!AppContext::getContext()->db.createTable(tables[i])) {
                        if(AppContext::getContext()->db.getIsError()) {
                            qDebug() << "SQL TABLE CREATE ERROR:" << AppContext::getContext()->db.getLastErrorMessage();
                            return false;
                        }
                    }
                }
                return true;

            }
        }
        catch(std::exception e) {
            qDebug() << e.what();
        }
    }
}

bool initPhantomjs()
{
    QString phantomjs = AppContext::getContext()->getProperty("Prefix", "PhantomJS");
    if(phantomjs.trimmed() == "")
        return false;

    AppContext::getContext()->setPhantomjsPrefix(phantomjs);
    return true;
}

bool getRegulars(Regulars *regulars)
{
    QString pathdir = QCoreApplication::applicationDirPath();

    QString fileName = pathdir + "/config/regulars";
    QFile file(fileName);

    if (!file.exists() || !file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Ошибка открытия для чтения" << fileName;
        return false;
    }

    QTextStream stream(&file);
    QStringList strings;
    stream.setCodec("UTF-8");
    while (!stream.atEnd())
    {
        QString temp = stream.readLine();
        strings.append(temp);
    }

    file.close();

    if(strings.size() < 6)
    {
        qDebug() << "Недостаточно данных в файле" << fileName;
    }

    QString regBoard = strings[0];
    QString regName = strings[1];
    QString regPostsPerHour = strings[2];
    QString regUnicIps = strings[3];
    QString regDescription = strings[4];
    QString regPostsCount = strings[5];

    if(regBoard.trimmed().isEmpty()) {
        qDebug() << "Недостаточно данных в файле" << fileName;
        return false;
    }
    if(regName.trimmed().isEmpty()) {
        qDebug() << "Недостаточно данных в файле" << fileName;
        return false;
    }
    if(regPostsPerHour.trimmed().isEmpty()) {
        qDebug() << "Недостаточно данных в файле" << fileName;
        return false;
    }
    if(regUnicIps.trimmed().isEmpty()) {
        qDebug() << "Недостаточно данных в файле" << fileName;
        return false;
    }
    if(regDescription.trimmed().isEmpty()) {
        qDebug() << "Недостаточно данных в файле" << fileName;
        return false;
    }
    if(regPostsCount.trimmed().isEmpty()) {
        qDebug() << "Недостаточно данных в файле" << fileName;
        return false;
    }

    regulars->regBoard = regBoard;
    regulars->regName = regName;
    regulars->regPostsPerHour = regPostsPerHour;
    regulars->regUnicIps = regUnicIps;
    regulars->regDescription = regDescription;
    regulars->regPostsCount = regPostsCount;

    return true;
}

bool initSession()
{
    QString clearIntervalStr = AppContext::getContext()->getProperty("clearInterval", "Session");
    if(clearIntervalStr.trimmed() == "")
        return false;

    int clearInterval;
    try {
        clearInterval = clearIntervalStr.toInt();
    }
    catch(exception e) {
        qDebug() << e.what();
        return false;
    }

    if(clearInterval <= 0) {
        qDebug() << "Значение параметра interval группы Session должно быть целым положительным числом!";
        return false;
    }

    AppContext::getContext()->session.setClearInterval(clearInterval);

    return true;
}

bool initGarbageUtilizer()
{
    QString timeoutStr = AppContext::getContext()->getProperty("interval", "GarbageUtilizer");
    if(timeoutStr.trimmed() == "")
        return false;

    int timeout;
    try {
        timeout = timeoutStr.toInt();
    }
    catch(exception e) {
        qDebug() << e.what();
        return false;
    }

    if(timeout <= 0) {
        qDebug() << "Значение параметра interval группы GarbageUtilizer должно быть целым положительным числом!";
        return false;
    }

    AppContext::getContext()->garbageUtilizer.setFilesDeleteTimeout(timeout);
    return true;
}

void loadDataFromDB()
{
    QVector<QPair<Task, QPair<QString, QString>>> tasks = AppContext::getContext()->dbShell.getTasks();
    for(int i = 0; i < tasks.size(); i++)
    {
        Tasks::getCurrentTasks()->addTask(tasks[i].first);
    }

}
