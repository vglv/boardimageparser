#ifndef TASKSWIDGET_H
#define TASKSWIDGET_H

#include <QWidget>
#include <QStandardItemModel>
#include <QTableWidgetItem>
#include <QMessageBox>
#include "addtaskwidget.h"

namespace Ui {
class tasksWidget;
}

class tasksWidget : public QWidget
{
    Q_OBJECT

public:
    explicit tasksWidget(QWidget *parent = 0);
    ~tasksWidget();
    addTaskWidget aWidget;

    void getTasks();
    void setButtonsEnabled(bool enabled = false);
public slots:
    void tasksChanged();

private slots:
    void on_pbOK_clicked();
    void on_pbAdd_clicked();

    void on_pbDelete_clicked();

    void on_tvTasks_clicked(const QModelIndex &index);

    void on_pbChange_clicked();

private:
    Ui::tasksWidget *ui;
    QStandardItemModel *model;

};

#endif // TASKSWIDGET_H
