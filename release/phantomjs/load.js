var system = require('system');
var url = system.args[1];
var ppp = system.args[2];
var timestamp = system.args[3];
var fs = require('fs');
var page = require('webpage').create();
page.open(url, function(status) {
    if (status === 'success') {
        var html = page.evaluate(function() {
            return document.documentElement.outerHTML;
        });
        try {
			fs.makeDirectory("temp");
            fs.write("temp/" + ppp + "_" + timestamp + ".html_temp", html, 'w');
			//fs.move("temp/" + ppp + "_" + timestamp + ".html_temp", "temp/" + ppp + "_" + timestamp + ".html");
        } catch(e) {
            console.log(e);
        }
    }
    phantom.exit();
});